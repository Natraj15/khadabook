package com.mycompany.springproject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component

public class Tyre {
    
    @Value("${Mycar.Brand}")//@Value("crf")
    String brand;
    @Value("${Mycar.name}")
    String name;

    public Tyre() {
    }

    public Tyre(String brand, String name) {
        this.brand = brand;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Tyre{" + "brand=" + brand +"name=" + name + '}';
    }
 
}
