package com.mycompany.springproject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.*;  

public class TestEmployee {

    public static void main(String[] args) {
//
//        Resource resource=new ClassPathResource("newXMLDocument.xml");
//        BeanFactory bean=new XmlBeanFactory(resource);
        ApplicationContext context = new ClassPathXmlApplicationContext("Employee.xml");
          
       Employee employee= context.getBean("b",Employee.class);
       Employee employee1= context.getBean("b",Employee.class);
       
        System.out.println("before");
        System.out.println(employee);
        System.out.println(employee1);
        employee.setId(10001);
        System.out.println("after");
        System.out.println(employee);
        System.out.println(employee1);
       
        employee.show();
        employee1.show();
        System.out.println(" ");
        Employee employee2= context.getBean("e",Employee.class);
        System.out.println(employee2);
        
    }
}
