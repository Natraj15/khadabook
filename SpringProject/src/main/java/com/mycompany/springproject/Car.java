package com.mycompany.springproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("cars")
public class Car implements Vihycle{
      
      @Autowired
      private Tyre tyre;

    public Tyre getTyre() {
        return tyre;
    }

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    @Override
    public void drive() {
        
        System.out.println("drive a car"+tyre);
    }
    
}
