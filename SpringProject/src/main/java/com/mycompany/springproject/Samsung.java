package com.mycompany.springproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

//@Component
public class Samsung {

    @Autowired
    @Qualifier("mobileprocessor")
    private MobileDao processor;

    public Samsung() {
    }

    public Samsung(MobileDao processor) {
        this.processor = processor;
    }

    public MobileDao getProcessor() {
        return processor;
    }

    public void setProcessor(MobileDao processor) {
        this.processor = processor;
    }

    public void config() {
        System.out.println("samsung is working");
        processor.processor();
    }
}
