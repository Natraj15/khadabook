package com.mycompany.springproject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;



@Configuration
@ComponentScan(basePackages ="com.mycompany.springproject")
public class MobileConfig {
    
    @Bean
    public Samsung getMobile(){
        return new Samsung();
    }
    
    @Bean(name ="mobileprocessor")
    public MobileDao getProcessor(){
        return new MobileProcessor();
    }
    @Bean(name="mobileprocessor2")
    public MobileDao getprocessor2(){
        return new MobileProcessor2();
    }
    
    @Bean
    public Demo getDemo(){
        return new Demo();
    }
}
