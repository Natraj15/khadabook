package com.mycompany.springproject;

public class Employee {
    
    int id;
    Address address;
    String name;

    public Employee() {
        super();
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

//     public Employee(int id) {
//        this.id = id;
//       
//    }
//     public Employee(String name) {
//       
//        this.name = name;
//    }
//    
//    public Employee(int id, String name) {
//        this.id = id;
//        this.name = name;
//    }
    public void setName(String name) {
        this.name = name;
    }

    public Employee(int id, Address address, String name) {
        this.id = id;
        this.address = address;
        this.name = name;
    }

    
    public void show(){
        
        System.out.println(id+""+name);
        
        System.out.println(address);
        
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", address=" + address + ", name=" + name + '}';
    }
}
