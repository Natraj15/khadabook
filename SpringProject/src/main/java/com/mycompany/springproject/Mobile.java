package com.mycompany.springproject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

//configuration bean (without xml file)
//------------------

public class Mobile {

    public static void main(String[] args) {
      
       ApplicationContext factory = new AnnotationConfigApplicationContext(MobileConfig.class);
       Samsung s = factory.getBean(Samsung.class);
       s.config();
       Demo demo=factory.getBean(Demo.class);
       demo.config();  
  
    }

}
