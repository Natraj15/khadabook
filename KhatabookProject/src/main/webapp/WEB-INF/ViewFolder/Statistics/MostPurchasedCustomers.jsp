<%-- 
    Document   : MostPurchasedCustomers
    Created on : 30-Mar-2023, 10:34:49 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Orders"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Khatabook</title>
        <style>
            td,th {
                border:1px solid black;
                border-style: dashed;
                text-align: center;
            }
            table{
                border-style: groove;
            }
        </style>
    </head>
    <body style="background-color:aliceblue">

        <%
            Orders[] orders=(Orders[])session.getAttribute("mostPurchasedCustomers");
            if(orders.length>0){
        
        %>
        

        <table>
            <tr>
                <th>Customer ID</th>
                <th>Total Price</th>
            </tr>
          <%  for (Orders order : orders) { %>
            <tr>
                <td> <%=order.getCustomerId() %> </td>
                <td><%=order.getTotalPrice() %></td>
                </tr>
          <% } %>  
            </table>
        <% }else{
              out.println("<p>Some Error</p>");
          }
        %>
            
        <a href="HomePage.html">Home</a>
    </body>
</html>
