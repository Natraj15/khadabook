<%-- 
    Document   : MostSoldProducts
    Created on : 30-Mar-2023, 11:09:35 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Inventory"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Khatabook</title>
        <style>
            td,th {
                border:1px solid black;
                border-style: solid;
                text-align: center;
            }
            table{
                border-style: double;
            }
        </style>
    </head>
    <body style="background-color:aliceblue">

        <% 
           Inventory[] products=(Inventory[])session.getAttribute("soldProducts");
           if(products.length>0){
        %>

        <table>
            <tr>
                <th>Product ID</th>
                <th>Sold Products</th>
            </tr>
           
            <% for (Inventory product : products) { %>
                        <tr>
                            <td><%= product.getProductID()%></td>
                            <td><%= product.getSoldProductsQuantity()%></td>
                        </tr>
                        <%  } %>
                    </table>
                        
             <%
                 }else{
                  out.println("<p>Some Error</p>");
}
             
             %>           
   <a href="HomePage.html">Home</a>
    </body>
</html>
