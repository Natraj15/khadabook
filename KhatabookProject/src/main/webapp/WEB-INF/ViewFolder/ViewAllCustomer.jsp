<%-- 
    Document   : ViewAllCustomer
    Created on : 30-Mar-2023, 10:46:07 am
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Customers" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Khatabook</title>
        <style>
            table, th, td {
                border:1px solid black;
            }
            caption{
                text-decoration-line: underline;
                text-decoration-style: wavy;
                text-decoration-color: royalblue;

            }
        </style>

    </head>
    <body style="background-color:aliceblue">



        <%
             Customers[] customers=(Customers[])session.getAttribute("customers");
             if(customers.length>0){
        
        %>

        <table style="width: 100px;">
            <caption><h3>Customers Details</h3></caption>
            <tr >
                <th>Customer ID</th>
                <th>Customer Name</th>
                <th>Phone Number</th>
                <th>Address</th>
                <th>Balance Amount</th>
            </tr>
            <%  for(Customers customer:customers){ %>

            <tr>
                <td><%=customer.getCustomerID()%></td>
                <td><%=customer.getName()%></td>
                <td><%=customer.getPhoneNumber()%></td>
                <td><%=customer.getAddress()%></td>
                <td><%=customer.getBalanceAmount()%></td>
            </tr>
            <% } %> 
        </table>
        <%
            }else{

                out.println("<p>Some Error </p>");
            }
                    
        %>
        <a href='HomePage.html'>Home</a><br>
    </body>
</html>
