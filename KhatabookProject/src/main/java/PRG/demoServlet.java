/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package PRG;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 *
 * @author bas200174
 */
@WebServlet(name = "demoServlet", urlPatterns = {"/demoServlet"})
public class demoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet demoServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet demoServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            /*
             for(Orders order:orders){
                                 out.println("_".repeat(25));  
                                 out.println("");
                                 out.println("Order ID    : "+order.getOrderId());
                                 out.println("Customer ID : "+order.getCustomerId());
                                 out.println("Date        : "+order.getDate());
                              for(LineItems lineitems: order.getLine()){
                                 out.println("LineItem ID : "+lineitems.getLineItemId());  
                                 out.println("product ID  : "+lineitems.getProductId());
                                 out.println("Quantity    : "+lineitems.getQuantity());
                                 out.println("price       : "+lineitems.getPrice());
                                 }
                                 out.println("-".repeat(25));
                                 out.println("Total Price : "+order.getTotalPrice());
                                 out.println("-".repeat(25));
                                      out.println("     #Thank You#");
                                 out.println("_".repeat(25));
                                 out.println(" ");
            */
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
