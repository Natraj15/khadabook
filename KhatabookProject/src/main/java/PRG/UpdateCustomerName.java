package PRG;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/UpdateCustomerName")
public class UpdateCustomerName extends HttpServlet{

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        int id=Integer.parseInt(request.getParameter("customerID"));
        String name=request.getParameter("customerName");
        PrintWriter out=response.getWriter();
        
        out.print("<body style='background-color:aliceblue'>");
        
        KhatabookDao khatabook=new MysqlConnection();
        boolean cheack=khatabook.getCustomers().checkCustomerID(id);
        if(cheack){
            
             int message=khatabook.getCustomers().updateCustomerName(id, name);
             HttpSession session=request.getSession();
             if(message>0){
                 session.setAttribute("message", "Name Updated Sucessfully");
                 response.sendRedirect("SendMessage1");
               // out.print("sucessfully Updated");
               
             }else{
                 session.setAttribute("message", "Updation Faild");
                  System.out.println(message);
                 response.sendRedirect("SendMessage1");
                 
               //  out.println("<p style='color:red;'>"+message+"</p>");
             }

        }else{
            out.println("<p style='color:red;'> ID is not Exist (Create Customer Account)</p>");
        }
        
        out.println("</body>");
        
    }
    
    
}
