package PRG;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/UpdateCustomerPhoneNumber")
public class UpdateCustomerPhoneNumber extends HttpServlet{

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        int id=Integer.parseInt(request.getParameter("customerID"));
        long phoneNumber=Long.parseLong(request.getParameter("customerPhoneNumber"));
        PrintWriter out=response.getWriter();
        KhatabookDao khatabook=new MysqlConnection();
        boolean cheack=khatabook.getCustomers().checkCustomerID(id);
        HttpSession session=request.getSession();
        if(cheack){
            
             int message=khatabook.getCustomers().updateCustomerPhoneNumber(id, phoneNumber);
             if(message>0){
                 
                 session.setAttribute("message", "PhoneNumber Updated Sucessfully");
                 response.sendRedirect("SendMessage1"); 
            
             }else{
                 
                  System.err.println(message);
                  response.sendRedirect("SendMessage1");
                  

//                 out.println("<p style='color:red;'>"+message+"</p>");
                
             }

        }else{
            out.println("<p style='color:red;'> ID is not Exist (Create Customer Account)</p>");
        }
    }
    
    
}
