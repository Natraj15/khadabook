package PRG;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import jakarta.servlet.GenericServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/updateproduct", asyncSupported = true)
public class UpdateProduct extends GenericServlet {

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
      
        int option = Integer.parseInt(request.getParameter("option"));
        int productID = Integer.parseInt(request.getParameter("productID"));
        PrintWriter out = response.getWriter();
        out.print(" <a href='http://localhost:8080/KhatabookProject/index.html'>Home</a><br>");  
        KhatabookDao khatabook = new MysqlConnection();
        Inventory product = new Inventory();
        product.setProductID(productID);
        boolean cheack = khatabook.getInventory().checkProductID(productID);
        int message = 0;
        if (cheack) {
            switch (option) {
                case 1:
                    String productName = request.getParameter("productName");
                    product.setProductName(productName);
                    message = khatabook.getInventory().updateProduct(product, option);
                    if (message > 0) {
                        out.println("sucessfully updated");
                        
                    } else {
                        out.print("<p style='color:red;'> sorry Not updated  </p>");
                    }
                case 2:
                    int productQuantity = Integer.parseInt(request.getParameter("productQuantity"));
                    product.setProductQuantity(productQuantity);
                    message = khatabook.getInventory().updateProduct(product, option);
                    if (message > 0) {
                        out.println("sucessfully updated");
                    } else {
                        out.print("<p style='color:red;'> sorry Not updated  </p>");
                    }
                case 3:
                    double oneprice = Double.parseDouble(request.getParameter("oneprice"));
                    product.setOneProductprice(oneprice);
                    message = khatabook.getInventory().updateProduct(product, option);
                    if (message > 0) {
                        out.println("sucessfully updated");
                    } else {
                        out.print("<p style='color:red;'> sorry Not updated  </p>");
                    }
                case 4: 
                     double totalprice = Integer.parseInt(request.getParameter("totalprice"));
                    product.setTotalProductprice(totalprice);
                    message = khatabook.getInventory().updateProduct(product, option);
                    if (message > 0) {
                        out.println("sucessfully updated");
                    } else {
                        out.print("<p style='color:red;'> sorry Not updated  </p>");
                    }
            }
        } else {
            out.print("<p style='color:red;'> ID is not Exist </p>");
        }
       
    }

}
