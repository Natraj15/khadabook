package Pojo;

public class LineItems {
    
    int orderId;
    int lineItemId;
    int productId;
    int quantity;
    double oneProductPrice;
    double price;

    public LineItems() {
    }

    public LineItems(int orderId, int lineItemId, int productId, int quantity, double oneProductPrice, double price) {
        this.orderId = orderId;
        this.lineItemId = lineItemId;
        this.productId = productId;
        this.quantity = quantity;
        this.oneProductPrice = oneProductPrice;
        this.price = price;
    }

  
    
    public LineItems( int productId, int Quantity, double price) {
        this.productId = productId;
        this.quantity = Quantity;
        this.price = price;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getLineItemId() {
        return lineItemId;
    }

    public void setLineItemId(int lineItemId) {
        this.lineItemId = lineItemId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int Quantity) {
        this.quantity = Quantity;
    }

    public double getOneProductPrice() {
        return oneProductPrice;
    }

    public void setOneProductPrice(double oneProductPrice) {
        this.oneProductPrice = oneProductPrice;
    }
    
    

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "LineItems{" + "orderId=" + orderId + ", lineItemId=" + lineItemId + ", productId=" + productId + ", quantity=" + quantity + ", oneProductPrice=" + oneProductPrice + ", price=" + price + '}';
    }

   
    
}
