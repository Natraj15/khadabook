package Pojo;

public class Inventory {
    
  
    private int productID;
    private String productName;
    private int productQuantity;
    private double oneProductPrice;
    private double totalProductPrice;
    private int soldProductsQuantity;
    public Inventory() {
    }

    public Inventory(int productID, String productName, int productQuantity, double oneProductprice, double totalProductprice, int soldProductsQuantity) {
        this.productID = productID;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.oneProductPrice = oneProductprice;
        this.totalProductPrice = totalProductprice;
        this.soldProductsQuantity = soldProductsQuantity;
    }
    
    public Inventory(String productName, int productQuantity, double oneProductprice, double totalProductprice) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.oneProductPrice = oneProductprice;
        this.totalProductPrice = totalProductprice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public double getOneProductprice() {
        return oneProductPrice;
    }

    public void setOneProductprice(double oneProductprice) {
        this.oneProductPrice = oneProductprice;
    }

    public double getTotalProductprice() {
        return totalProductPrice;
    }

    public void setTotalProductprice(double totalProductprice) {
        this.totalProductPrice = totalProductprice;
    }

    public int getSoldProductsQuantity() {
        return soldProductsQuantity;
    }

    public void setSoldProductsQuantity(int soldProductsQuantity) {
        this.soldProductsQuantity = soldProductsQuantity;
    }
    

    @Override
    public String toString() {
        return "Inventory{" + "productName=" + productName + ", productID=" + productID + ", productQuantity=" + productQuantity + ", oneProductprice=" + oneProductPrice + ", totalProductprice=" + totalProductPrice +", oneProductprice=" + soldProductsQuantity+  '}';
    }
    
    
}
