package Order;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet("/PaymentAmountServlet")
public class PaymentAmountServlet extends GenericServlet{

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        
        int customerID=Integer.parseInt(request.getAttribute("customer").toString());
        double balanceAmount=Double.parseDouble(request.getAttribute("balanceAmount").toString());
        response.getWriter().print("<p>Your Balance Amount  :"+balanceAmount+"</p>");
        response.getWriter().print("""
                                   <html>
                                     <body>""");
          response.getWriter().print("<form action=payments?customerID="+customerID+" method=post >");
           response.getWriter().print("""
                                         <label>Enter Paymetnt Amount</label><br>
                                         <input type="number" name="paymentAmount" min="1"><br>
                                         <input type="submit" name="submit" value="pay"><br>
                                      </form>
                                     </body> 
                                   </html>
                                   
                                   """  );
          
           
    }
}
