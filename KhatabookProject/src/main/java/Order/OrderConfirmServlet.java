package Order;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Objects;

@WebServlet(name = "OrderConfirmServlet", urlPatterns = {"/OrderConfirmServlet"})
public class OrderConfirmServlet extends HttpServlet {

     Inventory[] products = new Inventory[0];

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderConfirmServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            int orderId=Integer.parseInt(request.getParameter("OrderID"));
            out.println("<p>orderId:"+orderId+"</p>");
            KhatabookDao khatabook=new MysqlConnection();
            Orders order=khatabook.getOrders().viewOrdersbyOrderID(orderId);
            Customers customer;
            if(Objects.nonNull(order)){
           
                
            for(LineItems lineitems:order.getLine()){
                
            int productId=lineitems.getProductId();
            int quantity=lineitems.getQuantity();
            
//            Inventory product=khatabook.getInventory().viewOneProduct(productId);
//               int productQuantity = product.getProductQuantity() - quantity;
//                    product.setProductQuantity(productQuantity);
//                    int soldProductQuantity = product.getSoldProductsQuantity() + quantity;
//                    product.setSoldProductsQuantity(soldProductQuantity);
//                    product.setProductID(productId);

                    Inventory product=new Inventory();
                    product.setProductID(productId);
                    product.setProductQuantity(quantity);
                    products = Arrays.copyOf(products, products.length + 1);
                    products[products.length - 1] = product;
                 }
                 customer=khatabook.getCustomers().viewOneCustomer(order.getCustomerId());
                    
                  double balanceAmount=  order.getTotalPrice()+customer.getBalanceAmount();
                  customer.setBalanceAmount(balanceAmount);
                  
            HttpSession session=request.getSession();
            int message=0;
            message=khatabook.getOrders().confimeOrders(products, customer,orderId);
            if(message>0){
               
                 products=Arrays.copyOf(products, 0);
                 session.setAttribute("message", "Order Sucessfully Confiremed");
                 response.sendRedirect("SendMessage2");
            }else{
                 session.setAttribute("message", "Order Conformations Failed");
                 response.sendRedirect("SendMessage2");
               
            }
            
          }  
            else{
                out.println("<p>No pending Orders</p>");
            }
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
