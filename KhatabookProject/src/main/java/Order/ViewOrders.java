package Order;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/viewOrderByCustomerID")
public class ViewOrders extends HttpServlet{

     
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
        int customerID=Integer.parseInt(request.getParameter("customerID"));
        PrintWriter out=response.getWriter();
          out.println("<body style='background-color:aliceblue'>");
        KhatabookDao khatabook=new MysqlConnection();
        HttpSession session=request.getSession();
        Orders[]  orders=khatabook.getOrders().viewOrdersbyCustomerID(customerID);
        
        if(orders.length>0){
          
            session.setAttribute("orders", orders);
            RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewOrders.jsp");
            dispatcher.forward(request, response);
            /*for(Orders order:orders){
                                 out.println("_".repeat(25));  
                                 out.println("");
                                 out.println("Order ID    : "+order.getOrderId());
                                 out.println("Customer ID : "+order.getCustomerId());
                                 out.println("Date        : "+order.getDate());
                              for(LineItems lineitems: order.getLine()){
                                 out.println("LineItem ID : "+lineitems.getLineItemId());  
                                 out.println("product ID  : "+lineitems.getProductId());
                                 out.println("Quantity    : "+lineitems.getQuantity());
                                 out.println("price       : "+lineitems.getPrice());
                                 }
                                 out.println("-".repeat(25));
                                 out.println("Total Price : "+order.getTotalPrice());
                                 out.println("-".repeat(25));
                                      out.println("     #Thank You#");
                                 out.println("_".repeat(25));
                                 out.println(" ");
                       
            } */
      
            
        }else{
            out.println("<p color:'red'>customer not placed any orders </p>");
           
        }
        out.println(" <a href='http://localhost:8080/KhatabookProject/index.html'>Home</a><br>");
         out.println("</body>");
    }
    
    
}
