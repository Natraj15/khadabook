package Order;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet("/order")
public class OrdersServlet extends HttpServlet {

     LineItems[] lineitems = new LineItems[0];
     Inventory[] products = new Inventory[0];
     double totalPrice = 0.0;
     int customerID=0;
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
          response.setContentType("text/html;charset=UTF-8");
         PrintWriter out=response.getWriter();
           out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Message1</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
        
         int option=Integer.parseInt(request.getParameter("option"));
        
         boolean cheack=true;
         int message = 0;
          KhatabookDao khatabook = new MysqlConnection();
        if(option==1){
         customerID = Integer.parseInt(request.getParameter("customerID"));
         cheack = khatabook.getCustomers().checkCustomerID(customerID);
           }
        if (cheack) {
            int productId = Integer.parseInt(request.getParameter("productID"));
            cheack = khatabook.getInventory().checkProductID(productId);
            if (cheack){
                Inventory product = khatabook.getInventory().viewOneProduct(productId);
                int quantity = Integer.parseInt(request.getParameter("quantity"));
                if (quantity <= product.getProductQuantity()) {
                double price = quantity * product.getOneProductprice();
                    totalPrice += price;

                    int productQuantity = product.getProductQuantity() - quantity;
                    product.setProductQuantity(productQuantity);
                    int soldProductQuantity = product.getSoldProductsQuantity() + quantity;
                    product.setSoldProductsQuantity(soldProductQuantity);
                    product.setProductID(productId);
                    
                    products = Arrays.copyOf(products, products.length + 1);
                    products[products.length - 1] = product;

                    LineItems line = new LineItems();
                    line.setProductId(productId);
                    line.setQuantity(quantity);
                    line.setOneProductPrice(product.getOneProductprice());
                    line.setPrice(price);
                    

                    lineitems = Arrays.copyOf(lineitems, lineitems.length + 1);
                    lineitems[lineitems.length - 1] = line;
                    String submit = request.getParameter("submit");
                    if ("submit".equals(submit)) {
                        Orders order = new Orders();
                        order.setCustomerId(customerID);
                        order.setTotalPrice(totalPrice);
                        order.setLine(lineitems);
                        message = khatabook.getOrders().placeOrder(order, products);
                    } else {
//                        request.setAttribute("customer", customerID);
                        RequestDispatcher requestdispatcher = request.getRequestDispatcher("PlaceOrderProducts");
                        requestdispatcher.forward(request, response);
                    }
                    HttpSession session=request.getSession();
                    if (message>0) {
                          
                         // out.println("<p>order placed successfully</p>");
                          lineitems=Arrays.copyOf(lineitems, 0);
                          products=Arrays.copyOf(products, 0);
                          totalPrice=0.0;
                           session.setAttribute("message", "Order Placed Sucessfully");
                          response.sendRedirect("SendMessage2");
                    } else {
                      //  out.print("<p>sorry order not placed</p>");
                        session.setAttribute("message", "Order Failed");
                        response.sendRedirect("SendMessage2");
                    }
      
                } else {
                    out.print("<p style='color:red;'>Not Enough products<p>");
                }

            } else {
                out.print("<p style='color:red;'>productID not exist<p>");
            }

        } else {
            out.print("<p style='color:red;'>customerID not exist<p>");
        }
        
          out.println("<a href='http://localhost:8080/KhatabookProject/HomePage.html'>Home</a><br>");
          out.print("</body>");
          out.print("</html>");
          
    }

}
