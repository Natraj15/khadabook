package Order;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.LineItems;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ViewPendingOrders", urlPatterns = {"/ViewPendingOrders"})
public class ViewPendingOrders extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewPendingOrders</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
         //   out.println("<h3>Pending Orders</h3>");
            KhatabookDao khatabook=new MysqlConnection();
            HttpSession session=request.getSession();
            Orders[] orders= khatabook.getOrders().viewPendingOrders();
              if(orders.length>=1){
                  
                  session.setAttribute("orders", orders);
                  RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewPendingOrders.jsp");
                  dispatcher.forward(request, response);
                  
             /* for(Orders order:orders){
                                 out.println("_".repeat(22)+"<br>");  
                                 out.println("<br>");
                                 out.println("Order ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : "+order.getOrderId()+"<br>");
                                 out.println("Customer ID : "+order.getCustomerId()+"<br>");
                                 out.println("Date  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    : "+order.getDate()+"<br>");
                              for(LineItems lineitems: order.getLine()){
                                 out.println("LineItem ID  : "+lineitems.getLineItemId()+"<br>");  
                                 out.println("product ID &nbsp;&nbsp; : "+lineitems.getProductId()+"<br>");
                                 out.println("Quantity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   : "+lineitems.getQuantity()+"<br>");
                                 out.println("price   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    : "+lineitems.getPrice()+"<br>");
                                 }
                                 out.println("-".repeat(31)+"<br>");
                                 out.println("Total Price &nbsp;&nbsp; : "+order.getTotalPrice()+"<br>");
                                 out.println("-".repeat(31)+"<br>");
                                      out.println("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #Thank You# &nbsp;&nbsp;&nbsp;<br>");
                                 out.println("_".repeat(22));
                                 out.println(" ");
                               out.print("<div style='margin-left:135px;'>");
                               out.println("<form action=OrderConfirmServlet?OrderID="+order.getOrderId()+" method=post>");
                               out.println("""
                                                <input type="submit" name="submit" value="confim" ><br>  
                                           </form>
                                           </div>
                                               """);*/
                                 
                       
          //  } 
              }else{
                  out.println("<p>No pending Orders</p>");
              }
          
          
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
