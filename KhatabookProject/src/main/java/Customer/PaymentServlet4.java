package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import Pojo.Payments;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "PaymentServlet4", urlPatterns = {"/PaymentServlet4"})
public class PaymentServlet4 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
          
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PaymentServlet4</title>");            
            out.println("</head>");
            out.println("<body>");
            KhatabookDao khatabook=new MysqlConnection();
            
            HttpSession session=request.getSession();
            Customers customer=(Customers)session.getAttribute("customer");
            int customerid=customer.getCustomerID();
              double paymentAmount=Double.parseDouble(request.getParameter("paymentAmount"));
                  if(paymentAmount<=customer.getBalanceAmount()){
               double balanceAmount=customer.getBalanceAmount()-paymentAmount;
               Payments payment=new Payments(customerid,paymentAmount,balanceAmount);
              int message= khatabook.getPayment().MakePayment(payment);
                 
             
              if(message==2){
                               session.setAttribute("message", "Paid Sucessfully");
                               response.sendRedirect("SendMessage1");
                            
                    }else{
                          session.setAttribute("message", "Payment Failed");
                               response.sendRedirect("SendMessage1");
                    
                  }
               }
               else{
                   out.print("<p style='color:red;'>your Balance Amount Only:"+customer.getBalanceAmount()+"</p>");
               }
           
        
                  
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
