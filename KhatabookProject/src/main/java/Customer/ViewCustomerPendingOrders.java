package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ViewCustomerPendingOrders", urlPatterns = {"/ViewCustomerPendingOrders"})
public class ViewCustomerPendingOrders extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
          //  out.print("pending Orders");
           //  out.println("<h3>Pending Orders</h3>");
          
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewCustomerPendingOrders</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
            
            
            int customerid=Integer.parseInt(request.getParameter("customerID"));
           HttpSession session =request.getSession();
           
           // int customerid=Integer.parseInt(session.getAttribute("customerID").toString());
            
            
            KhatabookDao khatabook=new MysqlConnection();
            Orders[] orders= khatabook.getOrders().viewCustomerPendingOrders(customerid);
              if(orders.length>0){
             
                  session.setAttribute("orders", orders);
                  RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewOrders.jsp");
                  dispatcher.forward(request, response);
                  
              }else{
                  out.println("<p>No pending Orders</p>");
              }
            
            out.print(" <a href='CustomerHomePage'>Home</a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
