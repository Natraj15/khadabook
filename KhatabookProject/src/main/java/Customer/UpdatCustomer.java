package Customer;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet(name = "UpdatCustomer", urlPatterns = {"/UpdatCustomer"})
public class UpdatCustomer extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatCustomer</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
              int id=Integer.parseInt(request.getParameter("customerID"));
              
            HttpSession session=request.getSession();
         
           
                  out.println("<form action='updateCustomers?customerID="+id+"'"+ " method='post'>");
                    out.println("""             
                                 <label>Choose the Field</label><br>
                                 <select id="choose" name="choose">
                                         <option value="Name">Name</option>
                                         <option value="PhoneNumber">PhoneNumber</option>
                                         <option value="Address">Address</option>
                                     </select><br>
                                     <label for="id">Enter Value</label><br>    
                                 <input type="text" name="value" ><br>
                                 <input type="submit" name="submit" value="Update">
                                 
                             </form><br>
                        """);
            
            out.print(" <a href='CustomerHomePage'>Home</a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
