package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

@WebServlet(name = "CustomerPlaceOrder", urlPatterns = {"/CustomerPlaceOrder"})
public class CustomerPlaceOrder extends HttpServlet {
    
     LineItems[] lineitems = new LineItems[0];
     Inventory[] products = new Inventory[0];
     double totalPrice = 0.0;
     int customerID;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerPlaceOrder</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
            
            HttpSession session=request.getSession();
            // customerID=Integer.parseInt(session.getAttribute("customerID").toString());
              customerID=Integer.parseInt(request.getParameter("customerID"));
            boolean cheack;
        
          int message = 0;
          KhatabookDao khatabook = new MysqlConnection();

            int productId = Integer.parseInt(request.getParameter("productID"));
            cheack = khatabook.getInventory().checkProductID(productId);
            if (cheack){
                Inventory product = khatabook.getInventory().viewOneProduct(productId);
                int quantity = Integer.parseInt(request.getParameter("quantity"));
                if (quantity <= product.getProductQuantity()) {
                double price = quantity * product.getOneProductprice();
                    totalPrice += price;

//                    int productQuantity = product.getProductQuantity() - quantity;
//                    product.setProductQuantity(productQuantity);
//                    int soldProductQuantity = product.getSoldProductsQuantity() + quantity;
//                    product.setSoldProductsQuantity(soldProductQuantity);
//                    product.setProductID(productId);
//
//                    products = Arrays.copyOf(products, products.length + 1);
//                    products[products.length - 1] = product;

                    LineItems line = new LineItems();
                    line.setProductId(productId);
                    line.setQuantity(quantity);
                    line.setOneProductPrice(product.getOneProductprice());
                    line.setPrice(price);
                    

                    lineitems = Arrays.copyOf(lineitems, lineitems.length + 1);
                    lineitems[lineitems.length - 1] = line;
                    String submit = request.getParameter("submit");
                    if ("submit".equals(submit)) {
                        Orders order = new Orders();
                        order.setCustomerId(customerID);
                        order.setTotalPrice(totalPrice);
                        order.setLine(lineitems);
                        message = khatabook.getOrders().placeOrder(order, products);
                    } else {
//                        request.setAttribute("customer", customerID);
                        RequestDispatcher requestdispatcher = request.getRequestDispatcher("OrdersAddProducts");
                        requestdispatcher.forward(request, response);
                    }
                    
                    if (message>0) {
              
                          lineitems=Arrays.copyOf(lineitems, 0);
                          products=Arrays.copyOf(products, 0);
                          totalPrice=0.0;
                          session.setAttribute("message", "Order Placed Sucessfully");
                          response.sendRedirect("SendMessage1");
                          
                    } else {
                        
                        session.setAttribute("message", "Order Failed");
                        response.sendRedirect("SendMessage1");
                    }

                } else {
                    out.print("<p>Not enough products<p>");
                }

            } else {
                out.print("<p>productID not exist<p>");
            }

            out.print(" <a href='CustomerHomePage'>Home</a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
