package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet(name = "ViewProfile", urlPatterns = {"/ViewProfile"})
public class ViewProfile extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
       out.print("""
                      <style>
                 th,td{
                    font-size:20px;
                    border-style: dotted;
                     }
                 	table {
                  		        
                                        border-style:ridge;
                                        position: absolute;
                 			left: 0;
                 			right: 0;
                 			bottom:0;
                 			margin-left:180px;
                                        margin-bottom:230px;
                 			text-align: center;
                 		}
                
                 
                 </style>
                 """);
            out.println("<title>Servlet ViewProfile</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
             KhatabookDao khatabook=new MysqlConnection();
             HttpSession session=request.getSession();
             //int id=Integer.parseInt(session.getAttribute("customerID").toString());
             int id=Integer.parseInt(request.getParameter("customerID"));
      
         Customers customer=khatabook.getCustomers().viewOneCustomer(id);
       
       if(Objects.nonNull(customer)){  
          
           out.print("<table>");
            
//                             out.print(
//                                    """ 
//                                       <tr>      
//                                       <td> CustomerID </td>
//                                       <td> CustomerName</td>
//                                       <td> PhoneNumber</td>
//                                       <td> Address</td>
//                                       <td> BalanceAmount</td>
//                                     </tr>  
//                                     """
//                           );
         
                out.println("<tr>");
              
               out.println("<td> CustomerID </td><td>"+customer.getCustomerID() +"</td></tr>");
              
               out.println("<tr><td> CustomerName</td><td>"+customer.getName()+"</td></tr>");
             
               out.println("<tr><td> PhoneNumber</td><td>"+customer.getPhoneNumber()+"</td></tr>");
               out.println("<tr><td> Address</td><td>"+customer.getAddress()+"</td></tr>");
               out.println("<tr> <td> BalanceAmount</td><td>"+customer.getBalanceAmount()+"</td>");
                
               out.println("</tr>");     
            
               out.println("</table>");
       }else{
           out.println("<p style='color:red;'> ID is not Exist (Create Customer Account)</p>");
       }
            
            out.print(" <a href='CustomerHomePage'>Home</a><br>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
