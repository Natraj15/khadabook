
package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;



@WebServlet(name = "OrdersAddProducts", urlPatterns = {"/OrdersAddProducts"})
public class OrdersAddProducts extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
                 out.print("""
                      <style>
                 th,td{
                    
                    border-style: dotted;
                     }
                 	.table {
                 			position: absolute;
                           		border:1px solid black;
                 			text-align: center;
                                        border-style:inset;
                                        margin-left:50px;
                                        
                 		}
                         .table2{
                                border:1px solid;
                                border-style:outset;
                                position: absolute;
        			left: 0;
        			right: 0;
                                bottom:0;
       			        margin-left:400px;
                                margin-bottom:205px;
        			text-align: center;
                           }
                       caption{
                             text-decoration-line: underline;
                             text-decoration-style: wavy;
                             text-decoration-color: royalblue;
                             margin-top:100px;
                             height:45px;
                                             
                           }    
                           
                 </style>
                 """);
            
            out.println("<title>Servlet OrdersAddProducts</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
                out.print(" <a href='CustomerHomePage'>Home</a><br>");    
            int customerId=Integer.parseInt(request.getParameter("customerID"));
              HttpSession session =request.getSession();
           // int customerId=Integer.parseInt(session.getAttribute("customerID").toString());
            
            
                    out.println("<table class=table>");
                    
                    out.print("<br>");
                    out.print("<form action='CustomerPlaceOrder?customerID="+customerId+"'" + "method=post>");
                          out.print("""
                                    <tr><th colspan="2">Place Your's Order</th><tr>
                                              <tr>
                                             <td>  <label>Enter Product ID</label></td>
                                             <td>  <input type="number" name="productID"></td>
                                               </tr><tr>
                                               <td> <label>Enter Quantity</label></td>
                                               <td> <input type="number" name="quantity"></td>
                                             </tr><tr>
                                             <td><input type="submit" name="submit" value="AddProduct" ></td>
                                             <td><input type="submit" name="submit" value="submit" ></td>
                                             </tr>
                                           </form>
                                           </table>
                                               """);
//                          out.print("<br><br>");
//                          out.println("Products Details");
                          KhatabookDao khatabook=new MysqlConnection();
                        Inventory[] inventory=  khatabook.getInventory().viewAllProducts();
            if(inventory.length>0){
                
                  out.print("<table class=table2 > ");
                  out.println("<caption><h4>Products Details</h4></caption>");
                             out.print(
                                    """ 
                                       <tr>      
                                       <th> ProductID </th>
                                       <th> ProductName</th>
                                       <th> ProductQuantity</th>
                                       <th> OneProductPrice</th>

                                     </tr>  
                                     """
                           );
                             
//                                       <th> TotalProductPrice</th>
//                                       <th> SoldProducts</th>                          
               for(Inventory products:inventory){
                out.println("<tr>");
              
               out.println("<td>"+products.getProductID() +"</td>");
               out.println("<td>"+products.getProductName()+"</td>");
               out.println("<td>"+products.getProductQuantity()+"</td>");
               out.println("<td>"+products.getOneProductprice()+"</td>");
//               out.println("<td>"+products.getTotalProductprice()+"</td>");
//               out.println("<td>"+products.getSoldProductsQuantity()+"</td>");
                
               out.println("</tr>");     
               }
               out.println("</table>");
                    
                
            }else{
                System.err.println("<p>some error</p>");
            }   
                          
           
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
