package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/updateCustomers")
public class UpdateCustomerServlet extends HttpServlet{

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    
          response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateCustomersServlet</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
            
            int id=Integer.parseInt(request.getParameter("customerID"));
            String field=request.getParameter("choose");
            String value=request.getParameter("value");
          
            KhatabookDao khatabook=new MysqlConnection();
            boolean cheak=khatabook.getCustomers().checkCustomerID(id);
            if(cheak){
            
            int message=0;
            switch(field){
                case "Name":
                  message=   khatabook.getCustomers().updateCustomerName(id, value);
                    
                    break;
                 case "PhoneNumber":
                     
                     long phoneNumber=Long.parseLong(value);
                  message=    khatabook.getCustomers().updateCustomerPhoneNumber(id, phoneNumber);
                    break;  
                case "Address":
                    
                   message= khatabook.getCustomers().updateCustomerAddress(id, value);
                    
                    break;   
            }
            HttpSession session=request.getSession();
            if(message>0){
                
                String message2=field+" Updated Sucessfully";
                session.setAttribute("message", message2);
                response.sendRedirect("SendMessage1");
                
            }else{
                session.setAttribute("message", "Updation Failed");
                response.sendRedirect("SendMessage1");
            }
            
            }else{
                
                out.println("<p style='color:red;'>CustomerID not Exist<p>");
            }  
            
                
            
        }
    }
    
}
