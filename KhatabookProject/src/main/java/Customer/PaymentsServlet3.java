package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import Pojo.Payments;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet(name = "PaymentsServlet", urlPatterns = {"/PaymentsServlet3"})
public class PaymentsServlet3 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
              out.println("""
                        <style>
                        div{
                        border:1px;
                        border-style:
                        dashed;width:200px;
                        
                        padding:15px 30px;
                        }
                        
                        </style>
                        """);
            out.println("<title>Servlet PaymentsServlet3</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
           
           int customerid=Integer.parseInt(request.getParameter("customerID"));
           KhatabookDao khatabook=new MysqlConnection();
           Customers customer =khatabook.getCustomers().viewOneCustomer(customerid);
         

                 if(Objects.nonNull(customer) ){
               HttpSession session=request.getSession();
               session.setAttribute("customer", customer);
                 
                   out.println("<div style='margin-left:150px; margin-top:50px;'><p >Your Balance Amount :"+customer.getBalanceAmount()+"</p>");
               
              out.print("""
                        
                        <form action=PaymentServlet4?customerID="+customerID+" method=post
                                         <label>Enter Payment Amount</label><br>
                                         <input type="number" name="paymentAmount" min="1"><br>
                                       <input type="submit" name="submit" value="pay" style='margin-left:150px'><br>
                                      </form>
                              
                        </div>
                                     </body> 
                                   </html>
                                   
                                   """  );
           }
              else{
               
                out.println("<p style='color:red;'> ID is not Exist (Create Customer Account)</p>");
           }
               out.print("<a href='CustomerHomePage'>Home</a><br>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
