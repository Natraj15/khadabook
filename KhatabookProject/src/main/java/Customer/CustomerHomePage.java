package Customer;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CustomerHomePage", urlPatterns = {"/CustomerHomePage"})
public class CustomerHomePage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
           out.println("""
                         <style>
                          p {
                           margin-bottom: 3px; 
                            }
                       .center {
                           margin: auto;
                           width: 40%;
                           border: 3px solid LightBlue;
                           padding: 10px;
                         }
                      li{
                        font-size:20px;
                       }
                           .container {
                                 display: block;
                                 margin-left: auto;
                                 margin-right: auto;
                                 height: 90px;
                                 position: absolute;
                                 left: 290px;
                                 top: 0;
                                 margin-top: 5px;
                                }
                      
                         </style>""");
            out.println("<title>Servlet CustomerHomePage</title>");            
            out.println("</head>");
            out.println("<body style='background-color:Azure'>");
            
            HttpSession session=request.getSession();
           int customerID=Integer.parseInt(session.getAttribute("customerID").toString());
          // int customerID=1;
//            out.println("<p style='text-align:center'>Welcome to</p>");
           out.print(" <img class='container' src='khatabook.jpg' width='100px' height='400px' alt='kahatbook'/>");
            out.println(" <h1 style='text-align:center;font-size: 40px;'> KhataBook </h1>");
            KhatabookDao khatabook=new MysqlConnection();
            Customers customer=khatabook.getCustomers().viewOneCustomer(customerID);
            out.println("<p style='text-align:center;margin-right:250px'>Welcome<b> "+customer.getName()+"</b>...!</p>");
            out.println("<div class='center'");
            out.println("<ul title=clike>");                
            out.println("<li><a href='ViewProfile?customerID="+customerID+"'"+"</a>ViewProfile</li>");
            out.println("<li><a href='UpdatCustomer?customerID="+customerID+"'"+"</a>update Profile</li>");
            out.println("<li><a href='OrdersAddProducts?customerID="+customerID+"'"+"</a>Add Card</li>");
            out.println("<li><a href='PaymentsServlet3?customerID="+customerID+"'"+"</a>Make Payment</li>");
            out.println("<li><a href='viewOrderByCustomerID?customerID="+customerID+"'"+"</a>View confirmed Orders</li>");
            out.println("<li><a href='ViewCustomerPendingOrders?customerID="+customerID+"'"+"</a>View pending Orders</li>");
  
           out.println("</ul>"); 
           out.print("</div><br>");
           
        out.println("""  
                    <div align='center' style='margin-left:300px;'>      
            <form action="LogoutServlet">
            <input type="submit" id="id" name="name" value="Logout">
            </form>
            </div>        
            """);
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
