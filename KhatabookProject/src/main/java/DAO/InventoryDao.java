
package DAO;

import Pojo.Inventory;


public interface InventoryDao {
    
    public String addProducts(Inventory product);
    
    public Inventory viewOneProduct(int productId);
    
    public Inventory[] viewAllProducts();
    
    public boolean checkProductID(int productID);
    
    public int updateProduct(Inventory product,int option);
}
