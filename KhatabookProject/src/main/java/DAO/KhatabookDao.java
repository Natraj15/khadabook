package DAO;

public interface KhatabookDao {
    
    public CustomersDao getCustomers();
    
    public InventoryDao getInventory();
    
    public OrdersDao getOrders();
    
    public PaymentsDao getPayment();
    
    public StatisticsDao getStatistics();
}
