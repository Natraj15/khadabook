package DAO;

import Pojo.Customers;
import Pojo.Inventory;
import Pojo.Orders;

public interface OrdersDao {
    
    public int  placeOrder(Orders order,Inventory[] products);
    
    public Orders[] viewOrdersbyCustomerID(int customerID);
    
    Orders viewOrdersbyOrderID(int orderId);
    
    Orders[] viewPendingOrders();
    
    Orders[] viewCustomerPendingOrders(int customerID);
    
    int confimeOrders(Inventory[] products,Customers customers,int orderID);
}
