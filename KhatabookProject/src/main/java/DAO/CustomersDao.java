package DAO;

import Pojo.Customers;

public interface CustomersDao {
    
    public String addCustomers(Customers customer);
    
     public Customers viewOneCustomer(int id);
     
     public Customers[] viewAllCustomers();
     
     public boolean checkCustomerID(int id);
     
     public int updateCustomerName(int id,String name);
     
     public int updateCustomerAddress(int id,String address);
     
     public int updateCustomerPhoneNumber(int id,long phoneNumber);
}
