package MysqlDao;

import DAO.CustomersDao;
import DAO.InventoryDao;
import DAO.KhatabookDao;
import DAO.OrdersDao;
import DAO.PaymentsDao;
import DAO.StatisticsDao;

public class MysqlConnection implements KhatabookDao{

    @Override
    public CustomersDao getCustomers() {        
      return  new CustomersDaoImp();
    }

    @Override
    public InventoryDao getInventory() {
        
       return new InventoryDaoImp();
    }

    @Override
    public OrdersDao getOrders() {
        
        return new OrdersDaoImp();
    }

    @Override
    public PaymentsDao getPayment() {
       
        return new PaymentsDaoImp();
    }

    @Override
    public StatisticsDao getStatistics() {
     
       return new StatisticsDaoImp();
    }
    
    
}
