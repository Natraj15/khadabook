
package MysqlDao;

import DAO.InventoryDao;
import Pojo.Inventory;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;


public class InventoryDaoImp implements InventoryDao{

    @Override
    public String addProducts(Inventory product) {
        
           try ( Connection connection=Mysql.getConnection();
           PreparedStatement statement=connection.prepareCall("insert into Inventory (ProductName,ProductQuantity,OneProductprice,TotalProductprice) values(?,?,?,?)")){
           statement.setString(1,product.getProductName());
           statement.setInt(2,product.getProductQuantity());
           statement.setDouble(3, product.getOneProductprice());
           statement.setDouble(4, product.getTotalProductprice());
            
            statement.executeUpdate();
            return "successfull";
        }catch(SQLException | ClassNotFoundException sql ){
            System.out.println(sql.getMessage());
            return sql.getMessage();
        }
    }

    @Override
    public Inventory viewOneProduct(int productId) {
        
            Inventory product = null;
        try (Connection connection =Mysql.getConnection();
            Statement sta = connection.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Inventory where ProductID=" + productId);
            while (rs.next()) {
                product = new Inventory(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDouble(5), rs.getInt(6));
            }
        }catch(SQLException| ClassNotFoundException sql){
            System.err.println(sql.getMessage());
        }
        return product;
    }

    @Override
    public int updateProduct(Inventory product, int option) {
     
                int message=0;
        switch(option){
            case 1:try(Connection con=Mysql.getConnection();
                      PreparedStatement statement=con.prepareCall("update Inventory set ProductName=? where ProductID=?")){
                      statement.setInt(2, product.getProductID());
                      String name=product.getProductName();
                      statement.setString(1, name);
                    message=  statement.executeUpdate();
                  }catch(SQLException|ClassNotFoundException s){
                      s.printStackTrace();
                     System.err.println("Error".repeat(10)+s.getMessage());
                  }
            case 2:try(Connection con=Mysql.getConnection();
                      PreparedStatement statement=con.prepareCall("update Inventory set ProductQuantity=? where ProductID=?")){
                      statement.setInt(2, product.getProductID());
                      statement.setInt(1, product.getProductQuantity());
                    message=  statement.executeUpdate();
                  }catch(SQLException|ClassNotFoundException s){
                     System.err.println(s.getMessage());
                  }
            case 3:
                try(Connection con=Mysql.getConnection();
                      PreparedStatement statement=con.prepareCall("update Inventory set OneProductprice=? where ProductID=?")){
                      statement.setInt(2, product.getProductID());
                      statement.setDouble(1, product.getOneProductprice());
                    message=  statement.executeUpdate();
                  }catch(SQLException|ClassNotFoundException s){
                     System.err.println(s.getMessage());
                  }
            case 4:try(Connection con=Mysql.getConnection();
                      PreparedStatement statement=con.prepareCall("update Inventory set TotalProductprice=? where ProductID=?")){
                      statement.setInt(2, product.getProductID());
                      statement.setDouble(1, product.getTotalProductprice());
                    message=  statement.executeUpdate();
                  }catch(SQLException|ClassNotFoundException s){
                     System.err.println(s.getMessage());
                  }
        }return message;
    }

    @Override
    public boolean checkProductID(int productID) {
        
        try(Connection connection=Mysql.getConnection();
            Statement statement=connection.createStatement();
            ResultSet result= statement.executeQuery("select * from Inventory where productID="+productID)){
            if(result.next()){
                return true;
            }
        }catch(SQLException | ClassNotFoundException sql ){
            System.err.println(sql.getMessage());
          
        }return false;
    }

    @Override
    public Inventory[] viewAllProducts() {
        Inventory[] inventory=new Inventory[0];
        
         try(Connection connection=Mysql.getConnection();
             Statement statement=connection.createStatement();
             ResultSet result=statement.executeQuery("select * from Inventory")   ){
             while(result.next()){
                Inventory product=new Inventory(result.getInt(1),result.getString(2),result.getInt(3),result.getDouble(4),result.getDouble(5),result.getInt(6));
                inventory=Arrays.copyOf(inventory, inventory.length+1);
                inventory[inventory.length-1]=product;
                 
             }     
            
             }catch(SQLException|ClassNotFoundException exception){
                 System.err.println(exception.getMessage());
             }return inventory;
        
    }
    
    
    
}
