package MysqlDao;

import DAO.PaymentsDao;
import Pojo.Payments;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

public class PaymentsDaoImp implements PaymentsDao{

    @Override
    public int MakePayment(Payments payment) {
        int message=0;
        try(Connection connection=Mysql.getConnection();
            PreparedStatement statement=connection.prepareCall("insert into Payments (CustomerID,PaymentAmount,BalanceAmount) values (?,?,?)")){
            statement.setInt(1, payment.getCustomerID());
            statement.setDouble(2, payment.getPaymentAmount());
            statement.setDouble(3, payment.getBalanceAmount());
             message+=statement.executeUpdate();
             PreparedStatement statement2=connection.prepareCall("update Customers set BalanceAmount="+payment.getBalanceAmount()+"where ID="+payment.getCustomerID());
             message+=statement2.executeUpdate();
    }catch(SQLException|ClassNotFoundException sql){
            System.err.println(sql.getMessage());
        }return message;
    }
}
