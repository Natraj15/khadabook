package MysqlDao;

import DAO.CustomersDao;
import Pojo.Customers;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.Arrays;


public class CustomersDaoImp implements CustomersDao{

    @Override
    public String addCustomers(Customers customer) {
      
        try{  Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection=Mysql.getConnection();
             Statement statement=connection.createStatement();
          
            statement.executeUpdate("insert into Customers (Name,PhoneNumber,Address) values (" + "'" + customer.getName() + "'" + ',' + customer.getPhoneNumber() + ',' + "'" + customer.getAddress() + "'" + ")");
            System.out.println("#".repeat(10)+"successful");
            return "successfull";
        }catch(SQLException | ClassNotFoundException sql ){
            System.out.println(sql.getMessage());
            return sql.getMessage();
        }
        
    }
    
    @Override
    public Customers viewOneCustomer(int id){
       
               String url="jdbc:mysql://bassure.in:3306/Natrajdb";
               String user = "Natraj";
               String password = "Natraj@1501";
           Customers customer=null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection =DriverManager.getConnection(url,user, password);
            Statement statement=connection.createStatement();
            ResultSet result=statement.executeQuery("select * from Customers where ID="+id);
           
            while(result.next()){
                 
                customer=new Customers(result.getInt(1),result.getString(2),result.getLong(3),result.getString(4),result.getDouble(5));
            }
            
            
        }catch(SQLException|ClassNotFoundException sql){
            System.out.println(sql.getMessage());
        }
        return customer;
    }

    @Override
    public int updateCustomerName(int id, String name) {
        int message=0;
        try(Connection connection=Mysql.getConnection();
            PreparedStatement statement=connection.prepareCall("update Customers set Name=? where ID=?")){
            statement.setString(1, name);
            statement.setInt(2, id);
           message= statement.executeUpdate();
           
        }catch(SQLException|ClassNotFoundException sql){
            System.out.println(sql.getMessage()); 
        }return message;
    }

    @Override
    public boolean checkCustomerID(int id) {
        
            try{  
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection=Mysql.getConnection();
            Statement statement=connection.createStatement();
            ResultSet result= statement.executeQuery("select * from Customers where ID="+id);
            if(result.next()){
                return true;
            }
            //connection.close();
        }catch(SQLException | ClassNotFoundException sql ){
            System.err.println(sql.getMessage());
          
            
        }return false;
    }

    @Override
    public int updateCustomerAddress(int id, String address) {
           int message=0;
        try(Connection connection=Mysql.getConnection();
            PreparedStatement statement=connection.prepareCall("update Customers set Address=? where ID=?")){
            statement.setString(1, address);
            statement.setInt(2, id);
           message= statement.executeUpdate();
            
        }catch(SQLException|ClassNotFoundException sql){
            System.out.println(sql.getMessage()); 
        }return message;
    }

    @Override
    public int updateCustomerPhoneNumber(int id, long phoneNumber) {
        int message=0;
          try(Connection connection=Mysql.getConnection();
            PreparedStatement statement=connection.prepareCall("update Customers set PhoneNumber=? where ID=?")){
            statement.setLong(1, phoneNumber);
            statement.setInt(2, id);
           message= statement.executeUpdate();
            
        }catch(SQLException|ClassNotFoundException sql){
              System.out.println(sql.getMessage()); 
        }return message;
    }

    @Override
    public Customers[] viewAllCustomers() {
        Customers[] customers=new Customers[0];
        try(Connection connection=Mysql.getConnection();
             Statement statement=connection.createStatement();
             ResultSet result=statement.executeQuery("select * from Customers")   ){
             while(result.next()){
                Customers customer=new Customers(result.getInt(1),result.getString(2),result.getLong(3),result.getString(4),result.getDouble(5));
                customers=Arrays.copyOf(customers, customers.length+1);
                customers[customers.length-1]=customer;
                 
             }     
            
             }catch(SQLException|ClassNotFoundException exception){
                 System.err.println(exception.getMessage());
             }return customers;
    }
}
