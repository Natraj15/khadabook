package MysqlDao;

import DAO.OrdersDao;
import Pojo.Customers;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class OrdersDaoImp implements OrdersDao {

    @Override
    public int placeOrder(Orders order, Inventory[] products) {
        
        String status;
        if(products.length>0){
             status="confirmed";
        }else{
             status="pending";
        }
        
        int message = 0;
        double balanceAmount = 0.0;
        //update order 
        try (Connection connection = Mysql.getConnection()) {
            PreparedStatement satatement = connection.prepareCall("insert into Orders (CustomerID,TotalPrice,Status) values(" + order.getCustomerId() + ',' + order.getTotalPrice() +','+"'"+status+"'"+ ")");
            message = satatement.executeUpdate();
            for (LineItems lineitems : order.getLine()) {
                Statement ment = connection.createStatement();
                ResultSet result = ment.executeQuery("select max(OrderID) from Orders");
                while (result.next()) {
                    int Orderid = result.getInt(1);
                    int productId = lineitems.getProductId();
                    int Quantity = lineitems.getQuantity();
                    double oneProductprice=lineitems.getOneProductPrice();
                    double price = lineitems.getPrice();
                    PreparedStatement statement = connection.prepareCall("insert into  LineItems  (OrderID,ProductID,ProductQuantity,OneProductPrice,Price) values(?,?,?,?,?)");
                    statement.setInt(1, Orderid);
                    statement.setInt(2, productId);
                    statement.setInt(3, Quantity);
                    statement.setDouble(4, oneProductprice);
                    statement.setDouble(5, price);
                    message += statement.executeUpdate();
                }
                
                  

            }    
            //update customer balance amount
            if(products.length>0){
            Statement statement = connection.createStatement();
            ResultSet result1 = statement.executeQuery("select  BalanceAmount from Customers where ID=" + order.getCustomerId());
            while (result1.next()) {
                balanceAmount = result1.getDouble(1) + order.getTotalPrice();
            }
            PreparedStatement pps = connection.prepareCall("update Customers set BalanceAmount=? where ID=?");
            pps.setDouble(1, balanceAmount);
            pps.setInt(2, order.getCustomerId());
            message += pps.executeUpdate();
            //update inventory 
            for (Inventory product : products) {

                PreparedStatement pre = connection.prepareCall("update Inventory set ProductQuantity=?,SoldProducts=? where ProductID=?");
                pre.setInt(1, product.getProductQuantity());
                pre.setInt(2, product.getSoldProductsQuantity());
                pre.setInt(3, product.getProductID());
                message += pre.executeUpdate();
            }
          }
        } catch (SQLException | ClassNotFoundException s) {
            System.err.println(s.getMessage());
            s.printStackTrace();
        }
        return message;
    }

    @Override
    public Orders[] viewOrdersbyCustomerID(int customerID) {

        Orders[] order = new Orders[0];
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = Mysql.getConnection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where CustomerID=" + customerID);
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5),result.getDouble(6));
                }
                order = Arrays.copyOf(order, order.length + 1);
                order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));
                lineitem = Arrays.copyOf(lineitem, 0);
            }

        } catch (SQLException | ClassNotFoundException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }

        return order;
    }

    @Override
    public Orders[] viewPendingOrders() {
       
           Orders[] order = new Orders[0];
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = Mysql.getConnection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where Status='pending'");
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5),result.getDouble(6));
                }
                order = Arrays.copyOf(order, order.length + 1);
                order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));
                lineitem = Arrays.copyOf(lineitem, 0);
            }

        } catch (SQLException | ClassNotFoundException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }

        return order;
        
    }

    @Override
    public int confimeOrders(Inventory[] products, Customers customer,int orderid) {
        int message=0;
       
        try(Connection connection=Mysql.getConnection()){
        
         for (Inventory product : products) {

                PreparedStatement pre = connection.prepareCall("update Inventory set ProductQuantity=ProductQuantity-?,SoldProducts=SoldProducts+? where ProductID=?");
               
                pre.setInt(1, product.getProductQuantity());
                pre.setInt(2, product.getProductQuantity());
//                pre.setInt(2, product.getSoldProductsQuantity());
                pre.setInt(3, product.getProductID());
                message += pre.executeUpdate();
            }
         
        
         
           PreparedStatement pps = connection.prepareCall("update Customers set BalanceAmount=? where ID=?");
            pps.setDouble(1,customer.getBalanceAmount() );
            pps.setInt(2, customer.getCustomerID());
            message += pps.executeUpdate();
        
         
         
         PreparedStatement statement=connection.prepareCall("update Orders set Status='confirmed' where Status='pending'and orderID=?");
         statement.setInt(1, orderid);
         message+=statement.executeUpdate();
         
        }catch(SQLException|ClassNotFoundException sql){
            System.out.println(sql.getMessage());
        }return message;
    }

    @Override
    public Orders viewOrdersbyOrderID(int orderId) {
        
        Orders order = null;
        LineItems[] lineitem = new LineItems[0];
        
        try (Connection con = Mysql.getConnection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where OrderID=" + orderId);
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5),result.getDouble(6));
                }
               
                order = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));
               
            }

        } catch (SQLException | ClassNotFoundException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }

        return order;
        
    }

    @Override
    public Orders[] viewCustomerPendingOrders(int customerID) {
        
             Orders[] order = new Orders[0];
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = Mysql.getConnection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where Status='pending' and customerID="+customerID);
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5),result.getDouble(6));
                }
                order = Arrays.copyOf(order, order.length + 1);
                order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));
                lineitem = Arrays.copyOf(lineitem, 0);
            }

        } catch (SQLException | ClassNotFoundException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }

        return order;
        
    }

}
