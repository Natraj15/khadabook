package Statistics;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import Pojo.Inventory;
import Pojo.Orders;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

@WebServlet("/StatisticsServlet")
public class StatisticsServlet extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

         response.setContentType("text/html;charset=UTF-8");
         PrintWriter out = response.getWriter();
          out.println("<body style='background-color:aliceblue'>");
        int option = Integer.parseInt(request.getParameter("option"));
        HttpSession session=request.getSession();
        KhatabookDao khatabook = new MysqlConnection();
        switch (option) {
            case 1:
                Date date1 = Date.valueOf(request.getParameter("date1"));
                Date date2 = Date.valueOf(request.getParameter("date2"));

                Orders[] orders = khatabook.getStatistics().mostPurchasedCustomerbyStartDatetoEndDate(date1, date2);
                if (orders.length > 0) {
                  session.setAttribute("mostPurchasedCustomers", orders);
                  RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/Statistics/MostPurchasedCustomers.jsp");
                  dispatcher.forward(request, response);
                   /* out.print("""
                        <html>
                        <head>
                        <style>
                        table,td,th {
                          border:1px solid black;
                          border-style: dotted;
                        }
                        </style>
                        </head>
                        <body>
                        <table >
                          <tr>
                             <th>customerID</th>
                             <th>TotalPrice</th>
                          </tr>
                          
                        """
                    );
                    for (Orders order : orders) {
                        out.print("<tr>");
                        out.print("<td>" + order.getCustomerId() + "</td>");
                        out.print("<td>" + order.getTotalPrice() + "</td>");
                        out.print("</tr>");
                    }
                    out.print("</table>");*/

                } else {
                    out.print("<p>No orders are found</p>");

                }break;
            case 2:

                Customers[] customers = khatabook.getStatistics().paidCustomers();
                if (customers.length > 0) {
                 
                    session.setAttribute("CustomersPayments", customers);
                    RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/Statistics/CustomerPaymentDetails.jsp");
                    dispatcher.forward(request, response);
                    /*out.print("""
                             <style>
                                        table,td,th {
                                                    border:1px solid black;
                                                      border-style: dotted;
                                                  }
                            </style>
                              <table >
                                       <tr>
                                       <th>customerID</th>
                                       <th>BalanceAmount</th>
                                      </tr>
                            """);

                    for (Customers customer : customers) {
                        out.print("<tr>");
                        out.print("<td>" + customer.getCustomerID() + "</td>");
                        out.print("<td>" + customer.getBalanceAmount() + "</td>");
                        out.print("</tr>");
                    }
                    out.print("</table>");*/

                } else {
                    out.print("<p>No customers</p> ");
                }break;
            case 3:
                Customers[] unPaidCustomers = khatabook.getStatistics().unPaidCustomers();
                if (unPaidCustomers.length > 0) {
                    
                    session.setAttribute("CustomersPayments", unPaidCustomers);
                    request.getRequestDispatcher("WEB-INF/ViewFolder/Statistics/CustomerPaymentDetails.jsp").forward(request, response);
                    
                   /* out.print("""
                             <style>
                                        table,td,th {
                                                    border:1px solid black;
                                                      border-style: dotted;
                                                  }
                            </style>
                              <table >
                                       <tr>
                                       <th>customerID</th>
                                       <th>BalanceAmount</th>
                                      </tr>
                            """);

                    for (Customers customer : paidCustomers) {
                        out.print("<tr>");
                        out.print("<td>" + customer.getCustomerID() + "</td>");
                        out.print("<td>" + customer.getBalanceAmount() + "</td>");
                        out.print("</tr>");
                    }
                    out.print("</table>");*/

                } else {
                    out.print("<p>No customers</p> ");
                }break;
            case 4:
                Inventory[] products = khatabook.getStatistics().mostSoldProducts();
                if (products.length > 0) {
                    
                    session.setAttribute("soldProducts", products);
                    request.getRequestDispatcher("WEB-INF/ViewFolder/Statistics/SoldProducts.jsp").forward(request, response);
                   
                    /*out.print("""
                             <style>
                                        table,td,th {
                                                    border:1px solid black;
                                                      border-style: dotted;
                                                  }
                            </style>
                              <table >
                                       <tr>
                                       <th>ProductID</th>
                                       <th>Sold Products</th>
                                      </tr>
                            """);

                    for (Inventory product : products) {
                        out.print("<tr>");
                        out.print("<td>" + product.getProductID() + "</td>");
                        out.print("<td>" + product.getSoldProductsQuantity() + "</td>");
                        out.print("</tr>");
                    }
                    out.print("</table>");*/

                } else {
                    out.print("some error");

                }break;

            case 5:
                Date firstdate = Date.valueOf(request.getParameter("firstdate"));
                Date seconddate = Date.valueOf(request.getParameter("secondDate"));

                Orders[] orders2 = khatabook.getStatistics().OrdersListByStartDatetoEndDate(firstdate, seconddate);
                if(orders2.length>0){
                     session.setAttribute("orders", orders2);
                    RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewOrders.jsp");
                     dispatcher.forward(request, response);
                    /*for (Orders order : orders2) {
                     out.println("_".repeat(22)+"<br>");  
                                 out.println("<br>");
                                 out.println("Order ID &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : "+order.getOrderId()+"<br>");
                                 out.println("Customer ID : "+order.getCustomerId()+"<br>");
                                 out.println("Date  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    : "+order.getDate()+"<br>");
                              for(LineItems lineitems: order.getLine()){
                                 out.println("LineItem ID  : "+lineitems.getLineItemId()+"<br>");  
                                 out.println("product ID &nbsp;&nbsp; : "+lineitems.getProductId()+"<br>");
                                 out.println("Quantity &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   : "+lineitems.getQuantity()+"<br>");
                                 out.println("price   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    : "+lineitems.getPrice()+"<br>");
                                 }
                                 out.println("-".repeat(31)+"<br>");
                                 out.println("Total Price &nbsp;&nbsp; : "+order.getTotalPrice()+"<br>");
                                 out.println("-".repeat(31)+"<br>");
                                      out.println("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #Thank You# &nbsp;&nbsp;&nbsp;<br>");
                                 out.println("_".repeat(22)+"<br>");*/
                                // out.println(" ");
                    //    out.println("<p> </p>");
                 //}
                }else{
                        out.print("<p>No Orders</p>");
                        }
                break;
            case 6:
                
                      Inventory[] products1 = khatabook.getStatistics().lessSoldProducts();
                if (products1.length > 0) {
                   
                    session.setAttribute("soldProducts", products1);
                    
                    request.getRequestDispatcher("WEB-INF/ViewFolder/Statistics/SoldProducts.jsp").forward(request, response);
                   
                    /*
                    out.print("""
                             <style>
                                        table,td,th {
                                                    border:1px solid black;
                                                      border-style: dotted;
                                                  }
                            </style>
                              <table >
                                       <tr>
                                       <th>ProductID</th>
                                       <th>Sold Products</th>
                                      </tr>
                            """);

                    for (Inventory product : products1) {
                        out.print("<tr>");
                        out.print("<td>" + product.getProductID() + "</td>");
                        out.print("<td>" + product.getSoldProductsQuantity() + "</td>");
                        out.print("</tr>");
                    }
                    out.print("</table>");*/

                } else {
                    out.print("some error");

                }
                
        }

        out.print("<a href='http://localhost:8080/KhatabookProject/HomePage.html'>Home</a><br>");

        out.print(""" 
                         </body>
                         </html>
                         """);
    }

}
