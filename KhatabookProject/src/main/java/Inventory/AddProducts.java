package Inventory;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/addproducts")
public class AddProducts  extends HttpServlet{

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String productName=request.getParameter("productname");
        int productQuantity=Integer.parseInt(request.getParameter("productQuantity"));
        double oneProductPrice=Double.parseDouble(request.getParameter("oneproductprice"));
        double totalProductPrice=Double.parseDouble(request.getParameter("totalproductprice"));
        PrintWriter out=response.getWriter();
        Inventory product=new Inventory(productName,productQuantity,oneProductPrice,totalProductPrice);
        
         KhatabookDao khatabook=new MysqlConnection();
        HttpSession session=request.getSession();
        try {
           String message= khatabook.getInventory().addProducts(product);
           if(message.equals("successfull")){
                session.setAttribute("message", "Product Added Sucessfully");
                response.sendRedirect("SendMessage2");
           }else{
                
                System.out.println(message);
                session.setAttribute("message", "Insersion Faild");
                response.sendRedirect("SendMessage2");
           }
           
        } catch (Exception ex) {
           out.print(ex.getMessage());
        }
    }
    
    
}
