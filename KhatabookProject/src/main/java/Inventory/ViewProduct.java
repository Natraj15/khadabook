package Inventory;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet("/ViewProduct")
public class ViewProduct extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<body style='background-color:aliceblue'>");
        int option = Integer.parseInt(request.getParameter("option"));
        KhatabookDao khatabook = new MysqlConnection();
        HttpSession session = request.getSession();
        switch (option) {
            case 1:

                Inventory[] inventory = khatabook.getInventory().viewAllProducts();
                if (inventory.length > 0) {

                    session.setAttribute("products", inventory);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ViewFolder/ViewProducts.jsp");
                    dispatcher.forward(request, response);

                    /* out.print("<table border=1 >");
            
                             out.print(
                                    """ 
                                       <tr>      
                                       <th> ProductID </th>
                                       <th> ProductName</th>
                                       <th> ProductQuantity</th>
                                       <th> OneProductPrice</th>
                                       <th> TotalProductPrice</th>
                                       <th> SoldProducts</th>
                                     </tr>  
                                     """
                           );
               for(Inventory products:inventory){
                out.println("<tr>");
              
               out.println("<td>"+products.getProductID() +"</td>");
               out.println("<td>"+products.getProductName()+"</td>");
               out.println("<td>"+products.getProductQuantity()+"</td>");
               out.println("<td>"+products.getOneProductprice()+"</td>");
               out.println("<td>"+products.getTotalProductprice()+"</td>");
               out.println("<td>"+products.getSoldProductsQuantity()+"</td>");
                
               out.println("</tr>");     
               }
               out.println("</table>");*/
                } else {
                    System.err.println("some error");
                }

                break;

            case 2:

                int productId = Integer.parseInt(request.getParameter("productID"));

                Inventory product = khatabook.getInventory().viewOneProduct(productId);

                if (Objects.nonNull(product)) {

                    Inventory[] products = new Inventory[1];
                    products[0] = product;
                    session.setAttribute("products", products);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ViewFolder/ViewProducts.jsp");
                    dispatcher.forward(request, response);

                    /*   out.print("<table border=1 >");
            
                             out.print(
                                    """ 
                                       <tr>      
                                       <th> ProductID </th>
                                       <th> ProductName</th>
                                       <th> ProductQuantity</th>
                                       <th> OneProductPrice</th>
                                       <th> TotalProductPrice</th>
                                       <th> SoldProducts</th>
                                     </tr>  
                                     """
                           );
         
                out.println("<tr>");
              
               out.println("<td>"+product.getProductID() +"</td>");
               out.println("<td>"+product.getProductName()+"</td>");
               out.println("<td>"+product.getProductQuantity()+"</td>");
               out.println("<td>"+product.getOneProductprice()+"</td>");
               out.println("<td>"+product.getTotalProductprice()+"</td>");
               out.println("<td>"+product.getSoldProductsQuantity()+"</td>");
                
               out.println("</tr>");     
            
               out.println("</table>");*/
                } else {
                    out.println("<p style='color:red;'> ID is not Exist </p>");

                }
        }
        out.println("<a href='http://localhost:8080/KhatabookProject/HomePage.html'>Home</a><br>");
        out.println("</body>");
    }

}
