package Inventory;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Inventory;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UpdateProductsServlet", urlPatterns = {"/UpdateProductsServlet"})
public class UpdateProductsServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProductsServlet</title>");            
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
            
            int id=Integer.parseInt(request.getParameter("productID"));
            String field=request.getParameter("choose");
            String value=request.getParameter("value");
            KhatabookDao khatabook=new MysqlConnection();
            boolean cheak=khatabook.getInventory().checkProductID(id);
            Inventory products=new Inventory();
           
            int message=0;
            if(cheak){
            products.setProductID(id);
            switch(field){
                case "Name" :
                    products.setProductName(value);
                    int option=1;
                   message=  khatabook.getInventory().updateProduct(products, option);
                    
                    break;
                    
                case "Quantity":
                    int quantity=Integer.parseInt(value);
                    products.setProductQuantity(quantity);
                      option=2;
                    message= khatabook.getInventory().updateProduct(products, option);
                    break;
                case "OneProductPrice":
                    double onePrice=Double.parseDouble(value);
                    products.setOneProductprice(onePrice);
                    option=3;
                   message= khatabook.getInventory().updateProduct(products, option);
                    
                    break;
                case "TotalProductPrice":
                    option=4;
                    double totalPrice=Double.parseDouble(value);
                    products.setTotalProductprice(totalPrice);
                   message= khatabook.getInventory().updateProduct(products, option);
                    
                    break;
                    
            }
            HttpSession session=request.getSession();
            if(message>0){
                String message2=field+" Updated Sucessfully";
                session.setAttribute("message", message2);
                response.sendRedirect("SendMessage2");
            }else{
                session.setAttribute("message", "Updation Failed");
                response.sendRedirect("SendMessage2");
            }
            
            
        }else{
                out.println("<p style='color:red;'><p>");
                }
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
