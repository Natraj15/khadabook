package com.mycompany.khatabookproject;

import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import Pojo.Customers;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

@WebServlet("/ViewCustomerServlet")
public class ViewCustomerSevlet extends HttpServlet{
    
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
         PrintWriter out=response.getWriter(); 
         response.setContentType("text/html;charset=UTF-8");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>View Customer</title>");  
        
            out.println("</head>");
            out.println("<body style='background-color:aliceblue'>");
        
        KhatabookDao khatabook=new MysqlConnection();
          HttpSession session=request.getSession();
        
        int option=Integer.parseInt(request.getParameter("option"));
        switch(option){
            case 1:
       int id=Integer.parseInt(request.getParameter("customerID"));
        Customers customer=khatabook.getCustomers().viewOneCustomer(id);
     
       if(Objects.nonNull(customer)){  
          
           session.setAttribute("customer", customer);
           RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewCustomer.jsp");
           dispatcher.forward(request, response);
   
   
       }else{
           out.println("<p style='color:red;'> ID is not Exist (Create Customer Account)</p>");
           
       }break;
       case 2: 
       
         Customers[] customers=  khatabook.getCustomers().viewAllCustomers();
         
           if(customers.length>0){
               
           session.setAttribute("customers", customers);
           RequestDispatcher dispatcher=request.getRequestDispatcher("WEB-INF/ViewFolder/ViewAllCustomer.jsp");
           dispatcher.forward(request, response);
        
               
           }else{
               out.print("<p style='color:red;'>some error<p>");
           }
           
      }
        out.print(" <a href='HomePage.html'>Home</a><br>");
        out.print("</body>");
        out.print("</html>");
      
    }
}
