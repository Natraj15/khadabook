package com.mycompany.khatabookproject;

import Pojo.Customers;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import DAO.KhatabookDao;
import MysqlDao.MysqlConnection;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;



@WebServlet("/Addcutomer")
public class AddCustomerServlet extends HttpServlet{

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
         response.setContentType("text/html;charset=UTF-8");
        String name=request.getParameter("CustomerName");
        long phoneNumber=Long.parseLong(request.getParameter("PhoneNumber"));
        String address=request.getParameter("Address");
        PrintWriter out=response.getWriter();
        out.println("<body style='background-color:aliceblue'>");
        
        Customers customer=new Customers(name,phoneNumber,address);
        HttpSession session=request.getSession();
        KhatabookDao khatabook=new MysqlConnection();
        
        try {
           String message= khatabook.getCustomers().addCustomers(customer);
           if(message.equals("successfull")){
                
                 session.setAttribute("message", "Customer Added Sucessfully");
                 response.sendRedirect("SendMessage2");
           }else{
//                 out.println("<p style='color:red;'>Sorry 'not inserted'</p>");
//                 out.println("<p style='color:red;'>"+message+"</p>");
                 System.out.println(message);
                 
                 session.setAttribute("message", "Insertion Failed");
                 response.sendRedirect("SendMessage2");
           }
           
        } catch (Exception ex) {
           out.print(ex.getMessage());
        }
         out.print(" <a href='HomePage.html'>Home</a><br>");
         out.print("</body>");
    }
    
    
}
