<%-- 
    Document   : ViewProducts
    Created on : 30-Mar-2023, 3:17:32 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Inventory"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
             td,th{
                font-size:15px;
                    border-style: dotted;
            }
            
        </style>
    </head>
    <body style="background-color:aliceblue">

        <%
            Inventory[] inventory=(Inventory[])session.getAttribute("products");
            if(inventory.length>0){
        
        %>
              
             <table border=1 >
            
                             
                                       <tr>      
                                       <th>ProductID </th>
                                       <th>ProductName</th>
                                       <th>ProductQuantity</th>
                                       <th>OneProductPrice</th>
                                       <th>TotalProductPrice</th>
                                       <th>SoldProducts</th>
                                     </tr>  
                         
             <%  for(Inventory products:inventory){ %>
                <tr>
              
               <td><%=products.getProductID()%></td>   
               <td><%=products.getProductName()%></td>         
               <td><%=products.getProductQuantity()%></td>   
               <td><%=products.getOneProductprice()%></td>     
               <td><%=products.getTotalProductprice()%></td>     
               <td><%=products.getSoldProductsQuantity()%></td>
                
               </tr>     
             <%  } %>
               </table>
        
         
       
        
        
        <%
            }else{
          out.println("<p>Something error</p>");
           }
        
        
        %>
      <a href='HomePage.html'>Home</a><br>
    </body>
</html>
