<%-- 
    Document   : ViewCustomer
    Created on : 28-Mar-2023, 12:19:14 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Customers"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Khatabook</title>
        <style>
            
            td,th{
                font-size:20px;
                    border-style: dotted;
            }
            
            .table1 {
                  		        
                                        border-style:ridge;
                                        position: absolute;
                 			left: 0;
                 			right: 0;
                 			bottom:0;
                 			margin-left:180px;
                                        margin-bottom:230px;
                 			text-align: center;
                 		}
                
            
        </style>
    </head>
    <body style="background-color:aliceblue">
       
        <%
           Customers customer=(Customers)session.getAttribute("customer");
           if(customer!=null){
        %>
        
        <table class='table1'>
            
             
               <tr>
              
              <td> CustomerID </td><td><%=customer.getCustomerID()%></td></tr>
              <tr><td> CustomerName</td><td><%=customer.getName()%></td></tr>
              <tr><td> PhoneNumber</td><td><%=customer.getPhoneNumber()%></td></tr>
              <tr><td> Address</td><td><%=customer.getAddress()%></td></tr>
              <tr> <td> BalanceAmount</td><td><%=customer.getBalanceAmount()%></td>
                
              </tr>    
            
               </table>
        <%
            }else{
             out.print("<p>No Customers Found</p>");
               }
         
        %>
        <a href='HomePage.html'>Home</a><br>
        
    </body>
</html>
