<%-- 
    Document   : ViewPaidCustomers
    Created on : 30-Mar-2023, 9:57:16 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Customers"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Khatabook</title>
        <style>
            td,th {
                border:1px solid black;
                border-style: dotted;
                text-align: center;
            }
            table{
                border-style: dashed;
            }
        </style>
    </head>
    <body style="background-color:aliceblue" >

        <%
               Customers[] customers=(Customers[])session.getAttribute("CustomersPayments");
               if(customers.length>0){
        %>

        <table>
            <tr>
                <th>Customer ID</th>
                <th>Balance Amount</th>
            </tr>

            <%   for (Customers customer : customers) {%>
            <tr>
                <td><%=customer.getCustomerID()%> </td>
                <td><%=customer.getBalanceAmount()%></td>
            </tr>
            <% }%>
        </table>

        <%
            }else{
          out.println("<p>Some Error</p>");
}
        %>
      <a href="HomePage.html">Home</a>
    </body>
</html>
