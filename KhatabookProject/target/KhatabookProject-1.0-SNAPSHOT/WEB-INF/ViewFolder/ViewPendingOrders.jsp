<%-- 
    Document   : ViewPendingOrders
    Created on : 30-Mar-2023, 8:44:38 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Pojo.Orders"%>
<%@page import="Pojo.LineItems"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
           

            th,td{
                border-style: dotted;
            }
            .no-border{
                border:none;
            }
            .right{
                
                 text-align: right;
                padding-right:  20px;
            }
            .center{
                text-align: center;
                border-style: dashed;
               
            }
            .uderline{
                  
                text-decoration-line: underline;
                text-decoration-style: wavy;
                text-decoration-color: royalblue;

            
            }
            
        </style>

    </head>
    <body style="background-color:aliceblue">


        <%  
            Orders[] orders=(Orders[])session.getAttribute("orders");               
            if(orders.length>0){
          
        
        for(Orders order:orders){
        int count=1;
      %>  

        <table border="1px" style="margin-left:  20px;">
            <tr>

                <th colspan="3" class="uderline">Welcome</th>
                <td  colspan="2">
                    Order ID:<%=order.getOrderId()%><br>
                    Customer ID:<%=order.getCustomerId()%><br>
                    Date:<%=order.getDate()%><br>
                </td>
            </tr>
           
            <tr >
                <th style="border-style: groove"> S.No.</th>
                <th style="border-style: groove"> Product ID</td>
                <th style="border-style: groove"> Quantity</th>
                <th style="border-style: groove"> Rate</th>
                <th style="border-style: groove"> Amount</th>
            </tr>
            <%for(LineItems lineitems: order.getLine()){ %>

            <tr>
                <td style="border-style: groove; text-align: center;"><%=count++%></td>
                <td style="border-style: groove; text-align: center;"><%=lineitems.getProductId()%></td>
                <td style="border-style: groove; text-align: center;"><%=lineitems.getQuantity()%></td>
                <td style="border-style: groove ;text-align: center;"><%=lineitems.getOneProductPrice()%></td>
                <td style="border-style: groove; text-align: center;"><%=lineitems.getPrice()%></td>

            </tr>
            
            <%  } %>
            <tr><td class="right"  colspan="5" style="border-style: hidden"> Total Price:<%=order.getTotalPrice()%></td></tr>
            <tr>
                <th colspan="5"  class="center" >#Thank You#</th>

            </tr>
            
            

        </table>
       
          <div style='margin-left:250px;'>
              <form action="OrderConfirmServlet?OrderID=<%=order.getOrderId()%>" method=post>
                               <input type="submit" name="submit" value="confim" ><br>  
                                           </form>
                                           </div><br>
                                               

        <%
            }   
            
            }else{
              out.println("<p>some error<p>");
             }
        
        %>
    </body>
</html>

