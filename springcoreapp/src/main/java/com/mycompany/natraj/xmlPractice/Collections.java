package com.mycompany.natraj.xmlPractice;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Collections {
    
    private List list;
    private Map map;
    private Set set;
    private Properties properties;

    public Collections() {
    }

    public Collections(List list, Map map, Set set, Properties properties) {
        this.list = list;
        this.map = map;
        this.set = set;
        this.properties = properties;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Set getSet() {
        return set;
    }

    public void setSet(Set set) {
        this.set = set;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Collections{" + "list=" + list + ", map=" + map + ", set=" + set + ", properties=" + properties + '}';
    }
    
    
    
    
}
