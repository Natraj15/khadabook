package com.mycompany.natraj.xmlPractice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {
  private String url;  
  private String userName;
  private String password;
  private String driver;
  private Connection connection;

    public JdbcConnection() {
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public JdbcConnection(String url, String userName, String password, String driver, Connection connection) {
        this.url = url;
        this.userName = userName;
        this.password = password;
        this.driver = driver;
        this.connection = connection;
    }

    
    public void getAllDetails() throws ClassNotFoundException,SQLException{
        
        Class.forName(driver);
        
        Connection connection=DriverManager.getConnection(url,userName,password);
        Statement statement=connection.createStatement();
        ResultSet result=statement.executeQuery("select * from Students");
        
        while(result.next()){
            System.out.println(result.getInt(1)+" "+result.getString(2)+" "+result.getString(3)+"");
        }
        
        
    }
    
    
}
