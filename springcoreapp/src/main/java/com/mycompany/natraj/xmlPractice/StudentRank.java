package com.mycompany.natraj.xmlPractice;

public class StudentRank {
    
    String rank;
    String result;

    public StudentRank() {
        
    }

    public StudentRank(String rank, String result) {
        this.rank = rank;
        this.result = result;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "StudentRank{" + "rank=" + rank + ", result=" + result + '}';
    }
    
    
}
