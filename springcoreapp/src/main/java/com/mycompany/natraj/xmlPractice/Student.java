package com.mycompany.natraj.xmlPractice;

public class Student {
    
    private String name;
    private int id;
    private StudentRank rank;

    public Student() {
    }

    public Student(StudentRank rank) {
        this.rank = rank;
    }
    
    public Student(String name, int id, StudentRank rank) {
        this.name = name;
        this.id = id;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StudentRank getRank() {
        return rank;
    }

    public void setRank(StudentRank rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", id=" + id + ", rank=" + rank + '}';
    }
    
    
}
