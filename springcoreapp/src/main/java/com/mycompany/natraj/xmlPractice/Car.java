package com.mycompany.natraj.xmlPractice;

public class Car {
    
    String colour;
    double price;

    public Car() {
    }

    public Car(String colour, double price) {
        this.colour = colour;
        this.price = price;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" + "colour=" + colour + ", price=" + price + '}';
    }
    
    
    
}
