package com.mycompany.natraj.xmlPractice;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlJava {
    
    public static void main1(String[] args) {
        //collecitons
       ApplicationContext context=new ClassPathXmlApplicationContext("Collections.xml");
        Collections c=context.getBean("collection",Collections.class);
        
        System.out.println(c);
       
        
    }
    public static void main(String[] args) throws ClassNotFoundException,SQLException{
        //jdbc-connection
        
        ApplicationContext context=new ClassPathXmlApplicationContext("jdbc-connection.xml");
       // JdbcConnection con=context.getBean("resource",JdbcConnection.class);
        
        //con.getAllDetails();
        System.out.println(" ");
        //jdbc-connection-BasicDataSource
        DataSource datasource=(DataSource)context.getBean("myDataSource", DataSource.class );
        
        Connection connection=datasource.getConnection();
        Statement statement=connection.createStatement();
        ResultSet result=statement.executeQuery("select * from Students");
        
        while(result.next()){
            System.out.println(result.getInt(1)+" "+result.getString(2)+" "+result.getString(3)+"");
        }
        datasource.getConnection().close();
        
    }
    
    public static void main4(String[] args) {
         //inheritence
        
          ApplicationContext context=new ClassPathXmlApplicationContext("inheritence.xml");
        
        Car car=context.getBean("class1",Car.class);
        System.out.println(car);
        
        Jackware jack=context.getBean("class2",Jackware.class);
        
        System.out.println(jack);
        
    }
    public static void main5(String[] args) {
        //autowired
          ApplicationContext context=new ClassPathXmlApplicationContext("student.xml");
          Student student=context.getBean("student",Student.class);
          System.out.println(student);
         // inner bean
          
         Student student1=context.getBean("student1",Student.class);
         System.out.println(student1);
         
        Student student2=context.getBean("student2",Student.class);
         System.out.println(student2);
         
           Student student3=context.getBean("student3",Student.class);
         System.out.println(student3);
          
    }
  
        
    
   
}
