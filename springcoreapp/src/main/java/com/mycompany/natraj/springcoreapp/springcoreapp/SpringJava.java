package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringJava {

    public static void main1(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");

        Pen pen = new Pen();

        System.out.println(pen);

        Pen b = context.getBean("e1", Pen.class);
        System.out.println(b);

    }

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        ApplicationContext context = new ClassPathXmlApplicationContext("jdbc-connection.xml");
        // JdbcConnection con=context.getBean("resource",JdbcConnection.class);

        //con.getAllDetails();
        System.out.println(" ");
        //jdbc-connection-BasicDataSource
        DataSource datasource = (DataSource) context.getBean("myDataSource", DataSource.class);

        Connection connection = datasource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result = statement.executeQuery("select * from Students");

        while (result.next()) {
            System.out.println(result.getInt(1) + " " + result.getString(2) + " " + result.getString(3) + "");
        }
        datasource.getConnection().close();

    }
}
 

