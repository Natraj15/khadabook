package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
    
    @Autowired
    StudentRepository studentRepository;
    
    public List<Students> getStudents(){
        return studentRepository.getStudents();
    }
}
