package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.util.List;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringJdbcDemo {
    
    public static void main(String[] args) {
        
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        
        StudentComponent students = context.getBean(StudentComponent.class);
        
        List<Students> allStudents = students.getAllStudents();
        
        allStudents.stream().forEach(System.out::println);
        
        context.registerShutdownHook();
        
    }
    
}
