package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StudentController {
    
    @Autowired
    StudentService studentService;
    
    @GetMapping(path="/students")
    @ResponseBody
    public  String showAllStudents(){
        return studentService.getStudents().toString();
    }
    
    
}
