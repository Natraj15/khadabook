package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class StudentComponent {
    
    private JdbcTemplate jdbcTemplet;

    public StudentComponent(DataSource dataSource) {
        this.jdbcTemplet = new JdbcTemplate(dataSource);
    }
    
    public List<Students> getAllStudents(){
       
        return jdbcTemplet.query("select * from Students", new StudentRowMapper());
    }
}
