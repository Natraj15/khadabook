package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class StudentRepository {
    
     private JdbcTemplate jdbcTemplate;


    public StudentRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public List<Students> getStudents(){
        
        return this.jdbcTemplate.query("select * from Students", new StudentRowMapper());
    }
     
     
}
