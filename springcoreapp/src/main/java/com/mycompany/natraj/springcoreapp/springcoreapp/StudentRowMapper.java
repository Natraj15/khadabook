package com.mycompany.natraj.springcoreapp.springcoreapp;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class StudentRowMapper implements RowMapper<Students> {
    
    @Override
    public Students mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Students(rs.getInt(1), rs.getString(2), rs.getString(3));
    }
    
}
