
package com.mycompany.web.app;


public class Customer {
    
    
    private int customerID;
    private String name;
    private long phoneNumber;
    private String address;
    private double balanceAmount;

    public Customer() {
    }

    public Customer(int customerID, String name, long phoneNumber, String address, double balanceAmount) {
        this.customerID = customerID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.balanceAmount = balanceAmount;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerID=" + customerID + ", name=" + name + ", phoneNumber=" + phoneNumber + ", address=" + address + ", balanceAmount=" + balanceAmount + '}';
    }
    
    
}
