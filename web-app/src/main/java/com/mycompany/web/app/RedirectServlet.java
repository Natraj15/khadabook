package com.mycompany.web.app;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;



@WebServlet("/redirect")
public class RedirectServlet extends GenericServlet{

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
          response.getWriter().print(
        """
         <html>
         <body>
           <h1>Sucessfully Submitted</h1>      
          </body>
         </html>
        """);

    }
    
}
