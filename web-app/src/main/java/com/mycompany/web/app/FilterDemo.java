package com.mycompany.web.app;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import java.io.IOException;
import java.time.LocalTime;

public class FilterDemo implements Filter{

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
//        LocalTime time=LocalTime.now();
        
        System.out.println("pre"+LocalTime.now());
       
        
        chain.doFilter(request, response);
        
        System.out.println("post"+LocalTime.now());
    }
    
}
