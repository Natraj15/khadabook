
package com.mycompany.web.app;


import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Enumeration;


public class ProductsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductsServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductsServlet at " + request.getContextPath() + "</h1>");
           
            String chara= request.getCharacterEncoding();
            out.println(request.getContentLength());
            out.println(request.getMethod());
            out.println(request.getContentType());
            out.println(request.getProtocol());
            out.println(request.getContentLengthLong());
            out.println(chara);
            out.println(request.getScheme());
            //out.println(request.);
            
            
            Enumeration<String> enumeration1= request.getHeaderNames();
            out.println("<table style='border:1px solid black'>");
          
            while(enumeration1.hasMoreElements()){
             String headerNames=enumeration1.nextElement();   
                
            out.println("<tr>");
            out.println("<th style='border:1px solid black' >"+headerNames+"</th>");
            
            Enumeration<String> enumeration2=request.getHeaders(headerNames);
           //  while(enumeration2.hasMoreElements()){
             String headers=enumeration2.nextElement();
             out.println("<td style='border:1px solid black' >"+headers+"</td>");
             out.print("</tr￼>");
           //  }
            }
            
            out.print("</table>");
          
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
