package com.mycompany.web.app;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.Servlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Objects;

//@WebServlet("/time")
public class TimeServlet extends GenericServlet
{

//    @Override
//    public void init(ServletConfig sc) throws ServletException {
//        System.out.println("#".repeat(10)+"servlet Initialized");
//    }
//
//    @Override
//    public ServletConfig getServletConfig() {
//              return null;
//    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        
        response.getWriter().println("It's Now :"+LocalDateTime.now());
        
        
          
        ServletConfig config=this.getServletConfig();
       // config.getInitParameter("");
        ServletContext context=this.getServletContext();
       String mobile= context.getInitParameter("Mobile");
        String name= config.getInitParameter("Name");
        if(Objects.nonNull(config)){
             response.getWriter().println("Name :"+name);
             response.getWriter().println("Mobile :"+mobile);
             
        }else {
            response.getWriter().print("Object is null");
        }
        
         
       
//        String name="Natraj";
//        request.setAttribute("names", name);
//        RequestDispatcher rd=request.getRequestDispatcher("JSTL.jsp");
//        rd.forward(request, responce);
        
    }

    @Override
    public String getServletInfo() {
         return "gives time on server";
    }

//    @Override
//    public void destroy() {
//        System.out.println("#".repeat(10)+"servlet Destroy");
//    }
    
    
}
