package com.mycompany.web.app;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

public class SessionListener2 implements HttpSessionListener{

    @Override
    public void sessionCreated(HttpSessionEvent se) {
         String name="jobin";
        //ServletContext context=se.getSession().getServletContext();
        
       // context.setAttribute("cusnames", name);
       HttpSession session=se.getSession();
       session.setAttribute("names", name);
       
        
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
         HttpSession session=se.getSession();
        session.removeAttribute("cusnames");
        
    }
    
}
