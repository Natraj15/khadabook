package com.mycompany.web.app;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ApplicationListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context=sce.getServletContext();
      //  InitialContext ctx=InitialContext.
      try{
      DataSource ds=(DataSource)InitialContext.doLookup("java:comp/env/jdbc/myDB");
      context.setAttribute("dbpool", ds);
     // Class.forName("com.mysql.cj.jdbc.Driver");
      
      }catch(NamingException ex){
          
      }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
         ServletContext context=sce.getServletContext();
         try{
             DataSource ds=(DataSource)InitialContext.doLookup("java:comp/env/jdbc/myDB");
             context.removeAttribute("dbpool");
      
      }catch(NamingException ex){
          
      }
    }
    
    
    
}
