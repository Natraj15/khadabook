package com.mycompany.web.app;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "CustomerServlet", urlPatterns = {"/CustomerServlet"})
public class CustomerServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            
          
            CustomerDao customers=new CustomerDao();
           
            List<Customer> list=customers.getCustomer();
            
            request.setAttribute("customers", list);
            RequestDispatcher dispatcher=request.getRequestDispatcher("jstlViewCustomer.jsp");
            dispatcher.forward(request, response);
            //  if(list.isEmpty()){
            
            /*    out.print("<table border=1 >");
            
                             out.print(
                                    """ 
                                       <tr>      
                                       <th> CustomerID </th>
                                       <th> CustomerName</th>
                                       <th> PhoneNumber</th>
                                       <th> Address</th>
                                       <th> BalanceAmount</th>
                                     </tr>  
                                     """
                           );
            for(Customer customer1:list){
                out.println("<tr>");
              
               out.println("<td>"+customer1.getCustomerID() +"</td>");
               out.println("<td>"+customer1.getName()+"</td>");
               out.println("<td>"+customer1.getPhoneNumber()+"</td>");
               out.println("<td>"+customer1.getAddress()+"</td>");
               out.println("<td>"+customer1.getBalanceAmount()+"</td>");
                
               out.println("</tr>");     
            }
               out.println("</table>");*/
               
               
               
//           }else{
//               out.print("<p style='color:red;'>some error<p>");
//           }
           
            
            
          //   List<Customer> customerlist=new ArrayList<>(list);
          
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
