/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Filter.java to edit this template
 */
package newpackage;

import jakarta.servlet.Filter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;


@WebFilter("/DemoFilter")
public class NewFilter implements Filter {
    
   
    
    public NewFilter() {
    }    

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("filter start");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        int id=Integer.parseInt(request.getParameter("id"));
        
        if(id>=1){
            chain.doFilter(request, response);
        }else{
            response.getWriter().print("invalid input");
        }
        
    }

    @Override
    public void destroy() {
        System.out.println("filter stop");
    }
    
}
