package newpackage;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/practise")
public class ExampleServlet extends GenericServlet{

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
       
      // response.setContentType("text/html;charset=UTF-8");
        
        PrintWriter out=response.getWriter();
        
       out.println(request.getCharacterEncoding());
       
       out.println(request.getContentLength());
       
       out.println(request.getContentType());
       
       out.println(request.getInputStream());
       
       out.println(request.getDispatcherType());
       
      
      
        out.print(response.getBufferSize());
       
       request.setAttribute("name", "natraj");
       request.setAttribute("age", "23");
       
      // RequestDispatcher dispatcher=request.getRequestDispatcher("dipatcher");
       out.println(response.isCommitted());
//       if(response.isCommitted()){
//          dispatcher.include(request, response);
//       }else{
//            out.print("commited");
//            out.print(response.isCommitted());
//       }
       
       out.println(request.getContentType());
       out.println(request.getLocale());
       out.println(request.getScheme());
       out.println(request.getServerName());
       out.println(request.getServerPort());
      // out.println(request.getRemoteAddr());
      // out.println(request.getRemoteHost());
       out.println(request.getRemotePort());
   
       
       
       
       
    }
   
}
