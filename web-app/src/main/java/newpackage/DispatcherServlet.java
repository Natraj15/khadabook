package newpackage;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;


@WebServlet("/dipatcher")
public class DispatcherServlet  extends GenericServlet{

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        
        String name=request.getAttribute("name").toString();
        PrintWriter out=response.getWriter();
       
        Enumeration<String> enumeration=request.getAttributeNames();
        while(enumeration.hasMoreElements()){
          String name1=enumeration.nextElement();
          out.println(name1);
        }
        
       // response.getWriter().print(name);
        
    }
    
    
}
