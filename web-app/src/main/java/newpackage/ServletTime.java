package newpackage;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Objects;

@WebServlet("/date")
public class ServletTime extends HttpServlet {
    @Override
    public void service(HttpServletRequest request,HttpServletResponse response) throws IOException{
        response.getWriter().println(LocalDate.now());
        
        ServletContext context=this.getServletContext();
        
       String mobile= context.getInitParameter("Mobile");
        
       ServletConfig config=this.getServletConfig();
       
      String name=  config.getInitParameter("Name");
       if(Objects.nonNull(config)){
           response.getWriter().println("Name :"+name);
       }else response.getWriter().println("Object is null");
       
       
        
        response.getWriter().println("Mobile"+mobile);
    }
}
