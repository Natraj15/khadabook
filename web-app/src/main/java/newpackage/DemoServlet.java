 
package newpackage;

import jakarta.servlet.GenericServlet;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;


@WebServlet("/DemoServlet")
public class DemoServlet extends GenericServlet{

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {

       String submit=request.getParameter("submit");
       
       response.getWriter().println(submit);
       
       if("a".equals(submit)){
           response.getWriter().println("hi"+submit);
       }else{
           response.getWriter().println("bye"+submit);
       }
        
    }
    
}
