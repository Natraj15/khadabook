package newpackage;

import jakarta.servlet.Servlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;

@WebServlet("/Customers")
public class ServletSQL implements Servlet{

    @Override
    public void init(ServletConfig sc) throws ServletException {
        System.out.println("*".repeat(10)+"Welcome"+"*".repeat(10));
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
         
    }

    @Override
    public void service(ServletRequest request, ServletResponse responce) throws ServletException, IOException {
               String url="jdbc:mysql://bassure.in:3306/Natrajdb";
               String user = "Natraj";
               String password = "Natraj@1501";
         PrintWriter out=responce.getWriter();
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
           Connection connection =DriverManager.getConnection(url,user, password);
            Statement statement=connection.createStatement();
            ResultSet result=statement.executeQuery("select * from Customers");
                           
           out.print("<table border=1 >");
            
                           out.print(""" 
                                     <tr>      
                                       <th> CustomerID </th>
                                       <th> CustomerName </th>
                                       <th> PhoneNumber </th>
                                       <th> Address   </th>
                                       <th> BalanceAmount </th>
                                     </tr>  
                                     """);
                           
            while(result.next())
            {
                out.println("<tr>");
              
               out.println("<td>"+result.getInt(1) +"</td>");
               out.println("<td>"+result.getString(2)+"</td>");
               out.println("<td>"+result.getLong(3)+"</td>");
               out.println("<td>"+result.getString(4)+"</td>");
               out.println("<td>"+result.getDouble(5)+"</td>");
                
               out.println("</tr>");     
            }
                out.println("</table>");

        }catch(SQLException | ClassNotFoundException sql){
        }
    }

    @Override
    public String getServletInfo() {
        return "Natraj";
    }

    @Override
    public void destroy() {
         System.out.println("#".repeat(10)+"ThankYou"+"#".repeat(10));
    }
    
    
}
