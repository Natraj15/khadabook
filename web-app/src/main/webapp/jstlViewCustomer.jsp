<%-- 
    Document   : jstlViewCustomer
    Created on : 10-Apr-2023, 6:11:12 pm
    Author     : bas200174
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>


        <c:choose>

            <c:when test="${not empty requestScope.customers}">

                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>PhoneNumber</th>
                        <th>Address</th>
                        <th>BalanceAmount</th>

                    </tr>

                    <c:forEach items="${requestScope.customers}" var="currentCustomer">

                        <tr>
                            <td>${currentCustomer.customerID}</td>
                            <td>${currentCustomer.name}</td>
                            <td>${currentCustomer.phoneNumber}</td>
                            <td>${currentCustomer.address}</td>
                            <td>${currentCustomer.balanceAmount}</td>
                        </tr>

                    </c:forEach>
                </table>
            </c:when>
            <c:when test="${empty requestScope.customers}">
                no customers
            </c:when>

            <c:otherwise>
                some error
            </c:otherwise>          

        </c:choose>

    
    </body>
</html>
