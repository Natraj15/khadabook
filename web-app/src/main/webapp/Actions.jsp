<%-- 
    Document   : Actions
    Created on : 31-Mar-2023, 3:38:23 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .boxIt{
                border: 5 px;
                border-style: dashed;
            }
            
            
        </style>
    </head>
    <body>
       
        <jsp:element name="span">
        
        <jsp:attribute name="style"  trim="true" >
            color:darkblue; font-size:60px;
        </jsp:attribute>
            <jsp:attribute name="class" trim="true">
                boxIt
                
            </jsp:attribute>    
            
        <jsp:body>
            
            hello actions
        </jsp:body>
            </jsp:element>
        <%   
          //  <jsp:forward></jsp:forward> 
            //<jsp:include></jsp:include> (like requestdispatcher forwade , include)
         //   <jsp:fallback></jsp:fallback> give some message to client like(could not load applet)
        //<jsp:param></jsp:param>
        //<jsp:params></jsp:params>
        %>
        
        <jsp:element name="j">
            <jsp:attribute name="style" trim="true">
                color: red;
                
            </jsp:attribute>
                <jsp:body>
                    Hi java
                </jsp:body>
                
                
        </jsp:element>
        
       
        <jsp:useBean id="c1" class="com.mycompany.web.app.Customer" scope="request">
            
        </jsp:useBean>
        <jsp:setProperty name="c1" property="name" value="james"></jsp:setProperty>
        
        <jsp:getProperty name="c1" property="name" ></jsp:getProperty> 
        
        
         <jsp:useBean id="c" class="com.mycompany.web.app.Customer" scope="request"></jsp:useBean>

         <jsp:setProperty name="c" property="*"></jsp:setProperty>
         <jsp:getProperty name="c" property="name"></jsp:getProperty>
         <jsp:getProperty name="c" property="customerID"></jsp:getProperty>
         <jsp:getProperty name="c" property="phoneNumber"></jsp:getProperty>
         <jsp:getProperty name="c" property="address"></jsp:getProperty>
         <jsp:getProperty name="c" property="balanceAmount"></jsp:getProperty>
         
        
    </body>
</html>
