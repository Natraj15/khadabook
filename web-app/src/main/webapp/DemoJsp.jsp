<%-- 
    Document   : DemoJsp
    Created on : 30-Mar-2023, 12:18:41 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" language="java" errorPage="Error.jsp" %>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%@include file="Sample" %><br>
        2+5=${2+3}
        
        ${10/0}
        
        <%
        
           String[] str=new String[0];
           out.print(str[2]);
        
        %>
        
        <jsp:scriptlet>
                
             out.println(java.time.LocalTime.now());

            </jsp:scriptlet>
        
      
    </body>
</html>
