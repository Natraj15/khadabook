<%-- 
    Document   : jstlSql
    Created on : 11-Apr-2023, 4:34:39 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="sql" uri="jakarta.tags.sql" %>
<%@taglib  prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>


        <sql:setDataSource var="mydbs" driver="com.mysql.cj.jdbc.Driver" user="Natraj" password="Natraj@1501"
                           url="jdbc:mysql://bassure.in:3306/Natrajdb" />

        <sql:query var="rs" dataSource="${mydbs}" >select * from Students</sql:query>

            <table border="1">
                <tr>
                <c:forEach items="${rs.columnNames}" var="column" > 
                    <th>${column}</th>
                </c:forEach> </tr>  
                <c:catch var="catchtheException" >
                    <c:forEach items="${rs.rowsByIndex}" var="student" >
                    <tr>
                        <td><c:out value="${student[0]}"></c:out> </td>
                        <td><c:out value="${student[1]}"></c:out> </td>
                        <td><c:out value="${student[2]}"></c:out> </td>
                        </tr>
                </c:forEach>
            </c:catch>
        </table>

                
    </body>
</html>
