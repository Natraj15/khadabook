<%-- 
    Document   : JSTL
    Created on : 14-Feb-2023, 11:07:27 am
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="Error.jsp"%>
<%@taglib prefix ="c"  uri ="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
                <style>
        table, th, td {
        border:1px solid black;
            }
</style>
    </head>
    <body>
        <h1>Hello World!</h1>
<!--        <% 
          String name=(String)request.getAttribute("names");
           out.print(name);
        %><br>
        ${names}<br>-->

<!--        <c:out value="hei jo"/><br>-->
<!--    <c:import url="https://www.facebook.com/"/>
    -->
    <sql:setDataSource var="db" driver="com.mysql.cj.jdbc.Driver" user="Natraj" password="Natraj@1501"
                       url="jdbc:mysql://bassure.in:3306/Natrajdb" />
    
    <sql:query var="rs" dataSource="${db}">select * from Students</sql:query>   
    
    <table>
        <tr>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Student Email</th> 
        </tr>
    <c:forEach items="${rs.rows}" var="student" >
        <tr>
    <td><c:out value="${student.ID}"></c:out> </td>
    <td><c:out value="${student.Name}"></c:out> </td>
    <td><c:out value="${student.Email}"></c:out> </td>
        </tr>
    </c:forEach>
        
    </table>
    
    
    <c:set var="str" value="my name is jobin"/>
    
<!--    Lenth: ${fn:length(str)}
    -->
    <c:forEach items="${fn:split(str,' ')}" var="s">
        <br>
        new:${s}
    </c:forEach>
        
    
        index :${fn:indexOf(str, "jobin")}<br>
        
        is Present:${fn:contains(str, "jsp")}<br>
        
        <c:if test="${fn:contains(str, 'jobin')}">
            jobin is present
        </c:if><br>
        
        ${fn:toUpperCase(str)}
        
        <c:forEach items="${fn:split(str, ' ')}" var="c">
            ${c}<br>
        </c:forEach>
       
</body>
</html>
