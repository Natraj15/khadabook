<%-- 
    Document   : jstlupdatesql
    Created on : 11-Apr-2023, 5:06:24 pm
    Author     : bas200174
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="sql" uri="jakarta.tags.sql" %>
<%@taglib  prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      
         <sql:setDataSource var="db" driver="com.mysql.cj.jdbc.Driver" user="Natraj" password="Natraj@1501" url="jdbc:mysql://bassure.in:3306/Natrajdb"/>

         <sql:transaction dataSource="${db}" >
 

            <sql:update sql="insert into Students values(?,?,?)" >
                
             <sql:param value="${param.studentID}"/>   <!-- instate of this we can give in ?(${param.studentID}) -->
            <sql:param value="${param.studentname}"/>
            <sql:param value="${param.studentEmail}"/>
                
            </sql:update>
            
        </sql:transaction>
         
         <c:redirect url="jstlSql.jsp" />
      
    </body>
</html>
