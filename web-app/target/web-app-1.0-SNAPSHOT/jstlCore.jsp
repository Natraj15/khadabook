<%-- 
    Document   : jstlCore
    Created on : 10-Apr-2023, 4:28:21 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="jakarta.tags.core" %> 



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>


        <c:if  test="${1>0}" >
            true
        </c:if><br/>

        <c:set var="name" value="name is jobin"/>

        <c:out value="${name}"/><br/>

        <c:set var="income" property="10000"/>
        <c:choose>

            <c:when test="${income eq 10000}" >it is good </c:when>
            <c:when test="${income < 10000}" >it is not good </c:when>
            <c:otherwise> bad</c:otherwise>

        </c:choose>

        <% int[] arr={1,2,3,4,5}; %>

        <c:forEach items="${arr}" var="s"> ${s}<br/> </c:forEach><!-- items must be array or collections-->

        <c:forEach var="a" begin="1" end="5">${a}</c:forEach><br/>


        <%--<c:import url="https://www.facebook.com/" var="s" >s</c:import> similar to jsp include--%>


        <c:forTokens items="${name}" delims=" " var="a" >${a}<br/></c:forTokens><!-- items must be string -->

        <c:remove  var="name"></c:remove>
        <c:out value="after remove :${name}"></c:out><br/>

        <c:catch var="catchtheexception">

            <% int a=2/0 ; %>

        </c:catch>

        <c:if test = "${catchtheException != null}">  
            <p>The type of exception is : ${catchtheException} <br />  
                There is an exception: ${catchtheException.message}</p>  
            </c:if>  


        <c:url value="elDemo.jsp" var="url" >  <!--it is used to wirting url, below used the url --> 
            <c:param name="myYear" value="2023"></c:param><!--it is used to url rerwirting -->
            <c:param name="hobby" value="game"></c:param>
        </c:url>
        <!--${url}-->

        <c:if test="${false}" var="a"> 
            <c:redirect  url="${url}" > <!-- similar to send redirect --%> 
                
                <c:param name="hobby" value="music"></c:param><!-- it is used to url rerwirting -->
            </c:redirect>
        </c:if>
       
                
    </body>
</html>
