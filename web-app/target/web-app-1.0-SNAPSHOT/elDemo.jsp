<%-- 
    Document   : Sample
    Created on : 10-Apr-2023, 3:19:00 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.mycompany.web.app.Customer"  %>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        SessionID :${cookie["JSESSIONID"] ["value"]}<br/> <!-- array nottation ["abc"]["def"], it could be ["a b c"] -->
        SessionID :${cookie["JSESSIONID"].value}<br/>  <!-- dotted nottation abc.def, it should be abc (no space and special charater $ (similar to java identifiers))  -->
        Host : ${header.host} </br>
        Host: ${header["host"]} <br/>
        ${headerValues.accept[0]} <br/>
        
        
        Year : ${param.myYear}<br/>
        Hobby 0:${paramValues.hobby[0]}<br/>
        Hobby 1:${paramValues.hobby[1]}<br/>
        Hobby 2:${paramValues.hobby[2]}<br/>
        
        
        
        <jsp:useBean id="u1" class="com.mycompany.web.app.Customer" scope="request"/>  <!-- in dotted don't use space  -->
        <jsp:setProperty name="u1"  property="name" value="bala" />  <!-- property should be  remove the (get or set in  methods ) afterlette should be small, like (getName=name) it shouldn't field   -->
    
        <jsp:getProperty name="u1" property="name"/>
        
        ${requestScope.u1.name} <br/>
        ${requestScope["u1"]["name"]}
      
      
        ${2+2}=4 <br/>
        ${2>2} <br/>
        ${2 gt 2} <br/>
        ${2 lt 2} <br/>
        ${2 eq 2} <br/>
        ${2 ne 2} <br/>
        ${2 div 2} <br/>
        ${9 mod 5} <br/>
        ${9 gt 5 and 3 lt 1} <br/>
        ${9 gt 5 or 3 lt 1} <br/>
        ${9 mod 5} <br/>
        
        ${null} <!-- output empty not null-->
        
        
        
    </body>
</html>
