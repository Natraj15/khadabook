<%-- 
    Document   : ViewCustomer
    Created on : 27-Mar-2023, 3:15:41 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.web.app.Customer"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        
            out.println(java.time.LocalDateTime.now());
            if(request.getAttribute("customers") !=null){
            
        %>

        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>PhoneNumber</th>
                <th>Address</th>
                <th>BalanceAmount</th>

            </tr>
            <%
                List<Customer> customers=(List<Customer>)request.getAttribute("customers");
           
        
                 for(Customer customer:customers)
                {
                out.print("<tr>");
                out.print("<td>"+customer.getCustomerID()+"</td>");
                out.print("<td>"+customer.getName()+"</td>");
                out.print("<td>"+customer.getPhoneNumber()+"</td>");
                out.print("<td>"+customer.getAddress()+"</td>");
                out.print("<td>"+customer.getBalanceAmount()+"</td>");
                out.print("</tr>");
                }
            %>                  

        </table>
        <%
            } else{
                    out.print("some error");
                    }
        
        %>

    </body>
</html>
