
package com.mycompany.loginproject;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.Servlet;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet("/Logout")
public class Logout  implements Servlet{

    @Override
    public void init(ServletConfig sc) throws ServletException {
        
    }

    @Override
    public ServletConfig getServletConfig() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
       
        
        String name=request.getParameter("name");
        response.getWriter().print(name);
        request.removeAttribute("name");
        response.getWriter().print(name);
        request.removeAttribute("password");
       
        RequestDispatcher rd=request.getRequestDispatcher("Login.jsp");
        rd.forward(request, response);
        
    }

    @Override
    public String getServletInfo() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void destroy() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
