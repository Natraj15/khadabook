package com.mycompany.loginproject;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
             
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        LoginDao login=new LoginDao();
         try {
            if (login.cheak(name,password)) {    
//             response.getWriter().print("welcome");
                   HttpSession  session=request.getSession();
                   session.setAttribute("pass", password);
                   session.setAttribute("names", name);
                   response.sendRedirect("Welcome.jsp");

            } 
            else {
                
                response.sendRedirect("Login.jsp");
                
           }
        } 
            catch (Exception ex) {
            response.getWriter().print(ex.getMessage());
        }

    }
    
    
}
