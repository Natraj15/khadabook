package com.mycompany.loginproject;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebServlet(name = "EmployeeController", urlPatterns = {"/EmployeeController"})
public class EmployeeController extends HttpServlet {

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
    
    
        int id=Integer.parseInt(request.getParameter("id"));
        
        EmployeeDao employee=new EmployeeDao();
        
        Employee dao=null;
        try {
            dao = employee.getEmployee(id);
        } catch (Exception ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        request.setAttribute("employee", dao);
        
        if(dao.getId()==id){
        RequestDispatcher rd=request.getRequestDispatcher("ShowEmployee.jsp");
        rd.forward(request, response);
        }else{
            response.getWriter().print("employee id not exist");
            
            response.sendRedirect("index.html");
        }
    }
}
