package corejava;

import java.util.Comparator;

public class ProductColourComparator implements  Comparator{
    
    @Override
    public int compare(Object t, Object t1) {
        if(t instanceof Pen1 p1 && t1 instanceof Pen1 p2){
            
            return p1.getColour().compareTo(p2.getColour());
        }else{
      throw new ClassCastException(" ");
}
        
    }
    public static void main(String[] args) {
     Pen1 p= new Pen1();
        System.out.println(p);
    Pen1 s=p;
    
      
    }
}
