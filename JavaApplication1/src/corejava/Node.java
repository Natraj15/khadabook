
package corejava;


public class Node {
    
    int data;
    Node next;

    
    public Node(int data) {
        this.data = data;
        this.next = null;
    }
    
    
}
class NodeD{
    
    public Node head=null;
    public Node tail=null;
    
    public void addnode(int data){
       
        Node n=new Node(data);
        if(head==null){
          head=n;
          tail=n;  
       }else{
            tail.next=n;
            tail=n;
        }      
        
    }public void display(){
        Node current =head;
        
        if(head==null){
            System.out.println("list is empty");
            return;
        }
            System.out.println("nodes of singly linked list");
            
            while(head!=null){
                System.out.println(current.data+" ");
                current=current.next;
            }System.out.println();
    }
    
    public static void main(String[] args) {
        NodeD no=new NodeD();
        no.addnode(10);
        no.addnode(20);
        no.addnode(30);
        
        no.display();
    }
}