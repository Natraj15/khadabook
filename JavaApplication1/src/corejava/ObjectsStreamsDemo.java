
package corejava;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class ObjectsStreamsDemo {
    
      public static void main(String[] args) {
       
       try(ObjectInputStream in=new ObjectInputStream (new FileInputStream("/home/bas200174/objectoutpurstream.txt") )){
         //  Car c=null;
           System.out.println("reading car from file");
           if(in.readObject() instanceof Car c){
               System.out.println(c);
           }
           System.out.println("Read  sucessfully");
       }catch(IOException er){
           System.out.println("Ioexception");
           er.printStackTrace();
       }catch(ClassNotFoundException cls){
           System.out.println("class not found");
       }
       
    }
    
    public static void main1(String[] args) {
       Car c=new Car("TATA",12344.0,"RED",new Engine("TATA","petrol",2111.2));
       try(ObjectOutputStream out=new ObjectOutputStream (new FileOutputStream("/home/bas200174/objectoutpurstream.txt") )){
           System.out.println("writing car into file");
           out.writeObject(c);
          System.out.println("write sucessfully");
       }catch(IOException er){
           System.out.println("Ioexception");
       }
        
    }
}
