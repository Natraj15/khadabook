
package corejava;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;


public class DateDemo {
    
    public static void main(String[] args) {
        LocalDate d=LocalDate.now();
       // System.out.println(d);
           
              
      //  System.out.println(d.format(DateTimeFormatter.ofPattern("dd-MMM-YYYY")));
        LocalDate tplus10=d.plusDays(10);
       // System.out.println(tplus10);
       
       Temporal t=tplus10.adjustInto(tplus10);
       // System.out.println(t);
        
        LocalDateTime ld=d.atStartOfDay();
        System.out.println(ld);
    }
}