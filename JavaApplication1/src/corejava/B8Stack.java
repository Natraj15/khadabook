
package corejava;


public class B8Stack{
    
    private Object[]  values;
    private int size;
    {
        values=new Object[8];
        size=0;
    }
    public int currentCapasity(){
       return values.length;
    }
    public boolean push(Object input){
        if(this.getSize()==values.length){
        resizeStorage();
    }
         values[size++]=input;
          return true;
    }
    public Object pop() throws RuntimeException{
        if(!isEmpty()){
        return values[--size];
        }else{
            throw new RuntimeException("stack underflow");
        }
    }
    public Object peek() throws RuntimeException{
        if(!this.isEmpty()){
            return values[size-1];
        }else {
            throw new RuntimeException("stack is empty");
        }
        
    }
    public boolean isEmpty(){
        return  size ==0;
    }
    public int getSize(){
        return size;
    }
    private void resizeStorage(){
        Object[] temp=new Object [size*2];
        
        System.arraycopy(values, 0, temp, 0, size);
        values = temp;
    }
}
