
package corejava;


public class BGenericStack<T> {
    
    
    private T[] values;
    private int size;

    {
        values = (T[]) new Object[8];
        size = 0;
    }

    public int currentCapacity() {
        return values.length;
    }

    public boolean push(T input) {
        if (this.getSize() == values.length) {
            resizeStorage();
        }
        values[size++] = input;
        return true;
    }

    public T pop() throws RuntimeException {
        if (!isEmpty()) {
            return values[--size];
        } else {
            throw new RuntimeException("Stack underflow");
        }
    }

    public T peek() throws RuntimeException {
        if (!this.isEmpty()) {
            return values[size - 1];
        } else {
            throw new RuntimeException("Stack is empty");
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int getSize() {
        return size;
    }

    private void resizeStorage() {
        T[] temp = (T[]) new Object[values.length * 2];

        System.arraycopy(values, 0, temp, 0, size);
        values = temp;
    }
    
    public static void main(String[] args) {
        
        
        BGenericStack s=new BGenericStack();
        System.out.println(s.currentCapacity());
    }
}

