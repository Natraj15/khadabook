package corejava;

import java.util.Comparator;

public class ProductNameComparator implements Comparator {

    public int compare(Object old, Object that){
        //String oldName = old.
        
        if (that instanceof Pen1) {
            Pen1 oldd = (Pen1) old; 
            Pen1 thatt = (Pen1) that;
            String oldName = oldd.getBrand();
            String newName = thatt.getBrand();
            return oldName.compareTo(newName);
        } else {
            throw new ClassCastException("Given Object class are mismatching");
        }
    }
}
