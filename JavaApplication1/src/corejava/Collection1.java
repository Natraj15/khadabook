package corejava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Stack;
import java.util.Vector;
import java.util.function.Predicate;


import static java.util.stream.Collectors.toList;
public class Collection1 extends ArrayList{

    public static void main1(String[] args) {
        ArrayList<String> list = new ArrayList();
        list.add("natraj");//add the element
        list.add("raj");
        list.add("arun");
        list.add(0,"nj");//specified index and add the element
        list.replaceAll(e->e.toUpperCase());
        Iterator itr = list.iterator();
         while(itr.hasNext()){
          System.out.println(itr.next());
        }
        ArrayList arl = new ArrayList();
        ArrayList<Integer> in = new ArrayList<>();
        ArrayList<Integer> inn = new ArrayList<>();
        inn.add(10);
        inn.add(20);
        inn.add(30);
        inn.add(40);
        inn.add(30);
        arl = (ArrayList) inn.clone();//clone method (copy from inn to arl)
        arl.add(100);
        arl.set(5, 50);//replace the specified index element
        System.out.println(inn.hashCode());//ge the hashcode
        System.out.println(arl.lastIndexOf(30));//find the index value from the last 
        System.out.println(arl.indexOf(30));//find the index value

        System.out.println(arl.equals(inn));//check the two arraylist
        arl.ensureCapacity(3);//ensure the minmum capcity of the collection
        System.out.println("arl" + arl);
        in.add(11);
        in.add(2);
        in.add(3);
       
        System.out.println(in.get(0));//get the element using index value
        
        in.addAll(1, inn);//it is used to store are the element from one to another
        //in.clear();//it is used to clear the 
        System.out.println(in.contains(50));//it is used to check if the element is present or not

        //inn.remove(1);
        //in.removeAll(in);
        System.out.println(inn.size());//find the size of list
        System.out.println("in" + in);
        System.out.println(in.size());
    
        Iterator it = inn.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

    }

    public static void main2(String[] args) {//LinkedList

        List<String> str = new LinkedList<String>();
        str.add("parthiban");
        str.add("deeban");
        str.add("vijay");

        Iterator<String> itre = str.iterator();

        while (itre.hasNext()) {
            System.out.println(itre.next());
        }
    }

    public static void main3(String[] args) {//vector

        Vector<Double> ve = new Vector<Double>();
        ve.add(11.1);
        ve.add(22.2);
        ve.add(33.3);
        ve.add(0, 55.5);//add in the 0 index

        //    System.out.println(ve.containsAll(ve));
        Object vt = ve.clone();
        System.out.println(vt);
        System.out.println(ve.capacity());
        Iterator i = ve.iterator();
        while (i.hasNext()) {
            System.out.println("Vector " + i.next());
        }

    }

    public static void main(String[] args) {

        Stack st = new Stack();
        st.push("ananth");
        st.add("str");
        st.push("ganesh");
        st.add("vijay");
        st.add(12);

        st.empty();
        
           System.out.println(st.pop());//it shows the last element and remove the element

           System.out.println(st.peek());//it show the last element and donot remove the element
           System.out.println(st.lastElement());
           System.out.println(st.search("str"));
           System.out.println(st);
    }public static void main7(String[] args) {
        
    
      
          String[] arg={"natraj","vijay","ananth"};
          ArrayList<String> ar=new ArrayList<>(List.of(arg));
          System.out.println(ar);
          int[] in={1,2,3,4,5,6,7,8};
          List inn=new ArrayList(List.of(in));
           System.out.println(inn);
     //  ar = Arrays.asList(arg);
        List<String> narr=Arrays.stream(arg).collect(toList());
     //   List<Integer> arr=Arrays.stream(in).collect(toList());
        System.out.println(narr);
    
    }
    public static void main5(String[] args) {
        
        ArrayList arl=new ArrayList();
        arl.add(11);
        arl.add(12);
        arl.add(13);
        arl.add(14);
        arl.add(15);
      
        
        arl.forEach((a)-> System.out.println(a));//this method print the each elemnt
        // arl.removeAll(arl);//remove the all element from the collection
         
            
        Iterator lit=arl.listIterator(2);//this method specified the starting endex
        while(lit.hasNext()){
            System.out.println(" "+lit.next());
        }
        ListIterator li=arl.listIterator();//using this method,we can print the element forwand and backward as well
        System.out.println("forward");
        while(li.hasNext()){
            System.out.println(li.next());
        }
        System.out.println("backward");
        while(li.hasPrevious()){
            System.out.println(li.previous());
        }
      
        
    }
    public static void main8(String[] args) {
        
         ArrayList<Integer> al=new ArrayList<>();
       
            for (int i = 0; i <= 20; i++){
                al.add(i);
            }
           
          //  Predicate<Integer> pr=a->(a%2!=0);  
          //  al.removeIf(pr);//this method used to add the condition.it removes the element only the condition is satisfy
            
          //  System.out.println(al);
         
            al.remove(Integer.valueOf(12));//remove the element
        
            al.remove(0);//remove the element by specified index
            
             Collection1 alr=new Collection1();
             alr.add(21);
             alr.add(22);
             alr.add(23);
             alr.add(24);
             alr.add(25);
            
             ArrayList li=new ArrayList();
             li.add(24);
             li.add(25);
             li.add(26);
             li.add(27);
             li.add(28);
             System.out.println(li.subList(0, 1));//this method is show the element between from index to index values
             
                     
             li.retainAll(alr);//this method is used for remove elementcse expect the same elements
             
            System.out.println(li);
               System.out.println(alr);
               alr.removeRange(0, 2);
               System.out.println(alr);
                            

    }
    
   
}
