
package corejava;
import java.util.Scanner;

public class customisedexception extends Exception{//we must extend exception class,it is almost like if else statement
    public static void main(String[] args) {
 
    Scanner scn=new Scanner(System.in);
    
     
       while(true){
       try{
             System.out.println("Enter The rollnumber");
             String rollnu=scn.nextLine();
           if(rollnu.length() !=10){
               throw new customisedexception();//we create the object of exception
                
           }
           System.out.println("Roll number "+rollnu);
            break;
       }
        
        catch(customisedexception er){
            System.out.println("Roll number below 10 digits");
                
                }
       }    
            
}       
}
