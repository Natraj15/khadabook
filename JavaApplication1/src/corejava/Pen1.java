
package corejava;


public class Pen1 implements Comparable<Pen1>{
    
   private String Brand;
   private String colour;
   private double price;
   
//    public Pen1() {
//    }

    public Pen1(String Brand, String colour, double price) {
        this.Brand = Brand;
        this.colour = colour;
        this.price = price;
    }
     
    public Pen1(){
        this("brand","black",233);
    }
    
    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int compareTo(Pen1 that) {
       // public int compareTo(Object that){
      //  if(t instanceof Pen1 that){
            
            if(this.price>that.price){
                return 1;
            }
            else if(this.price==that.price){
                return 0;
            }
            else{
                return -1;
            }
          
            //return (int)(this.price-that.price);
//        }else{
//            throw new ClassCastException(" ");
//        }
    }

    @Override
    public String toString(){
        return "Pen1{" + "Brand=" + Brand + ", colour=" + colour + ", price=" + price + '}';
    }
    
}
