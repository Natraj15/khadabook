
package corejava;

import java.io.Serializable;


public class Engine implements Serializable{
   private String brand;
   private String type;
   private Double price;

    public Engine(String brand, String type, Double price) {
        this.brand = brand;
        this.type = type;
        this.price = price;
    }

   
   
    public String getBrand() {
        return brand;
    }
     public void setBrand(String brand) {
        this.brand = brand;
    }


    public String getType() {
        return type;
    }
     public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

  

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Engine{" + "brand=" + brand + ", type=" + type + ", price=" + price + '}';
    }
    
   
}
