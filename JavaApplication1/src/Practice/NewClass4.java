
package Practice;


public class NewClass4 {
    
    String name;
    int age;

    public NewClass4(String name, int age) {
        this.name = name;
        this.age = age;
    }
    
}

class emp  extends NewClass4{
     
    int id;

    public emp(int id, String name, int age) {
        super(name, age);
        this.id = id;
    }
    void dis(){
        System.out.println("ID:"+id+" Name:"+name+" AGE:"+age);
    }
    public static void main(String[] args) {
        emp e=new emp(1,"raj",23);
        e.dis();
    }
  
}