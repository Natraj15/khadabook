import java.time.LocalDateTime;

public class pen1 {
    public static String category;
    public final String BRAND;
    String color;
    private double price;
    public final LocalDateTime MFG_DATE;
    static {
        category ="writing Instruments";
    }
    {
        MFG_DATE=LocalDateTime.now();
    }
    public pen1(){
        BRAND="cello";
        color="Black";
        price = 5.0;
    }
    pen1(String brand){
        this.BRAND = brand;
        //colour = "blue";
        //price = 5.0;
    }
    pen1(String brand,String color,double price){
        this.BRAND=brand;
        this.price=price;
        this.color=color;
    }
     
    public void setPrice(double price){
       if(price >=5.0){
           this.price = price;
           
       }
       else{
           System.out.println("price is too low");
       }
    }
       public double getPrice(){
           return price;
       }
    public static void main(String[] args) {
        pen1 co = new pen1("Parker","Black",500.0);   // ConstructorOverloading(), COnstructorOverloading("Renoldsoo")
        System.out.println("brand is "+co.BRAND);
        System.out.println("colour is "+ co.color);
        System.out.println("price is "+co.price);
        System.out.println(co.category);
        category="nj";
        System.out.println(co.category);
        
        System.out.println();
    }
}
    

