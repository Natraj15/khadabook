
public class operaters {
    public static void main(String[] args) {
        int a=15;
        int b=6;
        //logical operators
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);
        System.out.println("------");
        //relational operators
        System.out.println(a<b);
        System.out.println(a>b);
        System.out.println(a<=b);
        System.out.println(a>=b);
        System.out.println(a==b);
        System.out.println(a!=b);
        System.out.println("--------"); 
        //Logical operators
        System.out.println(false && false);//logical and -f
        System.out.println(false && true);//f
        System.out.println(true && false);//f
        System.out.println(true && true);//t
        System.out.println("-----");
        System.out.println(false || false);//logicol or -f
        System.out.println(false || true);//t
        System.out.println(true || false);//t
        System.out.println(true || true);//t
        System.out.println("-----");
        System.out.println(!true);//logical not-opposite
        System.out.println(!false);
         System.out.println("-----");
        //Bitwise operators
        int c=10;//8421->1010=10
        int d=13;//8421->1101=13
        System.out.println(c&d);//bitwise and, 1010&1101=1000-8 (1-1=1,1-0=0,0-1=0,0-0=0)
        System.out.println(c|d);//bitwise or,  1010|1101=1111-15  (1-1=1,1-0=1,0-1=1,0-0=0)
        System.out.println(c^d);//bitwise xor, 1010^1101=7    same are false (0-0=0,1-1=1,0-1=1,1-0=1) difference are true             
        System.out.println(~d);//bitwise not,  
        System.out.println(~c);
        System.out.println(c<<1);//left shift ,left side shift the value like 1010-10100
        System.out.println(c>>1);//right shift, right side shift the value like 1010-0101
         System.out.println("-----");
        //unary operators
        System.out.println(++c);//pre operators, 11
        System.out.println(d++);//post operators ,13
        System.out.println(d);//14
    }
    
}
