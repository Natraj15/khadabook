
public class ifstatement {
    public static void main(String[] args) {
        int a=1;
        if(a<18)//version-1
        {
            System.out.println("version1");
            System.out.println("------");
        }
         int age=19;
        if(age>=18)//version-2
        {
            System.out.println("major");
        }
        else{
            System.out.println("minor");
            System.out.println("-----");
        }
        int num=2;
       if(num<0)//version-3 else if
       {
            System.out.println("nj");
       }
       else if(num==0){
           System.out.println("raj");
           
       }
       else{
           System.out.println("natraj");
           System.out.println("-------");
       }
       int vote=17;
       if(vote>16)//version-4 nested if
       {
       if(vote>=18){
           System.out.println("elligible for vote");
       }
       else{
           System.out.println("not eligible for vote");
       }
       }
       else{
               System.out.println("small boy");
               }
       //ternary operator it is also like if else statement
      int number=18;
      String str=num%2==0?"even":"add";
        System.out.println(str);
    }      
    
}
