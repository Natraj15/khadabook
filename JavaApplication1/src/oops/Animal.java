package oops;

abstract class Animal {//we can't creat object for abstract classs
   public abstract void sound();//abstract method
   public abstract void walkby();
   
   public void sleep(){//normal method/concrete method
       System.out.println("zzz");
   } 
    
}
class lion extends Animal{
   public  void sound(){
            System.out.println("lololo");
}
   public void walkby(){
       System.out.println("four legs");
   }
}
class main{

    public static void main(String[] args) {
        lion l=new lion();
        l.sound();
        l.sleep();
        l.walkby();
    }
    
}