package oops;


 public class staticmember {
    static int num=11;//any variable or method is created directly in class body is callled as a member.
    static void play(){//static member are create with static key word 
        System.out.println("cricket");
}
 }
 class nonstaticmember{
     int a=10;//non static members are created without static key word
     void song(){
         System.out.println("kathal rojave");
     }
     
 }
 class mem{
    public static void main(String[] args) {
        System.out.println(staticmember.num);//static members are accessed using class name only
        staticmember.play();
        nonstaticmember nst=new nonstaticmember();//non static members are accssed by creating object
        nst.song();
        System.out.println(nst.a);
    }
}