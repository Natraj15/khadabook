
package oops;


public class inheritance {//one cass acuqring the properties of another class,the super class declare with finaly key word it can't inherit
    int a=10;
    void song(){
        System.out.println("kanne kalamane");
    }
}
    class child extends inheritance{//single level inheritance ,the properties of super class acessed by one child class
        int b=20;
        void singer(){
            System.out.println("yesudas");
        }
    }
  class whatsapp1{//multy level inheritance ,ther properties of super class accessed by more than one child class
      void message(){
          System.out.println("only message");
      }
  }
    class whatsapp2 extends whatsapp1{
        void call(){
            System.out.println("call");
        }
    }
   class whatsapp3 extends whatsapp2{
       void videocall(){
               System.out.println("video call");
   }
   }
   class whatsapp4 extends whatsapp3{
       void payment(){
           System.out.println("payment");
   }
   }
  class chennai{//Hierarichal inheritance ,the properties of super class is accessed by more than one subclass in same leve
      void merina(){
          System.out.println("merina");
      }
  }
 class cuddalore extends chennai{
     void neively(){
             System.out.println("nlc");
 }
 }
 class pondy extends chennai{
     void beer(){
         System.out.println("king fisher");
     }
 }
class prg1{
    public static void main(String[] args) {
        child c=new child();
        System.out.println(c.a);
        c.song();
        System.out.println(c.b);
        c.singer();
        System.out.println("---------sinlelevel-------------------");
        whatsapp4 whats=new whatsapp4();
        whats.message();
        whats.call();
        whats.videocall();
        whats.payment();
        System.out.println("-----------multylevel------------------");
        pondy p=new pondy();
        p.beer();
        p.merina();
        cuddalore co=new cuddalore();
        co.neively();
        co.merina();
    }
}//multiple inheritance not possible ,in this case one subclass tryies to access the properties of more than one super class
  //multiple inheriance possible in interface