
package oops;

import java.lang.reflect.Constructor;

import java.lang.Object;


public class Objects  {//there are totally five ways to create objects
    
    String str="natraj";
    public static void main(String[] args) throws Exception, IllegalAccessException {
        Objects o=new Objects();//java new operator
        System.out.println(o.str);
        Objects ob=Objects.class.newInstance();//class.new instance method
        System.out.println(ob.str);
        Constructor<Objects> obj=Objects.class.getConstructor();//Java newInstance() method of Constructor cl
        Objects obj1=obj.newInstance();
        System.out.println(obj1.str);
        
    }
}
class Clone implements Cloneable{
      String str="natraj";
      
      @Override
          protected Object clone() throws CloneNotSupportedException{//Java Object.clone() method

              return super.clone();
              
          }
          public static void main(String[] args) throws  CloneNotSupportedException{
              Clone c=new Clone();
              
              Clone cl=(Clone)c.clone();
              System.out.println(c.str);
              
    }
}
        