package oops;

public class ConstructorOverloading {
   
    String brand;
    String colour;
    private double price;
    
    ConstructorOverloading(){
        brand = "Cello";
        colour = "blue";
         price = 5.0;
    }
    ConstructorOverloading(String brand){
        this.brand = brand;
        //colour = "blue";
        //price = 5.0;
    }
    ConstructorOverloading(String brand,String colour,double price){
        this.brand=brand;
        this.price=price;
        this.colour=colour;
    }
    public void setPrice(double price){
       if(price >=5.0){
           this.price = price;
       }
       else{
           System.out.println("price is too low");
       }
    }
       public double getPrice(){
           return price;
       }
    public static void main(String[] args) {
        ConstructorOverloading d = new ConstructorOverloading();
        System.out.println(d.brand);
        System.out.println(d.colour);
        System.out.println(d.price);
        System.out.println("------------------");
        ConstructorOverloading gg = new ConstructorOverloading("bright");
        System.out.println("brand is "+gg.brand);
        System.out.println("-------------");
        ConstructorOverloading co = new ConstructorOverloading("Parker","Black",5.0);   // ConstructorOverloading(), COnstructorOverloading("Renoldsoo")
        System.out.println("brand is "+co.brand);
        System.out.println("colour is "+ co.colour);
        System.out.println("price is "+co.price);
        System.out.println("-------------");
       
    
    }
}






    

