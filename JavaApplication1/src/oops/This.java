
package oops;

public class This {
    
    String name;
    int id;

    public This(String name, int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "This{" + "name=" + name + ", id=" + id + '}';
    }
    
    public static void main(String[] args){
        This t=new This("natraj",123);
        System.out.println(t);
    }
}
