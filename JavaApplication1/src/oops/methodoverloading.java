
package oops;


public class methodoverloading {//-developing multiple methods in a class with same name and diff arguments
    void food(int no){//type of the argument
        System.out.println("1234");
    }
    void food(String biriyani){
        System.out.println("biriyani");
        
    }
    void food(int no,int number){//lenth of the argument
        System.out.println("1,2");
    }
    void food(int one,int two,int three){
        System.out.println("1,2,3");
    }
    void food(int num,String biriyani){//sequence of the argument
        System.out.println("1 biriyani");
    }
    void food(int num, String biriyani,double price){
        System.out.println("1 biriyani 50.0");
    }
    
}
class prg{
    public static void main(String[] args) {
        methodoverloading m=new methodoverloading();
        m.food(1);
        m.food("biriyani");
        System.out.println("----------------------------");
        m.food(1, 2);
        m.food(1, 2, 3);
        System.out.println("------------------------------");
        m.food(1," biriyani");
        m.food(1, "biriyani", 50);
    }
}
