package oops;


public interface NewInterface {//we can't creat object for interface
     void sound();  //interface method as default abstract and public
}
interface new2{//interface methods does not have body
    void voice(); 
}
class speack implements NewInterface,new2{//multiple inheritance is possible in interface, one subclass inherit more than one super class

    @Override
    public void sound() {
        System.out.println("my sound"); 
    }

    @Override
    public void voice() {
        System.out.println("my voice"); 
    }
}
class tell{
    public static void main(String[] args) {
        speack s1=new speack();
        s1.sound();
        s1.voice();
    }
}
