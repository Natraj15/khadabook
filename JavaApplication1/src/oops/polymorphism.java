
package oops;

public class polymorphism {//one object showing different behaviours ,it is almost like method overloading
    void payment(){//compiletime polymorphisum
        System.out.println("normal price");
    }
    void payment(long card){
        System.out.println("5% discount");
    }
    void payment(String upi){
        System.out.println("10% discout");
    }
      
}
           //runtimepolymorpism ,like method overrding
   class soft1{
       void future(){
           System.out.println("aaaaaaaaaaaa");
       }
   }
class soft2 extends soft1{
    void future(){
        System.out.println("abababababababbabb");
}
}
class soft3 extends soft1{
    void future(){
        System.out.println("abc abc abc abc abc");
    }
}
class poly{
    public static void main(String[] args) {
        polymorphism p=new polymorphism();
        p.payment();
        p.payment(1234567l);
        p.payment("paytm");
        System.out.println("-------------------------");
        soft1 user;//declar oe name for all objects
//        user=new soft1();
//        user.future();
//        user=new soft2();
//        user.future();
        user=new  soft3();
        user.future();
    }
}
