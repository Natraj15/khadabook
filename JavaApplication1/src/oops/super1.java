
package oops;

public class super1 {
   
    String colour="black";
    String name;
    int age;

    public super1(String name, int age) {
        this.name = name;
        this.age = age;
    }
    void sup(){
        System.out.println("NAME:"+name+" AGE:"+age);
    }
    
    void eat(){
        System.out.println("eating");
    }
}  

class super2 extends super1{
    String colour="white";
    String co=super.colour;//used to call the parant class instance variable
    int id;

    public super2(int id, String name, int age) {
        super(name, age);//used to invoke the parent class constructor .it is mantatory
        this.id = id;
        
    }
    void display(){
        sup();
        System.out.println("ID:"+id+" NAME:"+name+" AGE:"+age);
    }
    void dis(){
        System.out.println(colour);
        System.out.println(super.colour);
        System.out.println(co);
    }
    void eat(){
        System.out.println("eating sanakes");
    }
    void speack(){
        super.eat();//used to invoke parant class method
        eat();
        System.out.println("speaking");
    }
    
    public static void main(String[] args) {
        super2 s=new super2(1,"raj",23);
         s.dis();
         s.speack();
         s.display();
    }
}
    

