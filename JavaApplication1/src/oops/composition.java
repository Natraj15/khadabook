
package oops;


public class composition {
    void speed(){
        System.out.println("fast");            
    }   
    void slow(){
        System.out.println("slow ");
    }
}
class veichle{
    void bike(){
        System.out.println("drive the bike");
    }
    void car(){
        System.out.println("drive the car");
    }
}
class compo{
    veichle v=new veichle();
    composition c=new composition();
}
class prog{
    public static void main(String[] args) {
        compo co=new compo();
        co.v.bike();
        co.v.car();
        System.out.println("-------------------");
        co.c.speed();
        co.c.slow();
    }
}