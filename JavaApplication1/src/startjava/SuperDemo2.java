package startjava;

public class SuperDemo2 {
    
    int id;
    String name;

    public SuperDemo2() {
         System.out.println("parantclass");
    }

    public SuperDemo2(int id, String name) {
        this.id = id;
        this.name = name;
       
    }
    
    
}
class DemoChild extends SuperDemo2{
    
      double salary;
    
    DemoChild(){
       
        super();//it is used to invoke the parant class constructor (if it is not here compiler will put default)
        System.out.println("chileclass");   
   }
    
    
    public DemoChild(double salary, int id, String name) {
        super(id, name);//it is used to access the parent class constructor
        this.salary = salary;
    }
    
    void print(){
        System.out.println("id :"+super.id+" name :"+super.name+" salary :"+salary);
    }
  
    
    public static void main(String[] args) {
        //DemoChild c=new DemoChild();
        
        DemoChild c1=new DemoChild(11.2,1,"jobin");
        
        c1.print();
        System.out.println(c1.name);
    }
  }
