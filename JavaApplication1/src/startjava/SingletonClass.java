package startjava;

public class SingletonClass {
    
    private static SingletonClass single=new SingletonClass();
    
   //private static SingletonClass single;

   private  SingletonClass() {
       
    }
    
    public static SingletonClass getSingle(){
        
//        if(single==null){
//            single=new SingletonClass();
//        }
        return single;
    }
    
    public int getMultipyle(int number){
        return number*number;
    }
}
