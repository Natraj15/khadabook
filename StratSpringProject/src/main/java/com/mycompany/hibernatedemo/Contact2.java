package com.mycompany.hibernatedemo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;


@Entity
@Table(name = "contacts")
@NamedQueries(value ={
     @NamedQuery(name="Contact2.findByContactId", query=" from Contact2 c where c.contactId=:ID"),
    
     @NamedQuery(name="Contact2.findAll", query="from Contact2 c")
})

//@NamedQuery(name="Contact2.findByContactId", query="select c from Contact2 c where c.contactId=:ID")
public class Contact2 {

    @Id
    @Column(name = "contact_id")
    private long contactId;
    @Column(name = "name")
    private String name;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "member_id")
    private Member2 member;

    public Contact2() {
    }

    public Contact2(long contactId, String name, String mobile, String email, Member2 member) {
        this.contactId = contactId;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.member = member;
    }

  

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Member2 getMember() {
        return member;
    }

    public void setMember(Member2 member) {
        this.member = member;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact2{" + "contactId=" + contactId + ", name=" + name + ", mobile=" + mobile + ", email=" + email  + '}';
    }

}
