
package com.mycompany.hibernatedemo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.util.List;

public class HibernateDemo {

    public static void main(String[] args) {
       
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        //native query
//        Integer count=(Integer)em.createNativeQuery("select count(*) from contacts",Contact2.class).getSingleResult();
//        System.out.println("count : "+count);
        
           Contact2 temp=(Contact2)em.createNativeQuery("select * from contacts where contact_id=1",Contact2.class).getSingleResult();
           System.out.println("contact : "+temp);
            System.out.println("*".repeat(30));
           List<Contact2> contacts=em.createNativeQuery("select * from contacts",Contact2.class).getResultList();
           
           for(Contact2 c:contacts){
               System.out.println(c);
           }
           
        emf.close();
        em.close();
        
    }
    
    public static void main2(String[] args) {
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        //fluent patten
         
        List<Contact2> contact=em.createQuery("select c from Contact2 c where c.contactId >?1 and c.member.memberId=:ID ",Contact2.class)
                .setParameter(1,2l)
                .setParameter("ID", 2l)
                .getResultList();
        
        for(Contact2 c:contact){
            System.out.println(c);
        }
        
        
        emf.close();
        em.close();
    }
    
    public static void main3(String[] args) {
        //named Queries
         
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        Contact2  temp=em.createNamedQuery("Contact2.findByContactId",Contact2.class)
                .setParameter("ID", 1l)
                .getSingleResult();
        System.out.println(temp);
        emf.close();
        em.close();
    }
    
    public static void main5(String[] args) {
        //named Queries
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        try{
        List<Contact2>  temp=em.createNamedQuery("Contact2.findAll",Contact2.class).getResultList();
                  
        for(Contact2 c:temp){
            System.out.println(c);
        }
        }finally{
        
        emf.close();
        em.close();
        }
    }
}
