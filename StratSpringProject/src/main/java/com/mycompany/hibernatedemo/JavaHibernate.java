package com.mycompany.hibernatedemo;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import java.util.List;


public class JavaHibernate {
    
    public static void main(String[] args) throws Exception{
        
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        
        List<Member> allMembers=em.createQuery("SELECT m FROM Member m",Member.class).getResultList();
        
        
        for(Member m:allMembers){
            System.out.println(m);
        }
        em.close();
        emf.close();
    }
    
    public static void main2(String[] args) throws Exception{
        
          
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        //insert
     //   Member member=new Member(2,"vengat","9832234232");//new
        
        EntityTransaction tx=em.getTransaction();
        tx.begin();
       // em.persist(member);//managed
        tx.commit();
        em.close();
        emf.close();
        
       // main1(null);
    }
    public static void main3(String[] args) {
        //update
           
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        Member me=em.find(Member.class, 2l);
        
        em.getTransaction().begin();
        //em.remove(me);
        me.setName("jobin");
        em.getTransaction().commit();
        
        em.close();
        emf.close();
        
    }
    
    public static void main5(String[] args) {
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
       
        String name=em.createQuery("select m.name from Member m where m.memberId=?11",String.class)
                .setParameter(11, 1l).getSingleResult();
        System.out.println("#".repeat(20)+name);
        em.close();
        emf.close();
    }
    
    public static void main6(String[] args) {
        
        //one to many (using one member id we can get many contacts)
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        Member temp=em.find(Member.class, 1l);
        
        for(Contact c:temp.getContacts()){
            System.out.println(c);
        }
        em.close();
        emf.close();
        
    }
    public static void main7(String[] args) {
      
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
       
        Member2 member=em.find(Member2.class, 1l);
        
        for(Contact2 m:member.getContacts()){
            System.out.println(m);
        }
          //many to one
        
        Contact2 contact=em.find(Contact2.class, 1l);
        System.out.println(contact.getMember());
        em.close();
        emf.close();
            
    }
    
}
