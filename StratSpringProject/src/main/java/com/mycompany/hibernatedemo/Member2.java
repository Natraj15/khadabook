package com.mycompany.hibernatedemo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.util.List;


@Entity
@Table(name = "members")
public class Member2 {
    
    
    @Id
    @Column(name = "member_id")
    private long memberId;
    @Column(name = "name")
    private String name;
    @Column(name = "mobile")
    private String mobile;
    
    
    @OneToMany(mappedBy = "member")
    private List<Contact2> contacts;

    public Member2(){
        super();
    }

    
    public Member2(long memberId, String name, String mobile, List<Contact2> contacts) {
        this.memberId = memberId;
        this.name = name;
        this.mobile = mobile;
        this.contacts = contacts;
    }

   
    public List<Contact2> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact2> contacts) {
        this.contacts = contacts;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "Member2{" + "memberId=" + memberId + ", name=" + name + ", mobile=" + mobile + '}';
    }

}
