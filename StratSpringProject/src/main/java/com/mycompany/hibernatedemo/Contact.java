package com.mycompany.hibernatedemo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.io.Serializable;



@Entity(name="contacts")
public class Contact implements Serializable{
    
    @Id
    @Column(name="contact_id")
    private long contactId;
    @Column(name="name")
     private String name;
    @Column(name="member_id")
    private long memberId;
    @Column(name="mobile")
    private String mobile;
    @Column(name="email")
    private String email;

 
    
    public Contact() {
    }

    public Contact(long contactId, String name, long memberId, String mobile, String email) {
        this.contactId = contactId;
        this.name = name;
        this.memberId = memberId;
        this.mobile = mobile;
        this.email = email;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact{" + "contactId=" + contactId + ", name=" + name + ", memberId=" + memberId + ", mobile=" + mobile + ", email=" + email + '}';
    }

   
   
}
