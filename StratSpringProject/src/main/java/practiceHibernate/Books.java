package practiceHibernate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;


@Entity(name = "library")
public class Books {
    
    @Id
    @Column(name="library_id")
    private long libraryId;
//    @Column(name = "student_id")
//    private long studentId;
    @Column(name="books") 
    private String bookName;

    @ManyToOne
    @JoinColumn(name="student_id")
    private Students student;
    
    public Books() {
        super();
    }

    public Books(long libraryId, String bookName, Students student) {
        this.libraryId = libraryId;
        this.bookName = bookName;
        this.student = student;
    }
    
    

    public Students getStudent() {
        return student;
    }

    public void setStudent(Students student) {
        this.student = student;
    }

  

    public long getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(long libraryId) {
        this.libraryId = libraryId;
    }

//    public long getStudentId() {
//        return studentId;
//    }
//
//    public void setStudentId(long studentId) {
//        this.studentId = studentId;
//    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public String toString() {
        return "Books{" + "libraryId=" + libraryId + ", bookName=" + bookName + ", student=" + student + '}';
    }
    
    
}
