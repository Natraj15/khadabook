package practiceHibernate;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.util.List;

public class PracticeHibernate2 {
    
    public static void main(String[] args) {
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
        
        List<Students> students=em.createQuery("SELECT s FROM Students s",Students.class).getResultList();
            
        for(Students s:students){
            System.out.println(s);
        }
       
        
        System.out.println("*".repeat(30));
        
       Books book=em.find(Books.class, 1l);
        System.out.println(book.getStudent());
       
        em.close();
        emf.close();
       
    }
}
