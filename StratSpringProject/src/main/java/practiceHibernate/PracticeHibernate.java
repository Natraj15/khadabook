package practiceHibernate;

import practiceHibernate.Students;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.util.List;


import org.hibernate.Session;
import org.hibernate.cfg.Configuration;  
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


public class PracticeHibernate {

    public static void main1(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em = emf.createEntityManager();
        
      //  Students student=new Students(15,"dinesh","dinesh@gmail");
        
        Students student1=em.find(Students.class, 13l);
        
       // EntityTransaction tx=em.getTransaction();
        
        em.getTransaction().begin();
       // em.persist(student);
        student1.setName("kannan");
        em.getTransaction().commit();
        

        List<Students> students = em.createQuery("SELECT s FROM Students s", Students.class).getResultList();
   

        for (Students s : students) {
            System.out.println(s);
        }
        
        
        emf.close();
        em.close();

    }
    
    public static void main3(String[] args) {
        
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("hibernate-jpa");
        EntityManager em=emf.createEntityManager();
       
        
        String name=em.createQuery("select s.name from Students  s where s.id=?11",String.class)
                .setParameter(11, 2l).getSingleResult();
        
        System.out.println("*".repeat(30)+name);
        em.close();
        emf.close();
        
    }
    
    public static void main(String[] args) throws Exception{//not working
        
       Configuration config=new Configuration();
       config.configure("hibernate.cfg.xml");
       
       SessionFactory factory=config.buildSessionFactory();    
       Session session=factory.openSession();
       
       //  Students student=new Students(15,"dinesh","dinesh@gmail");
       
       Transaction t= session.beginTransaction();
       
      // session.persist(student);
       t.commit();
       session.close();
       factory.close();
        
     
    }
}
