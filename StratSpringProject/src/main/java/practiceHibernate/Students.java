package practiceHibernate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity(name = "Students")
//@Table(name = "Students")
public class Students implements Serializable {

    @Id
    @Column(name = "student_id")
    private long studentId;
    @Column(name = "Name")
    private String name;
    @Column(name = "Email")
    private String mobile;

     @OneToMany(mappedBy = "student")
    private List<Books> books;
    
    public Students() {
        super();
    }

    public Students(long studentId, String name, String mobile, List<Books> books) {
        this.studentId = studentId;
        this.name = name;
        this.mobile = mobile;
        this.books = books;
    }

   

    public List<Books> getBooks() {
        return books;
    }

    public void setBooks(List<Books> books) {
        this.books = books;
    }

    public long getId() {
        return studentId;
    }

    public void setId(long id) {
        this.studentId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "Students{" + "studentId=" + studentId + ", name=" + name + ", mobile=" + mobile + '}';
    }

    //", books=" + books + 
}
