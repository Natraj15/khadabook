package khatabook;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class Orders {

    int CusID;
    int ProID;
    int Orderid;
    int Quantity;
    double Price;
    double TotalPrice;
   
    boolean b = true;

    void PlaceOrder() throws Exception {
        try (Connection con = Customer.connection();
                Statement st = con.createStatement()) {
            System.out.println("Enter The CustomerID");
            CusID = Khatabook.GetInput();
            ResultSet rs = st.executeQuery("select * from Customers where ID=" + CusID);
          //  if (rs.next()) {
                while (rs.next()) {
                    PreparedStatement stv = con.prepareCall("insert into Orders (CustomerID) values(" + CusID + ")");
                    stv.executeUpdate();
                    Statement ment = con.createStatement();
                    ResultSet result = ment.executeQuery("select max(OrderID) from Orders");
                    while (result.next()) {
                        Orderid = result.getInt(1);
                    }

                    while (b) {
                        System.out.println("Enter The Product ID");
                        ProID = Khatabook.GetInput();
                        Statement me = con.createStatement();
                        ResultSet re = me.executeQuery("select * from Inventory where ProductID=" + ProID);
                        while (re.next()) {
                            if (re.getInt(3) == ProID);
                            {
                                PreparedStatement ps = con.prepareCall("insert into  LineItems  (OrderID,ProductID,ProductQuantity,Price) values(?,?,?,?)");
                                ps.setInt(1, Orderid);
                                ps.setInt(2, ProID);
                                System.out.println("Enter the Quantity");
                                Quantity = Khatabook.GetInput();
                                Statement ste = con.createStatement();
                                ResultSet res = ste.executeQuery("select * from Inventory where ProductID=" + ProID);
                                while (res.next()) {
                                    if (Quantity < res.getInt(3)) {
                                        ps.setInt(3, Quantity);
                                        Price = Quantity * re.getDouble(4);
                                        ps.setDouble(4, Price);
                                        ps.executeUpdate();
                                        PreparedStatement sta = con.prepareCall("update Orders set TotalPrice=? where OrderID=?");
                                        TotalPrice += Price;
                                        sta.setDouble(1, TotalPrice);
                                        sta.setInt(2, Orderid);
                                        sta.executeUpdate();
                                        PreparedStatement pps = con.prepareCall("update Customers set BalanceAmount=? where ID=?");
                                        pps.setDouble(1, TotalPrice);
                                        pps.setInt(2, CusID);
                                        pps.executeUpdate();

                                        int qua = res.getInt(3);
                                        int soldpro = res.getInt(6);
                                        PreparedStatement pre = con.prepareCall("update Inventory set ProductQuantity=?,SoldProducts=? where ProductID=?");
                                        int balqua = qua - Quantity;
                                        int sold = soldpro + Quantity;
                                        pre.setInt(1, balqua);
                                        pre.setInt(2, sold);
                                        pre.setInt(3, ProID);
                                        pre.executeUpdate();
                                    } else {
                                        System.err.println("Not Enought Products");
                                    }
                                }
                            }

                        }
                        System.out.println("1.Add Products");
                        System.out.println("2.Exit");
                        int num = Khatabook.GetInput();
                        switch (num) {
                            case 1:
                                b = true;
                                break;
                            case 2:
                                b = false;
                                break;
                        }

                    }

                }
//            } else {
//                System.err.println("CustomerID Not Exist");
//            }

        } catch (SQLException s) {
            System.err.println(s.getMessage());

        }

    }

    void ViewOrders() throws Exception {
        System.out.println("1.One Customer Order");
        System.out.println("2.All Orders");
        int nu = Khatabook.GetInput();
        switch (nu) {
            case 1:
                System.out.println("Enter The CustomerID");
                int id = Khatabook.GetInput();
                try (Connection con = Customer.connection(); 
                      Statement st = con.createStatement();) {
                    ResultSet rs = st.executeQuery("select * from Orders where CustomerID=" + id);
                    while (rs.next()) {
                        System.out.println("OrderID    :" + rs.getInt(1));
                        int orderid = rs.getInt(1);
                        System.out.println("CustomerID :" + rs.getInt(2));
                        System.out.println("Date       :" + rs.getTimestamp(3));
                        Statement sta = con.createStatement();
                        ResultSet rset = sta.executeQuery("select * from LineItems where OrderID=" + orderid);
                        while (rset.next()) {

                            System.out.println("LineItemID :" + rset.getInt(2));
                            System.out.println("ProductID  :" + rset.getInt(3));
                            System.out.println("Quantity   :" + rset.getInt(4));
                            System.out.println("price      :" + rset.getDouble(5));
                        }
                        System.out.println("=".repeat(25));
                        System.out.println("Total Price:" + rs.getDouble(4));
                        System.out.println("=".repeat(25));
                        System.out.println("");
                    }
                } catch (SQLException sql) {
                    System.out.println(sql.getMessage());
                }
                break;
            case 2:
              try (Connection con = Customer.connection(); 
                      Statement st = con.createStatement();) {
                ResultSet rs = st.executeQuery("select * from Orders");
                while (rs.next()) {
                    System.out.println("OrderID    :" + rs.getInt(1));
                    int orderid = rs.getInt(1);
                    System.out.println("CustomerID :" + rs.getInt(2));
                    System.out.println("Date       :" + rs.getDate(3));
                    Statement sta = con.createStatement();
                    ResultSet rset = sta.executeQuery("select * from LineItems where OrderID=" + orderid);
                    while (rset.next()) {
                        System.out.println("LineItemID :" + rset.getInt(2));
                        System.out.println("ProductID  :" + rset.getInt(3));
                        System.out.println("Quantity   :" + rset.getInt(4));
                        System.out.println("price      :" + rset.getDouble(5));
                    }
                    System.out.println("=".repeat(25));
                    System.out.println("Total Price:" + rs.getDouble(4));
                    System.out.println("=".repeat(25));
                    System.out.println("");
                }
            } catch (SQLException sql) {
                System.out.println(sql.getMessage());
            }
            break;
        }

    }

}
