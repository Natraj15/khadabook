package khatabook;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.InputMismatchException;

public class Customer {

    private String Name;
    private long PhoneNumber;
    private String Address;
    private double BalanceAmount;

    public Customer() {
    }

    public Customer(String Name, long PhoneNumber, String Address) {
        this.Name = Name;
        this.PhoneNumber = PhoneNumber;
        this.Address = Address;
       // this.BalanceAmount=BalanceAmount;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public long getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(long PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public double getBalanceAmount() {
        return BalanceAmount;
    }

    public void setBalanceAmount(double BalanceAmount) {
        this.BalanceAmount = BalanceAmount;
    }

    @Override
    public String toString() {
        return "Customer{" + "Name=" + Name + ", PhoneNumber=" + PhoneNumber + ", Address=" + Address + ", BalanceAmount=" + BalanceAmount +  '}';
    }
    
    
    BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
    void AddCustomer() throws Exception {
           try (Connection con = connection(); 
                PreparedStatement sta = con.prepareCall("insert into Customers (Name,PhoneNumber,Address) values (?,?,?)")) {
                System.out.println("ENTER THE CUSTOMER NAME");
                Name = read.readLine();
                sta.setString(1, Name);
                System.out.println("ENTER THE CUSTOMER PHONE NUMBER");
                PhoneNumber = Long.parseLong(read.readLine());
                sta.setLong(2, PhoneNumber);
                System.out.println("ENTER THE CUSTOMER ADDRESS");
                Address = read.readLine();
                sta.setString(3, Address);
                sta.executeUpdate();
                } catch (SQLException e) {
                System.err.println(e.getMessage());
               }catch( NumberFormatException n){
                    System.err.println("Number Formate Only Accepted");
               }
            }
    
    void ViewCustomer() throws Exception{
       System.out.println("1.All Customer Details");
       System.out.println("2.One Customer Details");
       int num=Khatabook.GetInput();
       switch(num){
           case 1:try(Connection con=connection();
           Statement st=con.createStatement();
            ResultSet rs=st.executeQuery("select * from Customers")){
               System.out.println("-".repeat(102));
               System.out.printf("|%-12s|%-30s|%-17s|%-18s|%-19s|","CustomerID","Customer Name","PhoneNumber","Address","BalanceAmount");
           while(rs.next()){
               System.out.println();
               System.out.print("|"+"-".repeat(100)+"|"); 
               System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getLong(3),rs.getString(4),rs.getDouble(5));
 
             }System.out.println("\n"+"-".repeat(102));
          
                }catch(SQLException e){
                  System.err.println(e.getMessage());
                 }
            break;
           case 2:  try(Connection con=connection();
            PreparedStatement st=con.prepareCall("select * from Customers where ID=?") ){
               System.out.println("Enter The Customer ID");
               int id=Khatabook.GetInput();
               st.setInt(1, id);
               ResultSet rs=st.executeQuery();
               System.out.println("-".repeat(102));
               System.out.printf("|%-12s|%-30s|%-17s|%-18s|%-19s|","CustomerID","Customer Name","PhoneNumber","Address","BalanceAmount");
           while(rs.next()){
               System.out.println();
               System.out.print("|"+"-".repeat(100)+"|"); 
               System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getLong(3),rs.getString(4),rs.getDouble(5));
 
             }System.out.println("\n"+"-".repeat(102));
          
          
              }catch(SQLException e){
               System.err.println(e.getMessage());
               }
              break;
           default :System.out.println("Enter the Connrect option");
             }
       
           }
          void UpdateCustomer() throws Exception{
              
                System.out.println("1.Change The Name");
                System.out.println("2.Change The PhoneNumber");
                System.out.println("3.Change the Address");
                System.out.println("4.Change All Details");
                int ch=Khatabook.GetInput();
           switch(ch){
                case 1:
                     try(Connection conn=connection();
                     PreparedStatement sta=conn.prepareCall("update customers set Name=? where ID=?")){
                     System.out.println("Enter the ID");
                     int id=Khatabook.GetInput();
                     sta.setInt(2, id);
                     System.out.println("Change The Name");
                     String name=read.readLine();
                     sta.setString(1, name);
                     sta.executeUpdate();
                    }catch(SQLException e){
                    System.out.println(e.getMessage());
                   }
                  break;
                case 2:try(Connection c=connection();
                      PreparedStatement ps=c.prepareCall("update Customers set PhoneNumber=? where ID=?")){
                      System.out.println("Enter The ID");
                      int id=Khatabook.GetInput();
                      ps.setInt(2,id);
                      System.out.println("Change The PhoneNumber");
                      long phone=Long.parseLong(read.readLine());
                      ps.setLong(1, phone);
                      ps.executeUpdate();
                    }catch(SQLException e){
                     System.out.println(e.getMessage());
                  }
                 break;
                case 3:
                    try(Connection con=connection();
                    PreparedStatement pr=con.prepareCall("update Customer set Address=? where ID=?")){
                    System.out.println("Enter The ID");
                    int id=Khatabook.GetInput();
                    pr.setInt(2, id);
                    System.out.println("Change The Address");
                    String name=read.readLine();
                    pr.setString(1, name);
                    pr.executeUpdate();
                   }
                 break;
               case 4: try(Connection con=connection();
                    PreparedStatement pre=con.prepareCall("update Customers set Name=?,PhoneNumber=?,Address=? where ID=?")){
                    System.out.println("Enter the Customer ID");
                    int n=Khatabook.GetInput();
                    pre.setInt(4,n );
                    System.out.println("Change The Name");
                    String i=read.readLine();
                    pre.setString(1, i);
                    System.out.println("Change PhoneNumber");
                    long num=Long.parseLong(read.readLine());
                    pre.setLong(2, num);
                    System.out.println("Change Address");
                    String st=read.readLine();
                    pre.setString(3, st);
                    pre.executeUpdate();
                     }catch(SQLException e){
                    System.err.println(e.getMessage());
                    }
               break;
            }
     } 
   
        public  static Connection connection() throws Exception{
               String URL = "jdbc:mysql://bassure.in:3306/Natrajdb";
               String USER = "Natraj";
               String PASS = "Natraj@1501";
              Connection con=DriverManager.getConnection(URL, USER, PASS);
              return con;
          }
        public static void main(String[] args) {
            
      //      System.out.println(cus.Name);
    }
}
