package khatabook;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Inventory {
    
    private String ProductName;
    private int ProductID;
    private int ProductQuantity;
    private double OneProductprice;
    private double TotalProductprice;

    public Inventory() {
    }

    public Inventory(String ProductName, int ProductID, int ProductQuantity, double OneProductprice, double TotalProductprice) {
        this.ProductName = ProductName;
        this.ProductID = ProductID;
        this.ProductQuantity = ProductQuantity;
        this.OneProductprice = OneProductprice;
        this.TotalProductprice = TotalProductprice;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int ProductID) {
        this.ProductID = ProductID;
    }

    public int getProductQuantity() {
        return ProductQuantity;
    }

    public void setProductQuantity(int ProductQuantity) {
        this.ProductQuantity = ProductQuantity;
    }

    public double getOneProductprice() {
        return OneProductprice;
    }

    public void setOneProductprice(double OneProductprice) {
        this.OneProductprice = OneProductprice;
    }

    public double getTotalProductprice() {
        return TotalProductprice;
    }

    public void setTotalProductprice(double TotalProductprice) {
        this.TotalProductprice = TotalProductprice;
    }
    
    BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
   
   void AddProduct() throws Exception{
       try(Connection con=Customer.connection();
           PreparedStatement pre=con.prepareCall("insert into Inventory (ProductName,ProductQuantity,OneProductprice,TotalProductprice) values(?,?,?,?)")){
           System.out.println("Enter the ProductName");
           ProductName=read.readLine();
           pre.setString(1, ProductName);
           System.out.println("Enter The ProductQuantity");
           ProductQuantity=Integer.parseInt(read.readLine());
           pre.setInt(2, ProductQuantity);
           System.out.println("Enter the OneProductprice");
           OneProductprice=Double.parseDouble(read.readLine());
           pre.setDouble(3, OneProductprice);
           System.out.println("Enter the TotalProductPrice");
           TotalProductprice=Double.parseDouble(read.readLine());
           pre.setDouble(4, TotalProductprice);
           pre.executeUpdate();
          }catch(SQLException e){
           System.err.println(e.getMessage());
        }
       
   }
   
   void ViewProduct() throws Exception{
           System.out.println("1.View All Products");
           System.out.println("2.View One Product");
           int num=Khatabook.GetInput();
           switch(num){
               case 1:
               try(Connection con =Customer.connection();
               Statement pre=con.createStatement();
               ResultSet rs=pre.executeQuery("select * from Inventory")){
                   System.out.println("-".repeat(102));
                   System.out.printf("|%-12s|%-30s|%-15s|%-18s|%-19s|","ProuductID","Proudct Name"," Product Quantity"," One Product Price", "Total Product Price");
               while(rs.next()){
                   System.out.println();
                   System.out.print("-".repeat(101)+"|");
                   System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getDouble(5));
                }   System.out.println("\n"+"-".repeat(101));
              }catch(SQLException s){
               System.err.println(s.getMessage());
          
             }break;
               case 2: 
             try(Connection con =Customer.connection();
                 PreparedStatement pre=con.prepareCall("select * from Inventory where ProductID=?"))
              {
                   System.out.println("Enter The ProductID");
                   int n=Khatabook.GetInput();
                   pre.setInt(1, n);
                   ResultSet rs=pre.executeQuery();
                   System.out.println("-".repeat(102));
                   System.out.printf("|%-12s|%-30s|%-15s|%-18s|%-19s|","ProuductID","Proudct Name"," Product Quantity"," One Product Price", "Total Product Price");
               while(rs.next()){
                   System.out.println();
                   System.out.print("-".repeat(101)+"|");
                   System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getDouble(5));
                }  System.out.println("\n"+"-".repeat(101));
              }catch(SQLException s){
               System.err.println(s.getMessage());
          
             }
         }       
    }
      void UpdateProduct() throws Exception{
          System.out.println("1.Change The Name");
          System.out.println("2.Change The Quantity");
          System.out.println("3.Change One Product Price");
          System.out.println("4.Change Totoal Product Price");
          int num=Khatabook.GetInput();
          switch(num){
              case 1:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set ProductName=? where ProductID=?")){
                      System.out.println("Enter The ProductID");
                      int nu=Khatabook.GetInput();
                      pre.setInt(2, nu);
                      System.out.println("Change The Name");
                      String str=read.readLine();
                      pre.setString(1, str);
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                case 2:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set ProductQuantity=? where ProductID=?")){
                      System.out.println("Enter The ProductID");
                      int nu=Khatabook.GetInput();
                      pre.setInt(2, nu);
                      System.out.println("Change The Quantity");
                      int str=Khatabook.GetInput();
                      pre.setInt(1, str);
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                case 3:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set OneProductPrice=? where ProductID=?")){
                      System.out.println("Enter The ProductID");
                      int nu=Khatabook.GetInput();
                      pre.setInt(2, nu);
                      System.out.println("Change The OneProductPrice");
                      int str=Khatabook.GetInput();
                      pre.setInt(1, str);
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                  case 4:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set TotalProductPrice=? where ProductID=?")){
                      System.out.println("Enter The ProductID");
                      int nu=Khatabook.GetInput();
                      pre.setInt(2, nu);
                      System.out.println("Change The TotalProductPrice");
                      int str=Khatabook.GetInput();
                      pre.setInt(1, str);
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                
          }
      }
       
}
 
 