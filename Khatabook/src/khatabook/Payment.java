package khatabook;


import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Payment {

    int CusID;
    double PaymentAmount;


    void Addpayment() throws Exception {
        try (Connection con = Customer.connection();
             PreparedStatement st = con.prepareCall("insert into Payments (CustomerID,PaymentAmount,BalanceAmount) values (?,?,?)")) {
            System.out.println("Enter CustomerID");
            CusID = Khatabook.GetInput();
            Statement ment = con.createStatement();
            ResultSet rs = ment.executeQuery("select * from Customers where ID=" + CusID);
           // if (rs.next()) {
                while (rs.next()) {
                        st.setInt(1, CusID);
                        System.out.println("Enter the Payment Amount");
                        PaymentAmount = Khatabook.GetInput();
                        st.setDouble(2, PaymentAmount);
                      
                        double temp = rs.getDouble(5);
                        double bal = temp - PaymentAmount;
                        st.setDouble(3, bal);
                        st.executeUpdate();
                        PreparedStatement pre = con.prepareCall("update Customers set BalanceAmount=? where ID=?");
                        pre.setDouble(1, bal);
                        pre.setInt(2, CusID);
                        pre.executeUpdate();
                    } 
          //  }else{
               // System.err.println("Customer ID Not Exist");
            //}
        } catch (SQLException sql) {
            System.err.println(sql.getMessage());
        }
    }

}
