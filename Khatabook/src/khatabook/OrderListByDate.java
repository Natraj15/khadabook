package khatabook;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class OrderListByDate {
     
      BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
     void OneDayList() throws Exception{
            try(Connection con=Customer.connection();
            PreparedStatement ps=con.prepareCall("select * from Orders where OrderDate=?")){   
                System.out.println("Enter The Date (YYYY-MM-DD)");
                Date d=Date.valueOf(read.readLine());
                ps.setDate(1, d);
                ResultSet rs=ps.executeQuery();
                 while( rs.next()){
                System.out.println("OrderID :"+rs.getInt(1));
                int orderid=rs.getInt(1);
                System.out.println("CustomerID :"+rs.getInt(2));
                System.out.println("Date :"+rs.getDate(3));
                Statement sta=con.createStatement();
                ResultSet rset=sta.executeQuery("select * from LineItems where OrderID="+orderid);
                while(rset.next()){
                    System.out.println("LineItemID :"+rset.getInt(2));
                    System.out.println("ProductID :"+rset.getInt(3));
                    System.out.println("Quantity :"+rset.getInt(4));
                    System.out.println("price :"+rset.getDouble(5));
                }
                System.out.println("Total Price :"+rs.getDouble(4)); 
                System.out.println("");
            }
            } catch(SQLException s){
                System.err.println(s.getMessage());
            }
            catch(IllegalArgumentException ar){
                System.err.println("Enter Only Date Format(YYYY-MM-DD) ");
            }
       }
     void MultipleDaysList() throws Exception{
           try(Connection con=Customer.connection();
            PreparedStatement ps=con.prepareCall("select * from Orders where OrderDate between ?and?")){   
                System.out.println("Enter The Start Date (YYYY-MM-DD)");
                Date date1=Date.valueOf(read.readLine());
                System.out.println("Enter The End Date (YYYY-MM-DD)");
                Date date2=Date.valueOf(read.readLine());
                ps.setDate(1, date1);
                ps.setDate(2, date2);
                ResultSet rs=ps.executeQuery();
                 while( rs.next()){
                System.out.println("OrderID :"+rs.getInt(1));
                int orderid=rs.getInt(1);
                System.out.println("CustomerID :"+rs.getInt(2));
                System.out.println("Date :"+rs.getDate(3));
                Statement sta=con.createStatement();
                ResultSet rset=sta.executeQuery("select * from LineItems where OrderID="+orderid);
                while(rset.next()){
                    System.out.println("LineItemID :"+rset.getInt(2));
                    System.out.println("ProductID :"+rset.getInt(3));
                    System.out.println("Quantity :"+rset.getInt(4));
                    System.out.println("price :"+rset.getDouble(5));
                }
                System.out.println("Total Price :"+rs.getDouble(4)); 
                System.out.println("");
            }
            } catch(SQLException s){
                System.err.println(s.getMessage());
            }catch(IllegalArgumentException  ar){
                System.err.println("Enter Only Date Format(YYYY-MM-DD)");
            }
     }
    
}
