package khatabook;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Statistics {
      
    BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
    
    void HighPurchasedCus() throws Exception{
        
           try( Connection con= Customer.connection();
                   Statement st=con.createStatement()){
             ResultSet rs=st.executeQuery("select CustomerID,sum(TotalPrice) from Orders group by CustomerID order by sum(TotalPrice) desc");
             System.out.println("-".repeat(30));
             System.out.printf("|%-12s|%-16s|","CustomerID","TotalPrice");    
              while(rs.next()){
                System.out.println();
                System.out.print("|"+"-".repeat(29)+"|");    
                System.out.format("\n|%-12s|%-16s|",rs.getInt(1),rs.getInt(2));   
              }System.out.println("\n"+"-".repeat(30));
           }
         
    }
    void UnpaidCustomers() throws Exception{
         try(Connection con=Customer.connection();
            Statement st=con.createStatement();
             ResultSet rs=st.executeQuery("select * from Customers where BalanceAmount!=0 order by BalanceAmount desc")){
             System.out.println("-".repeat(30));
             System.out.printf("|%-12s|%-16s|","CustomerID","Balance Amount");    
             while(rs.next()){
             System.out.println();
             System.out.print("|"+"-".repeat(29)+"|");    
             System.out.format("\n|%-12s|%-16s|",rs.getInt(1),rs.getDouble(5));  
             }System.out.println("\n"+"-".repeat(30));
             
         }catch(SQLException s){
             System.err.println(s.getMessage());
         }
    }
    void PaidCustomers() throws Exception{
        
         try(Connection con=Customer.connection();
            Statement st=con.createStatement();
             ResultSet rs=st.executeQuery("select * from Customers where BalanceAmount=0.00 ")){
             System.out.println("-".repeat(30));
             System.out.printf("|%-12s|%-16s|","CustomerID","Balance Amount");    
             while(rs.next()){
             System.out.println();
             System.out.print("|"+"-".repeat(29)+"|");    
             System.out.format("\n|%-12s|%-16s|",rs.getInt(1),rs.getDouble(5));  
             }System.out.println("\n"+"-".repeat(30));
             
         }catch(SQLException s){
             System.err.println(s.getMessage());
         }
    }
    
     void MostSoldProduct() throws Exception{
           try(Connection con=Customer.connection();
            Statement st=con.createStatement()){
              ResultSet rs=st.executeQuery("select * from Inventory where SoldProducts in(select max(SoldProducts) from Inventory)");
             System.out.println("-".repeat(30));
             System.out.printf("|%-12s|%-16s|","ProductID","Sold Products Quantity");    
              while(rs.next()){
                System.out.println();
                System.out.print("|"+"-".repeat(29)+"|");    
                System.out.format("\n|%-12s|%-16s|",rs.getInt(1),rs.getInt(6));   
              }System.out.println("\n"+"-".repeat(30));
           }
         
     }
       void LessSoldProduct() throws Exception{
           try(Connection con=Customer.connection();
            Statement st=con.createStatement()){
              ResultSet rs=st.executeQuery("select * from Inventory where SoldProducts in(select min(SoldProducts) from Inventory)");
             System.out.println("-".repeat(30));
             System.out.printf("|%-12s|%-16s|","ProductID","Sold Products");    
              while(rs.next()){
                System.out.println();
                System.out.print("|"+"-".repeat(29)+"|");    
                System.out.format("\n|%-12s|%-16s|",rs.getInt(1),rs.getInt(6));   
              }System.out.println("\n"+"-".repeat(30));
           }
         
     }

}
