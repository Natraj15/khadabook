package khatabook;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Khatabook{
    
    
    
    public static void main(String[] args) throws Exception{
        boolean b=true;       
        while(b){
             System.out.println("*".repeat(15));
             System.out.println("|   MENU      |");
             System.out.println("|1.Customer   |");
             System.out.println("|2.Inventory  |");
             System.out.println("|3.Statics    |");
             System.out.println("|4.Exit       |");
             System.out.println("*".repeat(15));
             int nu=GetInput();
             boolean bl=true;
             switch(nu){
                 case 1:
                         Customer cus=new Customer();
                         Orders order=new Orders();
                         Payment pay=new Payment();
                        while(bl){
                        System.out.println("/".repeat(19));    
                        System.out.println("|1.add customer   |");
                        System.out.println("|2.view customer  |");
                        System.out.println("|3.update customer|");
                        System.out.println("|4.Place Order    |");
                        System.out.println("|5.Make Payment   |");
                        System.out.println("|6.view Orders    |");
                        System.out.println("|7.Exit           |");
                        System.out.println("/".repeat(19));
                       int num=GetInput();
                       switch(num){
                       case 1:cus.AddCustomer();
                         break;
                       case 2:cus.ViewCustomer();
                         break;
                       case 3:cus.UpdateCustomer();
                         break;
                       case 4: order.PlaceOrder();
                         break;
                       case 5:pay.Addpayment();
                          break;
                       case 6:order.ViewOrders();
                          break;
                       case 7:bl=false;
                         break;
                        }
                     
                       }
                     break;
                 case 2:
                     Inventory inventory=new Inventory();
                     boolean tru=true;
                     while(tru){
                         System.out.println("*".repeat(19));
                         System.out.println("/1.Add Product    /");
                         System.out.println("/2.View Product   /");
                         System.out.println("/3.Update Product /");
                         System.out.println("/4.Exit           /");
                         System.out.println("*".repeat(19));
                         int Product=GetInput();
                         switch(Product){
                             case 1:inventory.AddProduct();
                                  break;
                             case 2:inventory.ViewProduct();
                                 break;
                             case 3:inventory.UpdateProduct();
                                 break;
                             case 4:tru=false;
                                 break;
                         }
                     }break;
                 case 3:
                      
                       boolean sta=true;
                       while(sta){
                           System.out.println("=".repeat(40));
                           System.out.println("1.Most Purchased Customers List");
                           System.out.println("2.Paid Customers");
                           System.out.println("3.UnPaid Customers");
                           System.out.println("4.Most Sold Products");
                           System.out.println("5.Less Sold Products");
                           System.out.println("6.Orders List By One_Date");
                           System.out.println("7.Orders List By Start_Date to End_Date");
                           System.out.println("8.Exit");
                           System.out.println("=".repeat(40));
                           int num=GetInput();
                            Statistics statics=new Statistics();
                            OrderListByDate date=new OrderListByDate();
                           switch(num){
                               case 1:statics.HighPurchasedCus();
                                   break;
                               case 2:statics.PaidCustomers();
                                   break;
                               case 3:statics.UnpaidCustomers();
                                   break;
                               case 4:statics.MostSoldProduct();
                                   break;
                               case 5:statics.LessSoldProduct();
                                   break;
                               case 6:date.OneDayList();
                                   break;
                               case 7:date.MultipleDaysList();
                                  break;
                               case 8:sta=false;   
                           }
                       }break;
                 case 4:b=false;
                 break;
             }
          
        }  
    }
    public static int GetInput() throws Exception{
         int num=0;
         BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
         while(true){
         try{
              num=Integer.parseInt(read.readLine());
              break;
         }
         catch(NumberFormatException n){
             System.err.println("Number  Only Accepted");
         } 
     } 
     return num;
   }
}