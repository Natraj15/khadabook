package Dao.Mysql;

import DAO.CustomerDao;
import DAO.InventoryDao;
import DAO.KhadabookDao;
import DAO.OrdersDao;
import DAO.PaymentsDao;
import DAO.StatisticsDao;

public class MysqlConnection implements KhadabookDao{
    
    @Override
    public CustomerDao getCustomer(){
        return new CustomerDaoImp();
    }

    @Override
    public InventoryDao getInventory() {
       
        return new InventoryDaoImp();
    }

    @Override
    public OrdersDao getOrders() {
       return new OrdersDaoImp();
    }

    @Override
    public PaymentsDao getPayments(){
        return new PaymentsDaoImp();
    }

    @Override
    public StatisticsDao getStatistics() {
        return new StatisticsDaoImp();
    }

          
}
