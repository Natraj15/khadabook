package Dao.Mysql;

import java.sql.Connection;
import java.sql.SQLException;
import Pojo.Customers;
import khatabook.Customer;
import java.sql.Statement;
import DAO.CustomerDao;
import Main.MySql;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CustomerDaoImp implements CustomerDao {


    @Override
    public void addCustomer(Customers cus) throws Exception {
        try (Connection con = Customer.connection();
             Statement st = con.createStatement();) {
            st.executeUpdate("insert into Customers (Name,PhoneNumber,Address) values (" + "'" + cus.getName() + "'" + ',' + cus.getPhoneNumber() + ',' + "'" + cus.getAddress() + "'" + ")");

        } catch (SQLException | ClassNotFoundException sl) {
            System.out.println(sl.getMessage());
        }
    }

    @Override
    public void viewCustomer(int choose, int id) throws Exception {
        switch (choose) {
            case 1:try (Connection con = Customer.connection(); Statement st = con.createStatement(); ResultSet rs = st.executeQuery("select * from Customers")) {
                System.out.println("-".repeat(102));
                System.out.printf("|%-12s|%-30s|%-17s|%-18s|%-19s|", "CustomerID", "Customer Name", "PhoneNumber", "Address", "BalanceAmount");
                while (rs.next()) {
                    System.out.println();
                    System.out.print("|" + "-".repeat(100) + "|");
                    System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|", rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getDouble(5));

                }
                System.out.println("\n" + "-".repeat(102));

            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
            break;
            case 2:  
                 try (Connection con = Customer.connection(); PreparedStatement st = con.prepareCall("select * from Customers where ID=?")) {

                st.setInt(1, (id));
                ResultSet rs = st.executeQuery();
                System.out.println("-".repeat(102));
                System.out.printf("|%-12s|%-30s|%-17s|%-18s|%-19s|", "CustomerID", "Customer Name", "PhoneNumber", "Address", "BalanceAmount");
                while (rs.next()) {
                    System.out.println();
                    System.out.print("|" + "-".repeat(100) + "|");
                    System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|", rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getDouble(5));

                }
                System.out.println("\n" + "-".repeat(102));

            } catch (SQLException e) {
                System.err.println(e.getMessage());
            }
            break;

            }

    }

   

    @Override
    public void updateCustomer(int ch, Customers cus) throws Exception {

        switch (ch) {
            case 1:
                    try (Connection conn = Customer.connection();
                        PreparedStatement sta = conn.prepareCall("update Customers set Name=? where ID=?")){
                        sta.setInt(2, cus.getCustomerID());
                        sta.setString(1,cus.getName());
                        sta.executeUpdate();
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                
                
                break;
            case 2:try (Connection c = Customer.connection(); 
                    PreparedStatement ps = c.prepareCall("update Customers set PhoneNumber=? where ID=?")) {
                ps.setInt(2,cus.getCustomerID());
                ps.setLong(1, cus.getPhoneNumber());
                ps.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            break;
            case 3:
                  try (Connection con = Customer.connection();
                  PreparedStatement pr = con.prepareCall("update Customers set Address=? where ID=?")) {
                  pr.setInt(2, cus.getCustomerID());
                  pr.setString(1, cus.getAddress());
                  pr.executeUpdate();
            } catch (SQLException sql) {
                System.out.println(sql.getMessage());
            }
            break;
        }
    }

    @Override
    public boolean cheackCusID(int id) throws Exception {
        try (Connection con = Customer.connection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Customers where ID=" + id);
            if (rs.next() == true) {
                return true;
            } else {
                return false;
            }
        }

    }
    
    @Override
    public Customers getCustomerDetails(int customerID) throws Exception{
        Customers customer=new Customers();
        try(Connection connection=MySql.connection();
             Statement statment= connection.createStatement()){
            ResultSet result=statment.executeQuery("select * from Customers where ID="+customerID);
            while(result.next()){
                customer=new Customers(result.getInt(1),result.getString(2),result.getLong(3),result.getString(4),result.getDouble(5));
            }
        }return customer;
    }

}
