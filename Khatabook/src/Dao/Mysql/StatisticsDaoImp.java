package Dao.Mysql;

import DAO.StatisticsDao;
import Main.MySql;
import Pojo.Customers;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import java.sql.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.sql.Statement;

public class StatisticsDaoImp implements StatisticsDao{
     Orders[] order= new Orders[0];
     Customers[] customer=new Customers[0];
     Inventory[] products=new Inventory[0];
    @Override
    public Orders[] mostPurchasedCustomerbyOneDate(Date date) throws Exception{
        
            try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select CustomerID,sum(TotalPrice),OrderDate from Orders where OrderDate="+"'"+date+"'"+"group by CustomerID order by sum(TotalPrice) desc ")){
                ResultSet result=statement.executeQuery();
                
                while(result.next()){
                    order=Arrays.copyOf(order, order.length+1);
                    order[order.length-1]=new Orders(0,result.getInt(1),result.getDate(3),null,result.getDouble(2));
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return order;
    }

    @Override
    public Orders[] mostPurchasedCustomerbyStartDatetoEndDate(Date date1,Date date2) throws Exception {
        try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select CustomerID,sum(TotalPrice) from Orders where OrderDate between"+"'"+date1+"'"+"and"+"'"+date2+"'"+"group by CustomerID order by sum(TotalPrice) desc ")){
                ResultSet result=statement.executeQuery();
                
                while(result.next()){
                    order=Arrays.copyOf(order, order.length+1);
                    order[order.length-1]=new Orders(0,result.getInt(1),null,null,result.getDouble(2));
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return order;
    }

    @Override
    public Customers[] paidCustomers() throws Exception{
            try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select * from Customers where BalanceAmount=0.00")){
                ResultSet result=statement.executeQuery();
                while(result.next()){
                    customer=Arrays.copyOf(customer, customer.length+1);
                    customer[customer.length-1]=new Customers(result.getInt(1),null,0,null,result.getDouble(5));
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return customer;
        
    }

    @Override
    public Customers[] unPaidCustomers() throws Exception {
        
             try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select * from Customers where BalanceAmount !=0.00 order by BalanceAmount desc")){
                ResultSet result=statement.executeQuery();
                while(result.next()){
                    customer=Arrays.copyOf(customer, customer.length+1);
                    customer[customer.length-1]=new Customers(result.getInt(1),null,0,null,result.getDouble(5));
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return customer;
    }

    @Override
    public Inventory[] mostSoldProducts()  throws Exception{
        
              try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select ProductID,SoldProducts from Inventory order by SoldProducts desc")){
                ResultSet result=statement.executeQuery();
                while(result.next()){
                    products=Arrays.copyOf(products, products.length+1);
                    Inventory inv=new Inventory();
                    inv.setProductID(result.getInt(1));
                    inv.setSoldProductsQuantity(result.getInt(2));
                    products[products.length-1]=inv;
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return products;
    }      

    @Override
    public Inventory[] lessSoldProducts() throws Exception {
        
         try(Connection con=MySql.connection();
                PreparedStatement statement=con.prepareCall("select ProductID,SoldProducts from Inventory order by SoldProducts asc")){
                ResultSet result=statement.executeQuery();
                while(result.next()){
                    products=Arrays.copyOf(products, products.length+1);
                    Inventory inv=new Inventory();
                    inv.setProductID(result.getInt(1));
                    inv.setSoldProductsQuantity(result.getInt(2));
                    products[products.length-1]=inv;
                 
                }
            }catch(SQLException sql){
                System.out.println(sql.getMessage());
                sql.printStackTrace();
            }
        return products;
    }

    @Override
    public Orders[] OrdersListByOneDate(Date date) throws Exception {
        
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = MySql.connection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where OrderDate="+"'"+date+"'");
                while (rs.next()) {
                    Statement st = con.createStatement();
                    ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                    while (result.next()) {
                        lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                        lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5));
                    }
                    order = Arrays.copyOf(order, order.length + 1);
                        order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));

                }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }
        return order;
    }

    @Override
    public Orders[] OrdersListByStartDatetoEndDate(Date date1,Date date2)throws Exception {
          LineItems[] lineitem = new LineItems[0];
        try (Connection con = MySql.connection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where OrderDate between"+"'"+date1+"'"+"and"+"'"+date2+"'");
                while (rs.next()) {
                    Statement st = con.createStatement();
                    ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                    while (result.next()) {
                        lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                        lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5));
                    }
                    order = Arrays.copyOf(order, order.length + 1);
                        order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));

                }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }
        return order;
    }
    
    
}
