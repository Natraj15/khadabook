package Dao.Mysql;

import DAO.OrdersDao;
import Main.MySql;
import Pojo.Inventory;
import Pojo.LineItems;
import Pojo.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class OrdersDaoImp implements OrdersDao {

    @Override
    public void placeOrder(Orders order, Inventory inv) throws Exception {
        try (Connection con = MySql.connection()) {
            PreparedStatement stv = con.prepareCall("insert into Orders (CustomerID,TotalPrice) values(" + order.getCustomerId() + ',' + order.getTotalPrice() + ")");
            stv.executeUpdate();
            for (LineItems i : order.getLine()) {
                Statement ment = con.createStatement();
                ResultSet result = ment.executeQuery("select max(OrderID) from Orders");
                while (result.next()) {
                    int Orderid = result.getInt(1);
                    int productId = i.getProductId();
                    PreparedStatement ps = con.prepareCall("insert into  LineItems  (OrderID,ProductID,ProductQuantity,Price) values(?,?,?,?)");
                    ps.setInt(1, Orderid);
                    int Quantity = i.getQuantity();
                    ps.setInt(2, productId);
                    ps.setInt(3, Quantity);
                    double price = i.getPrice();
                    ps.setDouble(4, price);
                    ps.executeUpdate();
                    PreparedStatement pps = con.prepareCall("update Customers set BalanceAmount=BalanceAmount+? where ID=?");
                    pps.setDouble(1, price);
                    pps.setInt(2, order.getCustomerId());
                    pps.executeUpdate();
                    PreparedStatement pre = con.prepareCall("update Inventory set ProductQuantity=?,SoldProducts=? where ProductID=?");
                    pre.setInt(1, inv.getProductQuantity());
                    pre.setInt(2, inv.getSoldProductsQuantity());
                    pre.setInt(3, i.getProductId());
                    pre.executeUpdate();

                }

            }
        } catch (SQLException s) {
            System.err.println(s.getMessage());
            s.printStackTrace();
        }
    }

    @Override
    public Orders[] viewAllOrders() throws Exception {
        Orders[] order = new Orders[0];
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = MySql.connection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders");
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5));
                }
                order = Arrays.copyOf(order, order.length + 1);
                order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));

            }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }
        return order;
    }

    @Override
    public Orders[] viewOrdersbyCustomerID(int customerID) throws Exception {
        Orders[] order = new Orders[0];
        LineItems[] lineitem = new LineItems[0];
        try (Connection con = MySql.connection(); Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Orders where CustomerID=" + customerID);
            while (rs.next()) {
                Statement st = con.createStatement();
                ResultSet result = st.executeQuery("select * from LineItems where OrderID=" + rs.getInt(1));
                while (result.next()) {
                    lineitem = Arrays.copyOf(lineitem, lineitem.length + 1);
                    lineitem[lineitem.length - 1] = new LineItems(result.getInt(1), result.getInt(2), result.getInt(3), result.getInt(4), result.getDouble(5));
                }
                order = Arrays.copyOf(order, order.length + 1);
                order[order.length - 1] = new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), lineitem, rs.getDouble(4));

            }

        } catch (SQLException s) {
            System.out.println(s.getMessage());
            s.printStackTrace();
        }
        return order;
    }
}
