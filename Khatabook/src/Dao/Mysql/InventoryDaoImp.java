package Dao.Mysql;

import DAO.InventoryDao;
import Main.MySql;
import Pojo.Inventory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import khatabook.Customer;

public class InventoryDaoImp implements InventoryDao{

        BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
    @Override
    public void addProduct(Inventory product) throws Exception {
         try(Connection con=MySql.connection();
           PreparedStatement pre=con.prepareCall("insert into Inventory (ProductName,ProductQuantity,OneProductprice,TotalProductprice) values(?,?,?,?)")){
           pre.setString(1,product.getProductName());
           pre.setInt(2,product.getProductQuantity());
           pre.setDouble(3, product.getOneProductprice());
           pre.setDouble(4, product.getTotalProductprice());
           pre.executeUpdate();
          }catch(SQLException e){
           System.err.println(e.getMessage());
        }
    }

    @Override
    public void viewProduct(int choose, int id) throws Exception {
       switch(choose){
           case 1:
                      try(Connection con =Customer.connection();
                      Statement pre=con.createStatement();
                      ResultSet rs=pre.executeQuery("select * from Inventory")){
                      System.out.println("-".repeat(102));
                      System.out.printf("|%-12s|%-30s|%-15s|%-18s|%-19s|","ProuductID","Proudct Name"," Product Quantity"," One Product Price", "Total Product Price");
                  while(rs.next()){
                    System.out.println();
                    System.out.print("-".repeat(101)+"|");
                    System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getDouble(5));
                }   System.out.println("\n"+"-".repeat(101));
              }catch(SQLException s){
               System.err.println(s.getMessage());
              }
               break;
           case 2:
               try(Connection con =Customer.connection();
                 PreparedStatement pre=con.prepareCall("select * from Inventory where ProductID=?"))
              {
                   pre.setInt(1, id);
                   ResultSet rs=pre.executeQuery();
                   System.out.println("-".repeat(102));
                   System.out.printf("|%-12s|%-30s|%-15s|%-18s|%-19s|","ProuductID","Proudct Name"," Product Quantity"," One Product Price", "Total Product Price");
               while(rs.next()){
                   System.out.println();
                   System.out.print("-".repeat(101)+"|");
                   System.out.format("\n|%-12s|%-30s|%-17s|%-18s|%-19s|",rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getDouble(4),rs.getDouble(5));
                }  System.out.println("\n"+"-".repeat(101));
              }catch(SQLException s){
               System.err.println(s.getMessage());
          
             }
               break;
       }
        
    }

    @Override
    public void updateProduct(int choose,Inventory inv) throws Exception {
        
          switch(choose){
              case 1:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set ProductName=? where ProductID=?")){
                      pre.setInt(2, inv.getProductID());
                      String name=inv.getProductName();
                      pre.setString(1, name);
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                case 2:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set ProductQuantity=? where ProductID=?")){
                      pre.setInt(2, inv.getProductID());
                      pre.setInt(1, inv.getProductQuantity());
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                case 3:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set OneProductPrice=? where ProductID=?")){
                      pre.setInt(2, inv.getProductID());
                      pre.setDouble(1, inv.getOneProductprice());
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                  case 4:try(Connection con=Customer.connection();
                      PreparedStatement pre=con.prepareCall("update Inventory set TotalProductPrice=? where ProductID=?")){
                      pre.setInt(2, inv.getProductID());
                      pre.setDouble(1, inv.getTotalProductprice());
                      pre.executeUpdate();
                  }catch(SQLException s){
                     System.err.println(s.getMessage());
                 }break;
                
          }
        
    }
    
        @Override
    public boolean CheackProductId(int id) throws Exception{
         try (Connection con = MySql.connection();
                 Statement sta = con.createStatement()) {
            ResultSet rs = sta.executeQuery("select * from Inventory where ProductID=" + id);
            if (rs.next() == true) {
                return true;
            } else {
                return false;
            }
        }

    }
      @Override
    public Inventory getProductDetails(int productId) throws Exception {
        Inventory inv = null;
        try (Connection con = MySql.connection()) {
            Statement sta = con.createStatement();

            ResultSet rs = sta.executeQuery("select * from Inventory where ProductID=" + productId);
            while (rs.next()) {
                inv = new Inventory(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDouble(5), rs.getInt(6));
            }
        }
        return inv;
    }
    
}
