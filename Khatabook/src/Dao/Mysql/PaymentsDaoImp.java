package Dao.Mysql;

import DAO.PaymentsDao;
import Main.MySql;
import Pojo.Payments;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;


public class PaymentsDaoImp implements PaymentsDao{

    @Override
    public void addPayment(Payments pay) throws Exception{
        try(Connection connection=MySql.connection();
                 
            PreparedStatement statement =connection.prepareCall("insert into Payments (CustomerID,PaymentAmount,BalanceAmount) values ("+pay.getCustomerID()+','+pay.getPaymentAmount()+','+pay.getBalanceAmount()+')' )){
            statement.executeUpdate();
            PreparedStatement state=connection.prepareCall("update Customers set BalanceAmount="+pay.getBalanceAmount()+"where ID="+pay.getCustomerID());
            state.executeUpdate();
        }catch(SQLException s){
            System.out.println(s.getMessage());
            s.printStackTrace();
        }
                 
    }
    
   
}
