package Pojo;

public class Customers {
   
    private int customerID;
    private String name;
    private long phoneNumber;
    private String address;
    private double balanceAmount;

    public Customers() {
    }

    public Customers(String Name, long PhoneNumber, String Address) {
        this.name = Name;
        this.phoneNumber = PhoneNumber;
        this.address = Address;
    }

    public Customers(int customerID, String name, long phoneNumber, String address, double balanceAmount) {
        this.customerID = customerID;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.balanceAmount = balanceAmount;
    }
    
            
     public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long PhoneNumber) {
        this.phoneNumber = PhoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String Address) {
        this.address = Address;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double BalanceAmount) {
        this.balanceAmount = BalanceAmount;
    }

    @Override
    public String toString() {
        return "Customer{" + "Name=" + name + ", PhoneNumber=" + phoneNumber + ", Address=" + address + ", BalanceAmount=" + balanceAmount +  '}';
    }
    
    
}


