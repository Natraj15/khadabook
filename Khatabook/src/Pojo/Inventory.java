package Pojo;

public class Inventory {
    
  
    private int productID;
    private String productName;
    private int productQuantity;
    private double oneProductprice;
    private double totalProductprice;
    private int soldProductsQuantity;
    public Inventory() {
    }

    public Inventory(int productID, String productName, int productQuantity, double oneProductprice, double totalProductprice, int soldProductsQuantity) {
        this.productID = productID;
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.oneProductprice = oneProductprice;
        this.totalProductprice = totalProductprice;
        this.soldProductsQuantity = soldProductsQuantity;
    }
    
    public Inventory(String productName, int productQuantity, double oneProductprice, double totalProductprice) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.oneProductprice = oneProductprice;
        this.totalProductprice = totalProductprice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public double getOneProductprice() {
        return oneProductprice;
    }

    public void setOneProductprice(double oneProductprice) {
        this.oneProductprice = oneProductprice;
    }

    public double getTotalProductprice() {
        return totalProductprice;
    }

    public void setTotalProductprice(double totalProductprice) {
        this.totalProductprice = totalProductprice;
    }

    public int getSoldProductsQuantity() {
        return soldProductsQuantity;
    }

    public void setSoldProductsQuantity(int soldProductsQuantity) {
        this.soldProductsQuantity = soldProductsQuantity;
    }
    

    @Override
    public String toString() {
        return "Inventory{" + "productName=" + productName + ", productID=" + productID + ", productQuantity=" + productQuantity + ", oneProductprice=" + oneProductprice + ", totalProductprice=" + totalProductprice +", oneProductprice=" + soldProductsQuantity+  '}';
    }
    
    
}
