package Pojo;


import java.util.Arrays;
import java.util.Date;


public class Orders {
    
    int orderId;
    int customerId;
    Date date;
    LineItems[] line;
    double totalPrice;

    public Orders() {
    }

    public Orders(int orderId, int customerId, Date date, LineItems[] line , double totalPrice) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.date = date;
        this.line=line;
        this.totalPrice = totalPrice;
    }

    public LineItems[] getLine() {
        return line;
    }

    public void setLine(LineItems[] line) {
        this.line = line;
    }
     
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Orders{" + "orderId=" + orderId + ", customerId=" + customerId + ", date=" + date + ", line=" + Arrays.toString(line) + ", totalPrice=" + totalPrice + '}';
    }

}
