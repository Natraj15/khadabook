package Pojo;

public class Payments {
    
    int paymentID;
    int customerID;
    double paymentAmount;
    double BalanceAmount;

    public Payments() {
    }

    public Payments(int paymentID, int customerID, double paymentAmount, double BalanceAmount) {
        this.paymentID = paymentID;
        this.customerID = customerID;
        this.paymentAmount = paymentAmount;
        this.BalanceAmount = BalanceAmount;
    }
        public Payments( int customerID, double paymentAmount, double BalanceAmount) {
        this.customerID = customerID;
        this.paymentAmount = paymentAmount;
        this.BalanceAmount = BalanceAmount;
    }  
    
    public int getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getBalanceAmount() {
        return BalanceAmount;
    }

    public void setBalanceAmount(double BalanceAmount) {
        this.BalanceAmount = BalanceAmount;
    }

    @Override
    public String toString() {
        return "Payments{" + "paymentID=" + paymentID + ", customerID=" + customerID + ", paymentAmount=" + paymentAmount + ", BalanceAmount=" + BalanceAmount + '}';
    }
    
    
}
