package DAO;

import Pojo.Inventory;
import Pojo.Orders;

public interface OrdersDao {
    
    public void placeOrder(Orders order,Inventory inv) throws Exception;
    
    public Orders[] viewAllOrders() throws Exception;
    
    public Orders[] viewOrdersbyCustomerID(int customerID) throws Exception;
}
