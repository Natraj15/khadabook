package DAO;

import Pojo.Customers;
import Pojo.Inventory;
import Pojo.Orders;
import java.sql.Date;

public interface StatisticsDao {
    
    public Orders[] mostPurchasedCustomerbyOneDate(Date date) throws Exception;
    
    public Orders[] mostPurchasedCustomerbyStartDatetoEndDate(Date date1,Date date2)throws Exception;
    
    public Customers[] paidCustomers() throws Exception;
    
    public Customers[] unPaidCustomers() throws Exception;
    
    public Inventory[] mostSoldProducts() throws Exception;
    
    public Inventory[] lessSoldProducts() throws Exception;
    
    public Orders[] OrdersListByOneDate(Date date) throws Exception;
    
    public Orders[] OrdersListByStartDatetoEndDate(Date date1,Date date2)throws Exception;
}
