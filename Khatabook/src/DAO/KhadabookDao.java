package DAO;


public interface KhadabookDao {
    
     public CustomerDao getCustomer();
     
     public InventoryDao getInventory();
     
     public OrdersDao getOrders();
     
     public PaymentsDao getPayments();
     
     public StatisticsDao getStatistics();
}
