package DAO;

import Pojo.Customers;



public interface CustomerDao {
    
    public void addCustomer(Customers cus) throws Exception;
    
    public void viewCustomer(int choose,int id) throws Exception;
  
    public void updateCustomer(int ch,Customers cus) throws Exception;
    
    public boolean cheackCusID(int id) throws Exception;
    
    public Customers getCustomerDetails(int customerID) throws Exception;
}
