package DAO;

import Pojo.Inventory;

public interface InventoryDao {
    
    public  void addProduct(Inventory inv) throws Exception;
    
    public void viewProduct(int choose, int id) throws Exception;
    
    public void updateProduct(int choose,Inventory inv) throws Exception;
    
    public boolean CheackProductId(int id) throws Exception;
    
     public Inventory getProductDetails(int productId) throws Exception;
}
