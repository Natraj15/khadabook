package Main;

import DAO.KhadabookDao;
import Pojo.Inventory;
import Dao.Mysql.MysqlConnection;
import khatabook.OrderListByDate;
import Pojo.Orders;
import Pojo.Customers;
import Pojo.LineItems;
import Pojo.Payments;
import khatabook.Statistics;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.sql.Date;

public class KhataBook {
    
   
    public static void main(String[] args) throws Exception{
         BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
          KhadabookDao khadabook= new MysqlConnection();
        boolean b=true;       
        while(b){
             System.out.println("*".repeat(19));
             System.out.println("<>     MENU      <>");
             System.out.println("<>  1.Customer   <>");
             System.out.println("<>  2.Inventory  <>");
             System.out.println("<>  3.Statics    <>");
             System.out.println("<>  4.Exit       <>");
             System.out.println("*".repeat(19));
             int nu=Validation.getInt();
             boolean bl=true;
             switch(nu){
                 case 1:
                        
                        while(bl){
                        System.out.println("[]".repeat(10));    
                        System.out.println("| 1.add customer   |");
                        System.out.println("| 2.view customer  |");
                        System.out.println("| 3.update customer|");
                        System.out.println("| 4.Place Order    |");
                        System.out.println("| 5.Make Payment   |");
                        System.out.println("| 6.view Orders    |");
                        System.out.println("| 7.Exit           |");
                        System.out.println("[]".repeat(10));
                       int num=Validation.getInt();
                       switch(num){
                       case 1:
                             System.out.println("ENTER THE CUSTOMER NAME");
                             String Name  = read.readLine();
                             System.out.println("ENTER THE CUSTOMER PHONE NUMBER");
                             long PhoneNumber = Long.parseLong(read.readLine());
                             System.out.println("ENTER THE CUSTOMER ADDRESS");
                             String Address = read.readLine();
                             Customers cus1=new Customers(Name,PhoneNumber,Address);
                             khadabook.getCustomer().addCustomer(cus1);
                             
                            break;
                       case 2:
                                  System.out.println("1.All Customer Details");
                                  System.out.println("2.One Customer Details");
                                  int numb=Validation.getInt();
                                  switch(numb){
                                  case 1:khadabook.getCustomer().viewCustomer(numb,0);
                                   break;
                                  case 2:  
                                    while(true){  
                                   System.out.println("Enter The Customer ID");
                                   int id=Validation.getInt();
                                   if(khadabook.getCustomer().cheackCusID(id)){
                                   khadabook.getCustomer().viewCustomer(numb,id);
                                    break;
                                   }else System.err.println("CustomerID Not Exist");
                                    }
                                   break;
                                default :System.err.println("Enter the Connrect option");
                               }
       
                         break;
                       case 3:   
                               System.out.println("Enter The Customer ID");
                               int cusId=Validation.getInt();
                               if(khadabook.getCustomer().cheackCusID(cusId)==true)
                                     
                               {    
                               System.out.println("1.Change The Name");
                               System.out.println("2.Change The PhoneNumber");
                               System.out.println("3.Change the Address");
                               Customers cus2=new Customers();
                               cus2.setCustomerID(cusId);
                               int choose=Validation.getInt();
                               switch(choose){
                                   case 1: 
                                           System.out.println("Change The Name");
                                           String name=read.readLine();
                                           cus2.setName(name);
                                           khadabook.getCustomer().updateCustomer(choose, cus2);
                                       break;
                                   case 2:
                                           System.out.println("Change The PhoeNumber");
                                           long phone=Long.parseLong(read.readLine());
                                           cus2.setPhoneNumber(phone);
                                           khadabook.getCustomer().updateCustomer(choose, cus2);
                                       break;

                                   case 3:
                                           System.out.println("Change The Address");
                                           String address=read.readLine();
                                           cus2.setAddress(address);
                                           khadabook.getCustomer().updateCustomer(choose, cus2);
                                       break;
                                       
                                    }  
                               }  
                                 
                               else{
                                        System.err.println("Customer ID Not Exist");    
                                            }  
                                             
                         break;
                       case 4:  boolean cheack=true;
                               LineItems[] lineItem=new LineItems[0];
                               Orders order=new Orders();
                               Inventory in=new Inventory();
                               double totalPrice=0.0;
                                System.out.println("Enter The Customer ID");
                                int id=Validation.getInt();
                                if(khadabook.getCustomer().cheackCusID(id)){
                                     order.setCustomerId(id);
                                     while(cheack){
                                          System.out.println("Enter The Product ID");
                                          int proid=Validation.getInt();
                                          if(khadabook.getInventory().CheackProductId(proid)==true){
                                              Inventory inv= khadabook.getInventory().getProductDetails(proid);
                                              System.out.println("Enter The Quantity");
                                              int quantity=Validation.getInt();
                                              if(quantity<inv.getProductQuantity()){
                                              double price=quantity*inv.getOneProductprice();
                                               totalPrice +=price;
                                               order.setTotalPrice(totalPrice);
                                               lineItem = Arrays.copyOf(lineItem, lineItem.length+1);
                                               lineItem[lineItem.length-1] = new LineItems(proid,quantity,price);
                                              order.setLine(lineItem);
                                              int balanceproductQuantity=inv.getProductQuantity()-quantity;
                                              in.setProductQuantity(balanceproductQuantity);
                                              int soldProductQuantity=inv.getSoldProductsQuantity()+quantity;
                                              in.setSoldProductsQuantity(soldProductQuantity);
                                              
                                                 System.out.println("1.Add Products");
                                                 System.out.println("2.Exit");
                                                 int add = Validation.getInt();
                                                  switch (add) {
                                                  case 1:
                                                   cheack = true;
                                                  break;
                                                  case 2:
                                                   cheack = false;
                                                  break;
                                                 }
                                              } 
                                              else{
                                                  System.err.println("Not Enough products");
                                              }   
                                          }else{
                                              System.err.println("Product ID Not Exist");
                                        }
                                     }khadabook.getOrders().placeOrder(order,in);
                                   
                                }
                                else{
                                    System.err.println("Customer ID Not Exist");
                                }
                         break;
                       case 5:System.out.println("Enter The Customer's ID");
                              int customerId=Validation.getInt();
                              if(khadabook.getCustomer().cheackCusID(customerId)==true){
                                  Customers customer= khadabook.getCustomer().getCustomerDetails(customerId);
                                  System.out.println("Enter The Payment Amount");
                                  double paymentAmount=Validation.getDouble();
                                  if(paymentAmount<=customer.getBalanceAmount()){
                                 double balanceAmount =customer.getBalanceAmount()-paymentAmount;
                                  Payments pay=new Payments(customerId,paymentAmount,balanceAmount);
                                  khadabook.getPayments().addPayment(pay);
                                  }else{
                                      System.err.println("Your Balnce Amount only "+customer.getBalanceAmount());
                                  }
                                  
                              }else{
                                  System.err.println("Customer ID Not Exists");
                              }
                          break;
                       case 6: System.out.println("1.View All Orders List");
                               System.out.println("2.View One Customer's Orders List");
                               int choose=Validation.getInt();
                               switch(choose){
                                   case 1:Orders[] order2=khadabook.getOrders().viewAllOrders();
                                          Validation.printOrders(order2);
                                   case 2:System.out.println("Enter CustomerID");
                                          int customerID=Validation.getInt();
                                       Orders[] order3=khadabook.getOrders().viewOrdersbyCustomerID(customerID);
                                       Validation.printOrders(order3);
                               }
                               
                          break;
                       case 7:bl=false;
                         break;
                        }
                     
                       }
                     break;
                 case 2:
                     Inventory inventory=new Inventory();
                     boolean tru=true;
                     while(tru){
                         System.out.println("*".repeat(19));
                         System.out.println("/1.Add Product    /");
                         System.out.println("/2.View Product   /");
                         System.out.println("/3.Update Product /");
                         System.out.println("/4.Exit           /");
                         System.out.println("*".repeat(19));
                         int Product=Validation.getInt();
                         switch(Product){
                             case 1:
                                   System.out.println("Enter the ProductName");
                                   String productName=read.readLine();
                                   System.out.println("Enter The ProductQuantity");
                                   int productQuantity=Validation.getInt();
                                   System.out.println("Enter the OneProductprice");
                                   double onePrice=Validation.getDouble();
                                   System.out.println("Enter the TotalProductPrice");
                                   double totPrice=Validation.getDouble();
                                   Inventory Int=new Inventory(productName,productQuantity,onePrice,totPrice);
                                   khadabook.getInventory().addProduct(Int);
                                   break;
                             case 2:
                                 
                                  System.out.println("1.All Product Details");
                                  System.out.println("2.One Product Details");
                                  int numb=Validation.getInt();
                                  switch(numb){
                                  case 1:khadabook.getInventory().viewProduct(numb, 0);
                                   break;
                                  case 2:  
                                    while(true){  
                                   System.out.println("Enter The Product ID");
                                   int id=Validation.getInt();
                                   if(khadabook.getInventory().CheackProductId(id)==true){
                                    khadabook.getInventory().viewProduct(numb, id);
                                    break;
                                   }else System.err.println("Product ID Not Exist");
                                    }
                                   break;
                                default :System.err.println("Enter the Connrect option");
                               }
       
                                 break;
                             case 3:System.out.println("Enter The Product ID");
                                    int proid=Validation.getInt();
                                    if(khadabook.getInventory().CheackProductId(proid)==true){
                                         System.out.println("1.Change The Product Name");
                                         System.out.println("2.Change The Product Quantity");
                                         System.out.println("3.Change One Product Price");
                                         System.out.println("4.Change Totoal Product Price");
                                         Inventory pro=new Inventory();
                                         pro.setProductID(proid);
                                         int choose=Validation.getInt();
                               switch(choose){
                                   case 1: 
                                           System.out.println("Enter The Product Name");
                                           String name=read.readLine();
                                           pro.setProductName(name);
                                           khadabook.getInventory().updateProduct(choose, pro);
                                       break;
                                   case 2:
                                           System.out.println("Enter The Quantity");
                                           int qua=Validation.getInt();
                                           pro.setProductQuantity(qua);
                                           khadabook.getInventory().updateProduct(choose, pro);
                                       break;
                                   case 3:
                                           System.out.println("Enter One Product Price");
                                           double oneprice=Validation.getDouble();
                                           pro.setOneProductprice(oneprice);
                                           khadabook.getInventory().updateProduct(choose, pro);
                                       break;
                                   case 4:System.out.println("Enter Total Product Price"); 
                                          double totalprice=Validation.getDouble();
                                          pro.setTotalProductprice(totalprice);
                                          khadabook.getInventory().updateProduct(choose, pro);
                                         break;
                                   default :System.err.println("Enter the Correct option");      
                                    }  
                                    }else{
                                        System.err.println("Product ID Not Exist");
                                    }
                                 break;
                            case 4:tru=false;
                            break;
                         }
                     }break;
                 case 3:
                      
                       boolean sta=true;
                       while(sta){
                           System.out.println("=".repeat(40));
                           System.out.println("1.Most Purchased Customers List By One_Date");
                           System.out.println("2.Most Purchased Customers By Start_Date to End_Date");
                           System.out.println("3.Paid Customers");
                           System.out.println("4.UnPaid Customers");
                           System.out.println("5.Most Sold Products");
                           System.out.println("6.Less Sold Products");
                           System.out.println("7.Orders List By One_Date");
                           System.out.println("8.Orders List By Start_Date to End_Date");
                           System.out.println("9.Exit");
                           System.out.println("=".repeat(40));
                           int num=Validation.getInt();
                            Statistics statics=new Statistics();
                            OrderListByDate da=new OrderListByDate();
                           switch(num){
                               case 1:
                                   System.out.println("Enter the Date (YYYY-MM-DD)");
                                   Date date=Validation.getDate();
                                   Orders[] order=khadabook.getStatistics().mostPurchasedCustomerbyOneDate(date);
                                   if(order.length>0){
                                   System.out.println("-".repeat(30));
                                   System.out.printf("|%-12s|%-16s|","CustomerID","TotalPrice");
                                   for(Orders order1:order){
                                       System.out.println();
                                       System.out.print("|"+"-".repeat(29)+"|");    
                                       System.out.format("\n|%-12s|%-16s|",order1.getCustomerId(),order1.getTotalPrice()); 
                                       
                                   }System.out.println("\n"+"-".repeat(30));
                                   }else System.err.println("No Order");
                                   break;

                                case 2:
                                   System.out.println("Enter the Start_Date (YYYY-MM-DD)");
                                   Date date1=Validation.getDate();
                                   System.out.println("Enter The End_Date");
                                   Date date2=Validation.getDate();
                                   Orders[] order1=khadabook.getStatistics().mostPurchasedCustomerbyStartDatetoEndDate(date1, date2);
                                      if(order1.length>0){
                                   System.out.println("-".repeat(30));
                                   System.out.printf("|%-12s|%-16s|","CustomerID","TotalPrice");
                                   for(Orders orders:order1){
                                       System.out.println();
                                       System.out.print("|"+"-".repeat(29)+"|");    
                                       System.out.format("\n|%-12s|%-16s|",orders.getCustomerId(),orders.getTotalPrice()); 
                                       
                                   }System.out.println("\n"+"-".repeat(30));
                                   }else System.err.println("No Order");
                                   break;
                               case 3:
                                   Customers[] customer=khadabook.getStatistics().paidCustomers();
                                      if(customer.length>0){
                                   System.out.println("-".repeat(30));
                                   System.out.printf("|%-12s|%-16s|","CustomerID","BalanceAmount");
                                   for(Customers cust:customer){
                                       System.out.println();
                                       System.out.print("|"+"-".repeat(29)+"|");    
                                       System.out.format("\n|%-12s|%-16s|",cust.getCustomerID(),cust.getBalanceAmount()); 
                                       
                                   }System.out.println("\n"+"-".repeat(30));
                                   }else System.err.println("No Customers");
                                   break;
                               case 4:
                                    Customers[] customer1=khadabook.getStatistics().unPaidCustomers();
                                      if(customer1.length>0){
                                   System.out.println("-".repeat(30));
                                   System.out.printf("|%-12s|%-16s|","CustomerID","BalanceAmount");
                                   for(Customers cust:customer1){
                                       System.out.println();
                                       System.out.print("|"+"-".repeat(29)+"|");    
                                       System.out.format("\n|%-12s|%-16s|",cust.getCustomerID(),cust.getBalanceAmount()); 
                                       
                                   }System.out.println("\n"+"-".repeat(30));
                                   }else System.err.println("No Customers");
                                   break;
                               case 5: 
                                       Inventory[] products=khadabook.getStatistics().mostSoldProducts();
                                       System.out.println("-".repeat(36));
                                       System.out.printf("|%-12s|%-16s|","ProductID","Sold Products Quantity"); 
                                       for(Inventory product:products){
                                            System.out.println();
                                            System.out.print("|"+"-".repeat(35)+"|");    
                                            System.out.format("\n|%-12s|%-22s|",product.getProductID(),product.getSoldProductsQuantity());   
                                       }System.out.println("\n"+"-".repeat(36));
                                    break;
                               case 6:
                                       Inventory[] product=khadabook.getStatistics().lessSoldProducts();
                                       System.out.println("-".repeat(36));
                                       System.out.printf("|%-12s|%-16s|","ProductID","Sold Products Quantity"); 
                                       for(Inventory pro:product){
                                            System.out.println();
                                            System.out.print("|"+"-".repeat(35)+"|");    
                                            System.out.format("\n|%-12s|%-22s|",pro.getProductID(),pro.getSoldProductsQuantity());   
                                       }System.out.println("\n"+"-".repeat(36));
                                   break;
                               case 7:
                                      System.out.println("Enter the Date (YYYY-MM-DD)");
                                      Date date3=Validation.getDate();
                                      Orders[] orders=khadabook.getStatistics().OrdersListByOneDate(date3);
                                      Validation.printOrders(orders);
                                    
                                    break;
                               case 8:
                                      System.out.println("Enter the Date (YYYY-MM-DD)");
                                      Date date4=Validation.getDate();
                                      System.out.println("Enter the Date (YYYY-MM-DD)");
                                      Date date5=Validation.getDate();
                                      Orders[] order2=khadabook.getStatistics().OrdersListByStartDatetoEndDate(date4, date5);
                                      Validation.printOrders(order2);
                                   
                                  break;
                               case 9:sta=false;   
                           }
                       }break;
                 case 4:b=false;
                 break;
             }
          
        }  
    }

}
    

