package Main;

import Pojo.LineItems;
import Pojo.Orders;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Date;

public class Validation {
    
    static BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
    public static int getInt() throws Exception{
         int num=0;
         
         while(true){
         try{
              num=Integer.parseInt(read.readLine());
              break;
         }
         catch(NumberFormatException n){
             System.err.println("Number Only Accepted");
         } 
     } 
     return num;
   }
        
    public static double getDouble() throws Exception{
         double num=0.0;
         while(true){
         try{
              num=Double.parseDouble(read.readLine());
              break;
         }
         catch(NumberFormatException n){
             System.err.println("Number Only Accepted");
         } 
     } 
     return num;
   }
    
    public static long getLong() throws Exception{
         long num=0;
         while(true){
         try{
              num=Long.parseLong(read.readLine());
              break;
         }
         catch(NumberFormatException n){
             System.err.println("Number Only Accepted");
         } 
     } 
     return num;
   }

    public static Date getDate() throws Exception{
        Date date=null;
        while(true){
        try{
            date=Date.valueOf(read.readLine());
            break;
        }catch(IllegalArgumentException ia){
            System.err.println("Enter Only Date Format(YYYY-MM-DD)");
        }
        }
        
        return date;
    }
    public static void main(String[] args) throws Exception{
       Date date= getDate();
    }
    public static void printOrders(Orders[] order){
                             if(order.length>0){
                                for(Orders ord:order){
                                System.out.println("_".repeat(25));  
                                System.out.println("");
                                System.out.println("Order ID    : "+ord.getOrderId());
                                System.out.println("Customer ID : "+ord.getCustomerId());
                                System.out.println("Date        : "+ord.getDate());
                              for(LineItems li: ord.getLine()){
                                System.out.println("LineItem ID : "+li.getLineItemId());  
                                System.out.println("product ID  : "+li.getProductId());
                                System.out.println("Quantity    : "+li.getQuantity());
                                System.out.println("price       : "+li.getPrice());
                                 }
                                System.out.println("Total Price : "+ord.getTotalPrice());
                                             System.out.println("     #Thank You#");
                                System.out.println("_".repeat(25));
                                System.out.println(" ");
                                  }
                              }else {
                                    System.err.println("No records are Found"); 
                                }
    }
}
