package Abstraction;

public class ClassC extends AbstractClassB{

    @Override
    void display() {

        System.out.println("display");
    }

    @Override
    public void getPrint() {
         System.out.println("getPrint");
    }

    @Override
    public void play() {
          System.out.println("play");
    }
    public static void main(String[] args) {
        ClassC c=new ClassC();
        c.getAmount();
    }
}
