package Abstraction;

public class ObjectClass implements Cloneable{
    
    int id;
    String name;

    public ObjectClass() {
    }

    
    public ObjectClass(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public void printDetails(int id,String name){
        System.out.println("ID :"+id+"Name :"+name);
    }

    @Override
    public String toString() {
        return "ObjectClass{" + "id=" + id + ", name=" + name + '}';
    }
    
    
    
    public static void main(String[] args) throws CloneNotSupportedException{
        ObjectClass object=new ObjectClass(1,"pradeepan");
        ObjectClass object1=new ObjectClass(1,"pradeepan");
        
        ObjectClass object2=(ObjectClass)object1.clone();
     
       System.out.println(object2);
        
        System.out.println(object.hashCode());//it returns the hashcode of the object
        System.out.println(object.hashCode());//
        System.out.println(object1.hashCode());
        System.out.println(object2.hashCode());
        
        System.out.println(object1.getClass());//it returns the object's class name
     
    
        if(object.equals(object2)){
            System.out.println("yes");
        }
        //creating many objects using loops
        
        System.out.println("");
       ObjectClass[] objectclass=new ObjectClass[5];
         for (int i = 0; i < 5; i++) {
            
             objectclass[i]=new ObjectClass(i,"name"+i);
             
        }
       for(ObjectClass cl:objectclass){
           System.out.println(cl);
       }
    }
}
