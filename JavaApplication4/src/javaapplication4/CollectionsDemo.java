package javaapplication4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionsDemo {
    
    
    public static void main(String[] args) {
        
         List<String> list=new ArrayList();
        
        list.add("natraj");
       // list.add(15);
        list.add("vijay");
        
        Iterator iterator=list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        
        
        System.out.println(list);
        
    }
}
