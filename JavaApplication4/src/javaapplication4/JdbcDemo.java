
package javaapplication4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
public class JdbcDemo {
    
    public static void main(String[] args) {
        
        String DBURL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASSWORD="Natraj@1501";
        String query="select * from USERS";
        try( Connection con=DriverManager.getConnection(DBURL, USER, PASSWORD);
             Statement statement=con.createStatement();
             ResultSet result=statement.executeQuery(query)){
           // Class.forName("com.mysql.cj.jdbc.Driver");

             while(result.next()){
                 System.out.println("USER_ID :"+result.getInt(1));
                 System.out.println("USER_NAME :"+result.getString(2));
                 System.out.println("R0LE :"+result.getString(3));
                 System.out.println(" ");
             }
             
       }
        catch(SQLException e){
                   System.out.println(e.getMessage()); 
                }
    }
    
    public static void main2(String[] args) {
        
        String DBURL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASSWORD="Natraj@1501";
      
        try( Connection con=DriverManager.getConnection(DBURL, USER, PASSWORD);
             Statement statement=con.createStatement();
             ResultSet rs=statement.executeQuery("select * from USERS")){
            while(rs.next()){
                System.out.println("USER_ID :"+rs.getInt("USER_ID"));
                System.out.println("USER_NAME :"+rs.getString("USER_NAME"));
                System.out.println("ROLE :"+rs.getString("ROLE"));
            }
            
//              String sql="INSERT INTO USERS VALUES(6,'Natraj')";
//              statement.execute(sql);
             String sql="update USERS SET USER_NAME='NATARAJAN' WHERE USER_ID=5";
               statement.execute(sql);
  //            String sq="DELETE  FROM USERS where USER_ID=23";
  //            statement.execute(sq);
              System.out.println("update  the tables");
        }catch(SQLException e){
            System.err.println(e.getMessage());
        }
    }
    public static void main3(String[] args) throws Exception{
        add();
    }
    
    public static void add() throws Exception {
         
        String DBURL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASSWORD="Natraj@1501";
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
       // String query="select * from USERS";
       try{
           Connection  con=DriverManager.getConnection(DBURL, USER, PASSWORD);
           PreparedStatement ps=con.prepareCall("insert into USERS values(?,?)");
           System.out.println("Enter the user_id");
           int a=Integer.parseInt(br.readLine());
           ps.setInt(1, a);
           System.out.println("Enter the Name");
           String name=br.readLine();
           ps.setString(2, name);
           int rs=ps.executeUpdate();
          // System.out.println(rs);
           ps.close();
           con.close();
           System.out.println("1.CONTINUE");
           System.err.println("2.END");
           int c=Integer.parseInt(br.readLine());
           boolean b=true;
           while(b){
               switch(c){
                   case 1:add();
                   break;
                   case 2:b=false;
                   break;
               }
           }
         
       }catch(SQLException e){
           System.out.println(e.getMessage());
       }
    }      
    public static void main5(String[] args) {
        
        String DBURL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASSWORD="Natraj@1501";
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
       // String query="select * from USERS";
       try{
           Connection  con=DriverManager.getConnection(DBURL, USER, PASSWORD);
           PreparedStatement ps=con.prepareCall("insert into USERS values(?,?)");
           
           ps.setInt(1,5);
           ps.setString(2, "Dinesh");
           ps.executeUpdate();
           
    }catch(SQLException e){
           System.out.println(e.getMessage());
    }
}
    public static void main4(String[] args) {
            String DBURL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASSWORD="Natraj@1501";
        String query="{call getUSERSByUSER_NAME1(?)}";
        try{
               Connection  con=DriverManager.getConnection(DBURL, USER, PASSWORD);
               
               CallableStatement prepareCall =con.prepareCall(query);
               
               prepareCall.setString("RAMESH","VINOTH");
               
               ResultSet resultset=prepareCall.executeQuery();
                  while(resultset.next()){
                      System.out.println("\n----------------------");
                      System.out.println("USER_ID :"+resultset.getInt("USER_ID"));
                      System.out.println("USER_NAME :"+resultset.getString("USER_NAME"));
                  }
   
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        
      }
    
    public static void main6(String[] args)throws Exception{
        
        String URL="jdbc:mysql://bassure.in:3306/Natrajdb";
        String USER="Natraj";
        String PASS="Natraj@1501";
      //  String query="select * from USERS";delete from USERS where USER_ID=?
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        try(Connection con=DriverManager.getConnection(URL, USER, PASS);
            PreparedStatement st=con.prepareCall("select * from USERS");
            PreparedStatement ds=con.prepareCall("insert into USERS values(?,?,?)");
            PreparedStatement pr=con.prepareCall("insert into USERS values(9,'KALAI','backend')");
            ResultSet se=st.executeQuery()){
            //st.setInt(1, 7);
             pr.executeUpdate();
             System.out.println("ENTER USER_ID");
             int id=Integer.parseInt(br.readLine());
             ds.setInt(1, id);
             System.out.println("ENTER USER_NAME");
             String name=br.readLine();
             ds.setString(2, name);
             System.out.println("ENTER ROLE");
             String role=br.readLine();
             ds.setString(3, role);
             ds.executeUpdate();
            while(se.next()){
                System.out.println("USER_ID :"+se.getInt(1));
                System.out.println("USER_NAME :"+se.getString(2));
                String w=se.getString(2)+se.getString(3);
                System.out.println("ROLE :"+se.getString(3));
                System.out.println(" ");
            }
            System.out.println("successful");
        }catch(SQLException e){
            System.err.println(e.getMessage());
        }
    }
}
