package javaapplication4;

import java.util.ArrayList;
import java.util.function.Consumer;

public class LambdaFunctions {
    
    public static void main1(String[] args) {
        LambdaExpression lambda=new LambdaExpression(){
            @Override
            public void print(){
                System.out.println("Hi");
            }
        };
        lambda.print();
    }
    public static void main2(String[] args) {
        LambdaExpression lambda= () ->  System.out.println("Hi lambda");
        lambda.print();
        
    }
    public static void main(String[] args) {
        ArrayList<Integer> list=new ArrayList<>();
        
        list.add(10);
        list.add(20);
        list.add(30);
        
      Consumer cun=(n)-> System.out.println(n);
      list.forEach(cun);
    }
}
