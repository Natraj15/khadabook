package MVCDesignPattern;

public class MVCPatternDemo {
    
    
    public static void main(String[] args) {
        
    Student model=retriveStudent();
    StudentView view=new StudentView();
    StudentController controller=new StudentController(model,view);
    controller.updateView();
    controller.setStudentName("raj");
    controller.updateView();
    
    }
   
    private static Student retriveStudent(){
        Student student=new Student();
        student.setRollNumber(10);
        student.setName("jobin");
        return student;
    }
}
