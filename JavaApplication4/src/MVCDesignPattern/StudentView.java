package MVCDesignPattern;

public class StudentView {
    
    public  void printStudentDetails(int studentRollNo,String studentName){
        System.out.println("     Student"); 
        System.out.println("Student Roll Number :"+studentRollNo);
        System.out.println("Student Name :"+studentName);
        System.out.println("");
    }
}
