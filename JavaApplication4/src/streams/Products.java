package streams;

import static java.lang.Double.sum;
import java.util.ArrayList;
import java.util.List;

public class Products {

     int id;
     String name;
     double price;

    public Products() {
    }

    public Products(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Products{" + "id=" + id + ", name=" + name + ", price=" + price + '}';
    }
     
    public static void main(String[] args) {
        
        List<Products>  products=new ArrayList<>();
        products.add(new Products(1,"sony",20000));
        products.add(new Products(2,"dell",30000));
        products.add(new Products(3,"lenova",10000));
        products.add(new Products(4,"HP ",50000));
        products.add(new Products(5,"Apple",60000));
        
        
        products.stream().filter(a->a.price>=30000).forEach(System.out::println);
        
        double price1=   products.stream().map(a->a.price).reduce(0.,(sum,price)->(sum+price));
       //double price2=   products.stream().map(a->a.price).reduce(0.,Float::sum);
        System.out.println(price1);
       // products.stream().max(a);
       
       
    }
    
}
