package streams;

import java.util.function.Predicate;

public class PredicateDemo {
    
    
    public static void main(String[] args) {
        
        
        
        Predicate<Integer> predicate1= a->(a>10);
        Predicate<Integer> predicate2= a->(a>10);
        
        
        predicate1.and(predicate2);
        
        System.out.println(predicate1.test(2));
        
        
        
        
        
    }
    
}
