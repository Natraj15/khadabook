package com.mycompany.natraj.springcoreapp.spring.web1;

import java.time.LocalDateTime;
import java.util.Objects;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SampleController {

    @GetMapping("/hi")
    @ResponseBody
    public String sayHello(@RequestParam(name = ("un"), required = false) String name,
            @PathVariable(name ="userId",required = false)Integer userId) {
        if (Objects.nonNull(name) && !name.isEmpty()) {
            if(Objects.nonNull((userId))){
                return "Hello " + name +"with ID: "+userId+ " it's Now" + LocalDateTime.now();
            }else{
            return "Hello " + name + " it's Now" + LocalDateTime.now();}
        }else{
        return "Hello, it's Now" + LocalDateTime.now();
        }
    }

    @GetMapping("/newStudent")
    //@ResponseBody
    public String getStudent(ModelMap data) {
      data.addAttribute("student",new Students(2,"dinesh","abc@gmail"));
        return "newStudent";
    }

    @GetMapping(path = "/newStudent2", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Students getStudent2() {

        return new Students(2, "kalai", "abc@gmail");
    }

    @GetMapping(path = "/newStudent3", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public Students getStudent3() {

        return new Students(3, "arun", "abc@gmail");
    }

}
