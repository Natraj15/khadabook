<%-- 
    Document   : newStudent
    Created on : 06-Jun-2023, 5:36:00 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <div class='nameValue'>studentId: ${student.id}</div>
        <div class='nameValue'>Name ${student.name}</div>
        <div class='nameValue'>email ${student.email}</div>

    </body>
</html>
