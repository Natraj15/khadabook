package com.mycompany.jsp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletContextDemo extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse responce) throws IOException{
        
        ServletContext sc=getServletContext();
        //ServletConfig stc=getServletConfig();
        String str=sc.getInitParameter("name");
        
        PrintWriter out=responce.getWriter();
        out.print(str);
    }
}
