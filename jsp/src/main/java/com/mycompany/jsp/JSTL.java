package com.mycompany.jsp;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/jstl")
public class JSTL extends HttpServlet {
    
    @Override
    public void service(HttpServletRequest request, HttpServletResponse responce) throws ServletException,IOException{
        
        String name="jobin";
        List<Student> student=Arrays.asList(new Student(3,"asif"),new Student(4,"jobin"));
        request.setAttribute("student", student);
        RequestDispatcher rd=request.getRequestDispatcher("Home.jsp");
        rd.forward(request, responce);
        
        
    }
}
