package com.mycompany.jsp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/multiply")
public class Multiply extends HttpServlet {
    
   @Override
   public void service(HttpServletRequest req,HttpServletResponse res) throws IOException,ServletException{
       int a=Integer.parseInt(req.getParameter("fnum"));
       int b=Integer.parseInt(req.getParameter("snum"));
       
       int c=a*b;
      PrintWriter out=res.getWriter();
      out.print(c);
//       HttpSession session=req.getSession();
//       session.setAttribute("c", c);
       Cookie cookie=new Cookie("c", c+"");
         res.addCookie(cookie);
       res.sendRedirect("sq");
 //       req.setAttribute("c", c);
//       RequestDispatcher rd=req.getRequestDispatcher("sq");
//       rd.forward(req, res);
   }
}
