<%-- 
    Document   : Add
    Created on : 07-Feb-2023, 12:42:48 pm
    Author     : bas200174
--%>

<%@page import="java.time.LocalDate"%> <!-- directive -->
<%@page import="java.util.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body bgcolor="skyblue">
        <h1>Hello World!</h1>
        <%! 
            int a=10;//declaration a
            %>
        <%                                          //scriptlet
            int a=Integer.parseInt(request.getParameter("fnum"));
            int b=Integer.parseInt(request.getParameter("snum"));  
            int c=a+b; 
             out.print("output :"+c);
             %>
            My Number : <%=a //expression
             %>   
                
    </body>
</html>
