package com.mycompany.web.app2;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter(urlPatterns = {"/demoservlet","/TimeServlet"})
public class DemoFilter implements Filter{

   
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        System.out.println("#############");
        System.out.println("welcome");
        chain.doFilter(request, response);
        System.out.println("ThankYou");
        System.out.println("*************");
    }


    
}
