package com.mycompany.web.app2;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@WebListener
public class ApplicationListener implements ServletContextListener{

  

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
      
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
                Connection connection=DriverManager.getConnection("jdbc:mysql://bassure.in:3306/Natrajdb","Natraj" , "Natraj@1501");
            ServletContext context=sce.getServletContext();
            context.setAttribute("connection", connection);
            
        }catch(SQLException|ClassNotFoundException sql){
            sql.printStackTrace();
        }
        
    }
    
      @Override
    public void contextDestroyed(ServletContextEvent sce) {
         ServletContext context=sce.getServletContext();
            context.removeAttribute("connection");
        
    }
}
