package model;

public class Payments {
    
    int paymentID;
    int customerID;
    double paymentAmount;
    double balanceAmount;

    public Payments() {
    }

    public Payments(int paymentID, int customerID, double paymentAmount, double BalanceAmount) {
        this.paymentID = paymentID;
        this.customerID = customerID;
        this.paymentAmount = paymentAmount;
        this.balanceAmount = BalanceAmount;
    }
        public Payments( int customerID, double paymentAmount, double BalanceAmount) {
        this.customerID = customerID;
        this.paymentAmount = paymentAmount;
        this.balanceAmount = BalanceAmount;
    }  
    
    public int getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(double BalanceAmount) {
        this.balanceAmount = BalanceAmount;
    }

    @Override
    public String toString() {
        return "Payments{" + "paymentID=" + paymentID + ", customerID=" + customerID + ", paymentAmount=" + paymentAmount + ", BalanceAmount=" + balanceAmount + '}';
    }
    
    
}
