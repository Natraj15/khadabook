package model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class Orders {
    
   private  int orderId;
   private int customerId;
   private Date date;
   private List<LineItems> lineItems;
   private double totalPrice;

    public Orders() {
    }

    public Orders(int orderId, int customerId, Date date, List<LineItems> lineItems, double totalPrice) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.date = date;
        this.lineItems = lineItems;
        this.totalPrice = totalPrice;
    }

    public List<LineItems> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItems> lineItems) {
        this.lineItems = lineItems;
    }

         
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Orders{" + "orderId=" + orderId + ", customerId=" + customerId + ", date=" + date + ", lineItems=" + lineItems + ", totalPrice=" + totalPrice + '}';
    }


}
