package daopackage;

import model.Customers;
import model.Inventory;
import model.Orders;
import java.sql.Date;

public interface StatisticsDao {
    
     public Orders[] mostPurchasedCustomerbyStartDatetoEndDate(Date date1,Date date2);
     
     public Customers[] paidCustomers();
     
     public Customers[] unPaidCustomers();
     
     public Inventory[] mostSoldProducts();
     
     public Inventory[] lessSoldProducts();
     
     public Orders[] OrdersListByStartDatetoEndDate(Date date1,Date date2);
}
