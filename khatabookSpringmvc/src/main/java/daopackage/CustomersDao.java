package daopackage;

import java.util.List;
import model.Customers;

public interface CustomersDao {
    
    public int addCustomers(Customers customer);
    
     public Customers viewOneCustomer(int id);
     
     public List<Customers> viewAllCustomers();
     
   // public boolean checkCustomerID(int id);
     
   
     
     public int updateCustomerName(int id,String name);
     
     public int updateCustomerAddress(int id,String address);
     
     public int updateCustomerPhoneNumber(int id,long phoneNumber);
}
