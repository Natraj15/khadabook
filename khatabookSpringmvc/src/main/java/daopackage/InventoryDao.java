
package daopackage;

import java.util.List;
import model.Inventory;


public interface InventoryDao {
    
    public int addProducts(Inventory product);
    
    public Inventory viewOneProduct(int productId);
    
    public List<Inventory> viewAllProducts();
    
//    public boolean checkProductID(int productID);
    
    public int updateProducts(Inventory product,int option);
}
