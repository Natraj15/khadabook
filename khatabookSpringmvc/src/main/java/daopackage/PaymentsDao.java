package daopackage;

import model.Payments;

public interface PaymentsDao {
    
    public int makePayment(Payments payment);
}
