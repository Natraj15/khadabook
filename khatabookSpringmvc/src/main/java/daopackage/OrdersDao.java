package daopackage;

import java.util.List;
import model.Customers;
import model.Inventory;
import model.Orders;

public interface OrdersDao {
    
    public int  placeOrder(Orders order, List<Inventory> inventory);
    
    public List<Orders> viewOrdersByCutomerId(int customerID);
    
//    Orders viewOrdersbyOrderID(int orderId);
//    
//    Orders[] viewPendingOrders();
//    
//    Orders[] viewCustomerPendingOrders(int customerID);
//    
//    int confimeOrders(Inventory[] products,Customers customers,int orderID);
}
