package com.mycompany.natraj.khatabookspringmvc.configuration;

import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.HttpMessageConverter;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class AppConfig implements WebMvcConfigurer
{
    @Bean
    public ViewResolver internalResourceViewResolver() {
        final InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass((Class)JstlView.class);
        bean.setPrefix("/WEB-INF/pages/");
        bean.setSuffix(".jsp");
        return (ViewResolver)bean;
    }
    
    
    
    @Override
    public void configureMessageConverters( List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder().indentOutput(true).dateFormat((DateFormat)new SimpleDateFormat("yyyy-MM-dd"));
        converters.add((HttpMessageConverter<?>)new MappingJackson2HttpMessageConverter(builder.build()));
        converters.add((HttpMessageConverter<?>)new MappingJackson2XmlHttpMessageConverter(builder.createXmlMapper(true).build()));
    }
}
