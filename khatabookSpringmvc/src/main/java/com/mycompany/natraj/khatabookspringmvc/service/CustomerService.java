package com.mycompany.natraj.khatabookspringmvc.service;

import com.mycompany.natraj.khatabookspringmvc.Repository.CustomerRepository;
import java.util.List;
import model.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public int addCustomer(Customers customer) {
        int message = customerRepository.addCustomers(customer);
        return message;
    }

    public List<Customers> viewAllCustomers() {
        return customerRepository.viewAllCustomers();
    }

    public Customers viewOneCustomer(int id) {
        return this.customerRepository.viewOneCustomer(id);

    }

    public int updateCustomer(int id, String field, String value) {
        int message = 0;
        switch (field) {
            case "Name": {
                message = this.customerRepository.updateCustomerName(id, value);
                break;
            }
            case "PhoneNumber": {
                final long phoneNumber = Long.parseLong(value);
                message = this.customerRepository.updateCustomerPhoneNumber(id, phoneNumber);
                break;
            }
            case "Address": {
                message = this.customerRepository.updateCustomerAddress(id, value);
                break;
            }
        }
        return message;
    }
}

//this.customerRepository.addCustomers(customer);
//customerRepository.viewAllCustomers();
