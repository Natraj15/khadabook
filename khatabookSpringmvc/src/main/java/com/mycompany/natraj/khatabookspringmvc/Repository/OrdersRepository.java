package com.mycompany.natraj.khatabookspringmvc.Repository;

//import MysqlDao.MysqlConnection;
//import com.mysql.cj.MysqlConnection;
import daopackage.OrdersDao;
import java.util.ArrayList;
import java.util.List;
import model.Inventory;
import model.LineItems;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class OrdersRepository implements OrdersDao{

    @Autowired
    DbConnection dbConnection;
//    @Autowired
//    MysqlConnection sqlconnection;

    @Override
    public int placeOrder(Orders order, List<Inventory> inventory) {

        int message = dbConnection.jdbcTemplate.update("insert into Orders (CustomerID,TotalPrice,Status) values(?,?,?)", order.getCustomerId(), order.getTotalPrice(), "confirmed");
        int orderId = dbConnection.jdbcTemplate.queryForObject("select max(OrderID) from Orders", Integer.class);
        //update lineItems
        List<LineItems> lineItems = order.getLineItems();
        for (LineItems line : lineItems) {
              if(line.getQuantity()>0&&line.getProductId()>0){
            message += dbConnection.jdbcTemplate.update("insert into  LineItems  (OrderID,ProductID,ProductQuantity,OneProductPrice,Price) values(?,?,?,?,?)", orderId, line.getProductId(), line.getQuantity(), line.getOneProductPrice(), line.getPrice());
              }
              }
        //update products
        for (Inventory product : inventory) {
            message += dbConnection.jdbcTemplate.update("update Inventory set ProductQuantity=?,SoldProducts=? where ProductID=?", product.getProductQuantity(), product.getSoldProductsQuantity(), product.getProductID());
        }
        //update Customer BalnceAmount
        double balanceAmout = dbConnection.jdbcTemplate.queryForObject("select  BalanceAmount from Customers where ID=" + order.getCustomerId(), Integer.class);
        balanceAmout += order.getTotalPrice();
        message += dbConnection.jdbcTemplate.update("update Customers set BalanceAmount=? where ID=?", balanceAmout, order.getCustomerId());

        return message;
    }

    @Override
    public List<Orders> viewOrdersByCutomerId(int customerId) {
         
                 
       List<Orders>    orders= this.dbConnection.jdbcTemplate.query("select * from Orders where CustomerID="+customerId, ordersRowMap);
       for(Orders order:orders){
       List<LineItems> lineItems=    this.dbConnection.jdbcTemplate.query("select * from LineItems where OrderID="+order.getOrderId(), lineRowMap);
       order.setLineItems(lineItems);
     
       } 
        return orders;
//        return  this.dbConnection.jdbcTemplate.query(
//             "SELECT o.OrderID, o.CustomerID, o.OrderDate, li.ProductID, li.ProductQuantity ,li.OneProductPrice,li.price,o.TotalPrice FROM Orders o  JOIN LineItems li ON o.OrderID = li.OrderID"
//    , rowMap);
        
       }
       
       private RowMapper<Orders> rowMap = (rs, rowNum) -> {
  
           Orders orders=new Orders();
           orders.setOrderId(rs.getInt(1));
           orders.setCustomerId(rs.getInt(2));
           orders.setDate(rs.getDate(3));
         
      
       LineItems lineItems=new LineItems();
       lineItems.setProductId(rs.getInt(4));
       lineItems.setQuantity(rs.getInt(5));
       lineItems.setOneProductPrice(rs.getDouble(6));
       lineItems.setPrice(rs.getDouble(7));

     List<LineItems> lineitem=new ArrayList<>();
     lineitem.add(lineItems);
         orders.setLineItems(lineitem);
       orders.setTotalPrice(rs.getDouble(8));
        
       return orders;

       };
       
         private RowMapper<Orders> ordersRowMap=(rs,rowNum)->{
             
             return new Orders(rs.getInt(1),rs.getInt(2),rs.getDate(3),null,rs.getDouble(4));
         };
       private RowMapper<LineItems> lineRowMap=(rs,rowNum)->{
           
           return  new  LineItems(rs.getInt(1),rs.getInt(2),rs.getInt(3),rs.getInt(4),rs.getDouble(5),rs.getDouble(6));
       };
}
