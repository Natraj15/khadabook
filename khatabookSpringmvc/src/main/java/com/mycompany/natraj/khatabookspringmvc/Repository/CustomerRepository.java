package com.mycompany.natraj.khatabookspringmvc.Repository;

import com.mycompany.natraj.khatabookspringmvc.rowmappers.CustomerRowMapper;
import daopackage.CustomersDao;
import org.springframework.jdbc.core.RowMapper;
import java.util.List;
import java.util.Objects;
import model.Customers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerRepository implements CustomersDao {

    @Autowired
    DbConnection dbConnection;

    @Override
    public int addCustomers(Customers customer) {
        final int message = this.dbConnection.jdbcTemplate.update("insert into Customers(Name,PhoneNumber,Address) values(?,?,?) ", customer.getName(), customer.getPhoneNumber(), customer.getAddress());
        return message;
    }

    @Override
    public List<Customers> viewAllCustomers() {
        return (List<Customers>) this.dbConnection.jdbcTemplate.query("select * from Customers", customerRow);
    }

    @Override
    public Customers viewOneCustomer(int id) {
        try {
            return (Customers) this.dbConnection.jdbcTemplate.queryForObject("select * from Customers where ID=" + id,customerRow);
        } catch (EmptyResultDataAccessException ex) {
               return null;
          //  throw new IDNotFoundException("ID not found:" + id);
        }

    }

    @Override
    public int updateCustomerName(int id, String value) {
       
        int message = this.dbConnection.jdbcTemplate.update("update Customers set Name=? where ID=?", value, id);
        return message;
       
    }

    @Override
    public int updateCustomerPhoneNumber(int id, long value) {
        
         int message = this.dbConnection.jdbcTemplate.update("update Customers set PhoneNumber=? where ID=?", value, id);
        return message;
      

    }

    @Override
    public int updateCustomerAddress(int id, String value) {
        
        int message = this.dbConnection.jdbcTemplate.update("update Customers set Address=? where ID=?", value, id);
        return message;
      
  
    }
    
      

//    @Override
//    public boolean checkCustomerID(int id) {
//
//        Customers customer = this.dbConnection.jdbcTemplate.queryForObject("select * from Customers where ID=" + id, new CustomerRowMapper());
//        if (Objects.nonNull(customer)) {
//            return true;
//        } else {
//            return false;
//        }
//    }
    
        private RowMapper<Customers>   customerRow=(rs,numRow)->{
            
             return new Customers(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getDouble(5));
        };
              
}
