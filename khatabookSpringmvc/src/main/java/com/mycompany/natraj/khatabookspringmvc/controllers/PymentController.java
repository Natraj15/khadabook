package com.mycompany.natraj.khatabookspringmvc.controllers;

import com.mycompany.natraj.khatabookspringmvc.service.CustomerService;
import com.mycompany.natraj.khatabookspringmvc.service.PaymentService;
import java.util.Objects;
import model.Customers;
import model.Payments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PymentController {

    @Autowired
    CustomerService customerService;
    @Autowired
    PaymentService paymentService;

    @GetMapping("/PaymentForm")
    public String getMaymentForm(@ModelAttribute("payment") Payments payment) {
        return "paymentForm";
    }

    @PostMapping("/addPayment")
    public String makePayment(Model model, @RequestParam(name = "submit", required = false) String value, @ModelAttribute("payment") Payments payment) {

        int customerId = payment.getCustomerID();
        int message = 0;
        double balanceAmount = 0;
        if (value.equals("Pay") && payment.getPaymentAmount() > 0 && payment.getCustomerID() > 0) {

            message = this.paymentService.makePayments(payment);

            if (message == 2) {
                model.addAttribute("message", "Paid Sucessfully");
                return "message1";
            } else if (message == 100) {
                model.addAttribute("message", "your BalanceAmount Amount Only " + paymentService.getCustomer(customerId).getBalanceAmount());
                return "message2";
            } else if (message == 111) {
                model.addAttribute("message", "sorry! Customer ID " + "'" + payment.getCustomerID() + "'" + " not found");
                return "message2";
            } else {
                model.addAttribute("message", "sorry! Payment Failed");
                return "message2";
            }

        } else if (value.equals("getBalance") && payment.getCustomerID() > 0) {

            Customers customer = paymentService.getCustomer(customerId);
            if (Objects.nonNull(customer)) {
                balanceAmount = customer.getBalanceAmount();
                model.addAttribute("balanceAmount", balanceAmount);
                return "paymentForm";

            } else {
                model.addAttribute("message", "sorry! Customer ID " + "'" + payment.getCustomerID() + "'" + " not found");
                return "message2";
            }
        } else {
            model.addAttribute("message", "Missing Payment Amount or Customer ID");
            return "message2";
        }

    }

}
