package com.mycompany.natraj.khatabookspringmvc.Repository;

import daopackage.PaymentsDao;
import model.Payments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class PaymentRepository implements PaymentsDao{
    
        @Autowired
        DbConnection dbConnection;
        
        @Override
        public int makePayment(Payments payment){
            
              int message=dbConnection.jdbcTemplate.update("insert into Payments (CustomerID,PaymentAmount,BalanceAmount) values (?,?,?)",payment.getCustomerID(),payment.getPaymentAmount(),payment.getBalanceAmount());
              
              message+=dbConnection.jdbcTemplate.update("update Customers set BalanceAmount=? where ID=?", payment.getBalanceAmount(),payment.getCustomerID());
            return message;
        }
}
