package com.mycompany.natraj.khatabookspringmvc.service;

import com.mycompany.natraj.khatabookspringmvc.Repository.StatisticsRepository;
import java.sql.Date;
import java.util.List;
import model.Customers;
import model.Inventory;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {
    
        @Autowired
        StatisticsRepository statisticsRepository;
    
        public List<Orders> getMostPurchasedCustomer(Date date1,Date date2){
            
         return    statisticsRepository.getMostPurchasedCustomers(date1, date2);
        }
        
         public List<Customers> getPaidCustomers(){
                 
                  return  this.statisticsRepository.getPaidCustomers();
         }
         
         public List<Customers> getUnPaidCustomers(){
             
                return this.statisticsRepository.getUnPaidCustomers();
         }
         
         public List<Inventory> getMostSoldProducts(){
             return this.statisticsRepository.getMostSoldProducts();
         }
         
         public List<Inventory> getLessSoldProducts(){
             return this.statisticsRepository.getLessSoldProducts();
         }
         
         public List<Orders> getOrdersByDate(Date date1,Date date2){
             return this.statisticsRepository.getOrdersByDate(date1, date2);
         }
}
