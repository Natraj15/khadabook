package com.mycompany.natraj.khatabookspringmvc.controllers;

import com.mycompany.natraj.khatabookspringmvc.service.InventoryService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class InventoryContoller {

    @Autowired
    InventoryService inventoryService;
    int message;

    @GetMapping("/addProductsForm")
    public String addProductsForm(@ModelAttribute("product")Inventory inventory) {
        return "addProducts";
    }

    @PostMapping("/addProducts")
    public String addProducts(Model model,@ModelAttribute("product")Inventory inventory
            
    ) {

        //Inventory inventory = new Inventory(productName, productQuantity, oneProductPrice, totalProductPrice);
        message = inventoryService.addProducts(inventory);
        if (message > 0) {
            model.addAttribute("message", "product added sucessfully");
            return "message1";
        } else {
            model.addAttribute("message", "sorry! failed");
            return "message2";
        }
    }

    @GetMapping("/viewProducts")
    public String viewProducts() {
        return "viewProducts";
    }

    @PostMapping("/viewAllProducts")
    public String viewAllProducts(ModelMap data) {

        data.addAttribute("products", this.inventoryService.viewAllProducts());
        return "viewAllProducts";
    }

    @PostMapping("/viewOneProduct")
    public String viewOneProduct(Model model,ModelMap data, @RequestParam(name = "productID", required = false) Integer id) {
       
        Inventory inventory= this.inventoryService.viewOneProducts(id);
      if(Objects.nonNull(inventory)){
          data.addAttribute("product",inventory);
        return "viewOneProduct";
      }else{
             model.addAttribute("message","sorry! Product ID not found "+"'"+id+"'");
             return "message2";
      }
    }

    @GetMapping("/updateProductFrom")
    public String updateInventoryForm() {
        return "updateProducts";
    }

    @PostMapping("/updateProducts")
    public String updateProducts(Model model,
            @RequestParam(name = "productID", required = false) Integer id,
            @RequestParam(name = "choose", required = false) String field,
            @RequestParam(name = "value", required = false) String value) {
        int message1 = this.inventoryService.updateProducts(id, field, value);
        if (message1 > 0) {
            model.addAttribute("message", "updated sucessfully");
            return "message1";
        } else {
            model.addAttribute("message", "sorry! not updated");
            return "message2";
        }

    }
}
