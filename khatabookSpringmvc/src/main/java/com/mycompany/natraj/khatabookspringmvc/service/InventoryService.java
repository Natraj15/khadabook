package com.mycompany.natraj.khatabookspringmvc.service;

import com.mycompany.natraj.khatabookspringmvc.Repository.InventoryRepository;
import java.util.List;
import model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    public int addProducts(Inventory inventory) {

        int message = inventoryRepository.addProducts(inventory);
        return message;
    }

    public List<Inventory> viewAllProducts() {

        return inventoryRepository.viewAllProducts();
    }

    public Inventory viewOneProducts(int id) {

        return inventoryRepository.viewOneProduct(id);
    }

    public int updateProducts(int id, String field, String value) {

        int message = 0;
        Inventory products = new Inventory();
        products.setProductID(id);
        switch (field) {
            case "Name":
                products.setProductName(value);
                int option = 1;
                message = inventoryRepository.updateProducts(products, option);

                break;

            case "Quantity":
                int quantity = Integer.parseInt(value);
                products.setProductQuantity(quantity);
                option = 2;
                message = inventoryRepository.updateProducts(products, option);
                break;
            case "OneProductPrice":
                double onePrice = Double.parseDouble(value);
                products.setOneProductprice(onePrice);
                option = 3;
                message = inventoryRepository.updateProducts(products, option);

                break;
            case "TotalProductPrice":
                option = 4;
                double totalPrice = Double.parseDouble(value);
                products.setTotalProductprice(totalPrice);
                message = inventoryRepository.updateProducts(products, option);

                break;

        }

        return message;
    }
}
