package com.mycompany.natraj.khatabookspringmvc.Repository;

import com.mycompany.natraj.khatabookspringmvc.rowmappers.CustomerRowMapper;
import com.mycompany.natraj.khatabookspringmvc.rowmappers.InventoryRowMapper;
import java.sql.Date;
import java.util.List;
import model.Customers;
import model.Inventory;
import model.LineItems;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class StatisticsRepository {

    @Autowired
    DbConnection dbConnection;

    public List<Orders> getMostPurchasedCustomers(Date date1, Date date2) {

        return this.dbConnection.jdbcTemplate.query("select CustomerID,sum(TotalPrice) from Orders where OrderDate between" + "'" + date1 + "'" + "and" + "'" + date2 + "'" + "group by CustomerID order by sum(TotalPrice) desc", orderRow);
    }

    public List<Customers> getPaidCustomers() {

        return this.dbConnection.jdbcTemplate.query("select * from Customers where BalanceAmount =0.00", new CustomerRowMapper());
    }

    public List<Customers> getUnPaidCustomers() {

        return this.dbConnection.jdbcTemplate.query("select * from Customers where BalanceAmount !=0.00 order by BalanceAmount desc", new CustomerRowMapper());
    }

    public List<Inventory> getMostSoldProducts() {

        return this.dbConnection.jdbcTemplate.query("select * from Inventory order by SoldProducts desc", new InventoryRowMapper());
    }

    public List<Inventory> getLessSoldProducts() {
        return this.dbConnection.jdbcTemplate.query("select * from Inventory order by SoldProducts asc", new InventoryRowMapper());
    }

    public List<Orders> getOrdersByDate(Date date1, Date date2) {

        List<Orders> orders = this.dbConnection.jdbcTemplate.query("select * from Orders where OrderDate between" + "'" + date1 + "'" + "and" + "'" + date2 + "'", ordersRowMap);
        for (Orders order : orders) {
            List<LineItems> lineItems = this.dbConnection.jdbcTemplate.query("select * from LineItems where OrderID=" + order.getOrderId(), lineRowMap);
            order.setLineItems(lineItems);

        }
        return orders;
    }

    private RowMapper<Orders> orderRow = (rs, rowNum) -> {

        return new Orders(0, rs.getInt(1), null, null, rs.getDouble(2));
    };

    private RowMapper<Orders> ordersRowMap = (rs, rowNum) -> {

        return new Orders(rs.getInt(1), rs.getInt(2), rs.getDate(3), null, rs.getDouble(4));
    };
    private RowMapper<LineItems> lineRowMap = (rs, rowNum) -> {

        return new LineItems(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getDouble(5), rs.getDouble(6));
    };

}
