package com.mycompany.natraj.khatabookspringmvc.controllers;

import com.mycompany.natraj.khatabookspringmvc.service.CustomerService;
import java.util.Objects;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import model.Customers;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/HomePage")
    public String homePage() {

        return "HomePage";
    }
    
    @GetMapping("/addCustomerForm")
    public String getAddCustomer(@ModelAttribute("customer") Customers customer) {
        return "addCustomer";
    }

    @PostMapping("/Addcustomer1")
    public String addCustomer(Model model, @ModelAttribute("customer") Customers customer) {
        //final Customers customers = new Customers(name, phoneNumber, address);
        int message = this.customerService.addCustomer(customer);
        if (message > 0) {
            model.addAttribute("message", "Customer Added Sucessfully");
            return "message1";
        } else {
            model.addAttribute("message", "Sorry! failed");
            return "message2";
        }
    }

   

    @GetMapping("/viewCustomer")
    public String viewCustomer() {
        return "viewCustomer";
    }

    @PostMapping("/viewAllCustomers")
    public String viewAllCustomers(ModelMap data) {
        data.addAttribute("customers", this.customerService.viewAllCustomers());
        return "viewAllCustomers";
    }

    @PostMapping("/viewOneCustomer")
    public String viewOneCustomer(Model model,ModelMap data, @RequestParam(name = "customerID", required = false) Integer id) {
        Customers customer=this.customerService.viewOneCustomer(id);
        if(Objects.nonNull(customer)){
        data.addAttribute("customer", customer);
        return "viewOneCustomer";
        }else{
            model.addAttribute("message", "sorry! Customer ID Not Found "+"'"+id+"'");
            return "message2";
        }

    }

    @GetMapping("/updateCustomerForm")
    public String updateCustomer() {
        return "updateCustomer";
    }

    @PostMapping("/updateCustomer")

    public String updateCustomer(Model model, @RequestParam(name = "customerID", required = false) Integer id,
            @RequestParam(name = "choose", required = false) String field,
            @RequestParam(name = "value", required = false) String value) {
        int message1 = this.customerService.updateCustomer(id, field, value);
        if (message1 > 0) {
            model.addAttribute("message", "updated sucessfully");
            return "message1";
        } else {
            model.addAttribute("message", "sorry! id not found");
            return "message2";
        }
    }
}
