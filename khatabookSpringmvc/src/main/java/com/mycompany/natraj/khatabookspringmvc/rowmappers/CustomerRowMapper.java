package com.mycompany.natraj.khatabookspringmvc.rowmappers;

import java.sql.SQLException;
import java.sql.ResultSet;
import model.Customers;

import org.springframework.jdbc.core.RowMapper;

public class CustomerRowMapper implements RowMapper<Customers> 
{
    @Override
    public Customers mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Customers(rs.getInt(1), rs.getString(2), rs.getLong(3), rs.getString(4), rs.getDouble(5));
    }
    
    
}
