package com.mycompany.natraj.khatabookspringmvc.controllers;

import com.mycompany.natraj.khatabookspringmvc.service.OrdersService;
import java.util.List;
import model.Inventory;
import model.LineItems;
import org.springframework.ui.Model;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrdersController {

    @Autowired
    OrdersService ordersService;

    @GetMapping("/OrdrsForm")
    public String getOrdersFrom(Model model, @ModelAttribute("orders") Orders order, ModelMap data) {
//        List<Inventory> products = this.ordersService.getAllProducts();
//        if (!products.isEmpty()) {
//            data.addAttribute("products", products);
//            return "ordersTable";
//        } else {
//            model.addAttribute("message", "no products");
//            return "message2";
//        }
      model.addAttribute("check", "true");
        return "ordersTable";
    }

    @PostMapping("/placeOrder")
  
    public String placeOrders(ModelMap data, @RequestParam(name = "submit", required = false) String value, Model model, @ModelAttribute("orders") Orders order) {

        String message;

        if (value.equals("confirmOrder")) {

            message = this.ordersService.placeOrder(order);

            if (message.equals("sucess")) {
                model.addAttribute("message", "Order Placed Sucessfully");
                return "message1";

            } else {
                model.addAttribute("message", message);
                return "message2";
            }

        } else if (ordersService.cheackCustomerId(order.getCustomerId()) && order.getCustomerId() > 0) {
            List<Inventory> products = this.ordersService.getAllProducts();
            if (!products.isEmpty()) {
                data.addAttribute("products", products);
                model.addAttribute("check", "false");
                return "ordersTable";
            } else {
                model.addAttribute("message", "no products");
                return "message2";
            }

        } else {
            model.addAttribute("message", "customer ID not found");
            return "message2";
        }

    }

    @GetMapping("/orderViewForm")
    public String getOrderForm() {
        return "orderViewForm";
    }

    @PostMapping("/orderView")
    public String getOrdersView(ModelMap data, Model model,
            @RequestParam(name = "customerId", required = false) Integer customerId
    ) {
        //    int customerID=1;
        List<Orders> orders = ordersService.viewOrdersByCutomerId(customerId);
        if (!orders.isEmpty()) {
            model.addAttribute("order", orders);
            data.addAttribute("orders", orders);
            return "viewOrders";

        } else {
            model.addAttribute("message", "No Ordes for Customer ID:" + customerId);
            return "message2";
        }
    }

}

//order.getLineItems().get(order.getLineItems().size()-1).getProductId()
