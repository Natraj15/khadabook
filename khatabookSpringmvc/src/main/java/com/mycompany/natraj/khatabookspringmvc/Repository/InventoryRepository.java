package com.mycompany.natraj.khatabookspringmvc.Repository;

import com.mycompany.natraj.khatabookspringmvc.rowmappers.InventoryRowMapper;
import daopackage.InventoryDao;
import java.util.List;
import model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryRepository implements InventoryDao {

    @Autowired
    DbConnection dbConnection;

    @Override
    public int addProducts(Inventory inventory) {

        int message = dbConnection.jdbcTemplate.update("insert into Inventory (ProductName,ProductQuantity,OneProductprice,TotalProductprice) values (?,?,?,?)",
                inventory.getProductName(), inventory.getProductQuantity(), inventory.getOneProductprice(), inventory.getTotalProductprice());
        return message;
    }

    @Override
    public List<Inventory> viewAllProducts() {

        return dbConnection.jdbcTemplate.query("select * from Inventory", productRow);
    }

    @Override
    public Inventory viewOneProduct(int id) {
        try {
            return this.dbConnection.jdbcTemplate.queryForObject("select * from Inventory where ProductID=" + id, productRow);
        } catch (EmptyResultDataAccessException ex) {
            return null;
            //  throw new IDNotFoundException("sorry! id not found");
        }
    }

    @Override
    public int updateProducts(Inventory inventory, int option) {
        int message = 0;
        switch (option) {
            case 1:
                message = this.dbConnection.jdbcTemplate.update("update Inventory set ProductName=? where ProductID=?", inventory.getProductName(), inventory.getProductID());
                break;
            case 2:
                message = this.dbConnection.jdbcTemplate.update("update Inventory set ProductQuantity=? where ProductID=?", inventory.getProductQuantity(), inventory.getProductID());
                break;
            case 3:
                message = this.dbConnection.jdbcTemplate.update("update Inventory set OneProductprice=? where ProductID=?", inventory.getOneProductprice(), inventory.getProductID());
                break;
            case 4:
                message = this.dbConnection.jdbcTemplate.update("update Inventory set TotalProductprice=? where ProductID=?", inventory.getTotalProductprice(), inventory.getProductID());
                break;
        }
        return message;
    }

    private RowMapper<Inventory> productRow = (rs, numRow) -> {
        return new Inventory(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDouble(5), rs.getInt(6));
    };
}
