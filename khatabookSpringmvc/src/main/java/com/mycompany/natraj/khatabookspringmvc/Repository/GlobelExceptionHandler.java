package com.mycompany.natraj.khatabookspringmvc.Repository;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobelExceptionHandler {

    @ExceptionHandler(IDNotFoundException.class)
    public ResponseEntity<String> handleIDNotFoundException(IDNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
    }

}
