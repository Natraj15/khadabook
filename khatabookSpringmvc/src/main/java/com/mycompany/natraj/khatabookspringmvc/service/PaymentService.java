package com.mycompany.natraj.khatabookspringmvc.service;

import com.mycompany.natraj.khatabookspringmvc.Repository.PaymentRepository;
import java.util.Objects;
import model.Customers;
import model.Payments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    CustomerService customerService;
    double balanceAmount = 0;

    public Customers getCustomer(int customerId) {

        Customers customer = customerService.viewOneCustomer(customerId);
        if (Objects.nonNull(customer)) {
            balanceAmount = customer.getBalanceAmount();
            return customer;
        } else {
            return null;
        }
    }

    public int makePayments(Payments payment) {

        //    double balanceAmount = payment.getBalanceAmount();
           Customers customer=     this.getCustomer(payment.getCustomerID());
           if(Objects.nonNull(customer)){
                balanceAmount=customer.getBalanceAmount();
        double paymentAmount = payment.getPaymentAmount();
        if (paymentAmount <= balanceAmount) {
            double newBalanceAmount = balanceAmount - paymentAmount;

            payment.setBalanceAmount(newBalanceAmount);
            int message = paymentRepository.makePayment(payment);
            return message;
        } else {
            return 100;
        }
           }else{
               return  111;
           } 

    }
}
