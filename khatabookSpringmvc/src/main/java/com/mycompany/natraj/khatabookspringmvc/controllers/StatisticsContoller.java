package com.mycompany.natraj.khatabookspringmvc.controllers;

import com.mycompany.natraj.khatabookspringmvc.service.StatisticsService;
import java.sql.Date;
import java.util.List;
import model.Customers;
import model.Inventory;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StatisticsContoller {

    @Autowired
    StatisticsService statisticsService;

    List<Customers> customers = null;
    List<Inventory> products=null;
    List<Orders> orders=null;
    @GetMapping("/statisticsHomePage")
    public String getStatistics() {
        return "statistics";
    }

    @PostMapping(path = "/mostPurchasedCustomer")
    public String getMostPurchasedCustomer(Model model,ModelMap data, @RequestParam(name = "date1", required = false) Date date1,
            @RequestParam(name = "date2", required = false) Date date2) {
           orders= this.statisticsService.getMostPurchasedCustomer(date1, date2);
           if(!orders.isEmpty()){
        data.addAttribute("customers",orders);
        return "mostPurchasedCustomers";
           }else{
                 model.addAttribute("message", "No customers Found");
                 return "message2";
           }
    }



    @GetMapping("/paidCustomers")
    public String getPaidCustomers(ModelMap data, Model model) {

        customers = this.statisticsService.getPaidCustomers();
        if (!customers.isEmpty()) {
            data.addAttribute("customers", customers);
            return "viewAllCustomers";
        } else {
            model.addAttribute("message", "No Paid Customers");
            return "message2";
        }

    }

    @GetMapping("/unpaidCustomers")
    public String getUnPaidCustomers(ModelMap data, Model model) {
        customers = this.statisticsService.getUnPaidCustomers();
        if (!customers.isEmpty()) {
            data.addAttribute("customers", customers);
            return "viewAllCustomers";
        } else {
            model.addAttribute("message", "No UnPaid Customers");
            return "message2";
        }

    }
        
       @GetMapping("/mostSoldProducts")
        public String getMostSoldProducts(ModelMap data,Model model){
            products=this.statisticsService.getMostSoldProducts();
            if(!products.isEmpty()){
                data.addAttribute("products", products);
                return "viewAllProducts";
            }else{
                model.addAttribute("message", "No Products");
                return "message2";
            }
            
        }
        
        @GetMapping("/lessSoldProducts")
        public String getLessSoldProducts(ModelMap data,Model model){
            products=this.statisticsService.getLessSoldProducts();
            if(!products.isEmpty()){
                     data.addAttribute("products", products);
                     return "viewAllProducts";
            }else{
                model.addAttribute("products", products);
                return "message2";
            }
        }
        
        @PostMapping("/ordersByDate")
        public String getOrdersByDate(Model model,ModelMap data, @RequestParam(name = "date1", required = false) Date date1,
            @RequestParam(name = "date2", required = false) Date date2){
            orders=this.statisticsService.getOrdersByDate(date1, date2);
            
            if(!orders.isEmpty()){
                    data.addAttribute("orders", orders);
                    return "viewOrders";
            }else{
                model.addAttribute("message", "No Orders Found");
                return "message2";
            }
            
            
        }
        
}
