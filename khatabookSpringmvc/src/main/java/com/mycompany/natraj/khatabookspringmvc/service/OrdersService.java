package com.mycompany.natraj.khatabookspringmvc.service;

import com.mycompany.natraj.khatabookspringmvc.Repository.OrdersRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import model.Customers;
import model.Inventory;
import model.LineItems;
import model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdersService {

    @Autowired
    OrdersRepository ordersRepository;
    @Autowired
    InventoryService inventoryService;
    @Autowired
    CustomerService customrService;

    public String placeOrder(Orders order) {

        double totalPrice = 0.0;
        int cheakQuantity = 0;
        int message = 0;
        List<Inventory> inventory = new ArrayList<>();
        try {
            List<LineItems> lineItems = order.getLineItems();
            Customers customer = customrService.viewOneCustomer(order.getCustomerId());
            if (Objects.nonNull(customer)) {
                for (LineItems line : lineItems) {

                    int productId = line.getProductId();
                    cheakQuantity += line.getQuantity();

                    Inventory product = inventoryService.viewOneProducts(productId);
                    if (Objects.nonNull(product)) {

                        int quantity = line.getQuantity();
                        if (quantity <= product.getProductQuantity()) {

                            double price = product.getOneProductprice() * quantity;
                            totalPrice += price;

                            int productQuantity = product.getProductQuantity() - quantity;
                            product.setProductQuantity(productQuantity);
                            int soldProductQuantity = product.getSoldProductsQuantity() + quantity;
                            product.setSoldProductsQuantity(soldProductQuantity);
                            product.setProductID(productId);

                            inventory.add(product);

                            line.setOneProductPrice(product.getOneProductprice());
                            line.setPrice(price);

                            order.setTotalPrice(totalPrice);

                        } else {
                            return "sorry! Not enough products 'Enter less product'";
                        }

                    } else {
                        return "sorry! ProductID not found";
                    }

                }
            } else {
                return "sorry! Customer ID not found '" + order.getCustomerId() + "'";
            }

            if (cheakQuantity > 0) {
                message = ordersRepository.placeOrder(order, inventory);
                if (message >= 4) {
                    return "sucess";
                } else {
                    return "failed";
                }
            } else {
                return "sorry! You have to enter at least one Quantity";
            }
        } catch (NullPointerException ex) {
            return "sorry! First Check Customer ID";
        }
    }

    public List<Inventory> getAllProducts() {

        List<Inventory> products = inventoryService.viewAllProducts();
        return products;
    }

    public List<Orders> viewOrdersByCutomerId(int customerId) {

        return ordersRepository.viewOrdersByCutomerId(customerId);
    }

    public boolean cheackCustomerId(int customerId) {

        Customers customer = this.customrService.viewOneCustomer(customerId);
        if (Objects.nonNull(customer)) {
            return true;
        } else {
            return false;
        }
    }

}
