package com.mycompany.natraj.khatabookspringmvc.rowmappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import model.Inventory;
import org.springframework.jdbc.core.RowMapper;

public class InventoryRowMapper implements RowMapper<Inventory> {
    
    @Override
    public Inventory mapRow(ResultSet rs, int rowNum) throws SQLException {
        
        return new Inventory(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDouble(5), rs.getInt(6));
    }
    
}
