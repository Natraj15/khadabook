package com.mycompany.natraj.khatabookspringmvc.Repository;

public class IDNotFoundException extends RuntimeException {

    public IDNotFoundException(String message) {
        super(message);
    }
    
}
