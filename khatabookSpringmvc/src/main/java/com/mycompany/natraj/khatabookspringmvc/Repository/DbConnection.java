package com.mycompany.natraj.khatabookspringmvc.Repository;

import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Repository
public class DbConnection {
    
    JdbcTemplate jdbcTemplate;

    public DbConnection(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public JdbcTemplate getConnection(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }
    
    
}
