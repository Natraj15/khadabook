<%-- 
    Document   : viewAllProducts
    Created on : 09-Jun-2023, 12:39:23 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script><br>
          <style>
            table {
                border:1px solid black;
            }
            caption{
                text-decoration-line: underline;
                text-decoration-style: double-line;
                text-decoration-color: royalblue;

            }
        </style>

    </head>
    <body>

       
            <table class="table table-striped-columns container caption-top text-center" >

                <caption class="h4 text-center">Products Details</caption>
                <tr>      
                    <th>ProductID </th>
                    <th>ProductName</th>
                    <th>ProductQuantity</th>
                    <th>OneProductPrice</th>
                    <th>TotalProductPrice</th>
                    <th>SoldProducts</th>
                </tr>  

                <c:forEach items="${products}" var="product" varStatus="loop">
                    <tr>

                        <td>${product.productID}</td>   
                        <td>${product.productName}</td>         
                        <td>${product.productQuantity}</td>   
                        <td>${product.oneProductprice}</td>     
                        <td>${product.totalProductprice}</td>     
                        <td>${product.soldProductsQuantity}</td>

                    </tr> 
                </c:forEach>

            </table>
        
    </body>
</html>
