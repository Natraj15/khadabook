<%-- 
    Document   : addCustomer
    Created on : 07-Jun-2023, 5:54:17 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </head>
    <style>
        
    </style>
    <body>
      
<!--         <form action="Addcustomer1" method="post">
          
            <label for="CustomerName">Enter Customer Name</label> <br>
            <input required type="text" Name="CustomerName" id="CustomerName" pattern="[A-Za-z]+" ><br>
            <label for="PhoneNumber">Enter Customer PhoneNumber</label><br>
            <input required type="tel" Name="PhoneNumber" id="PhoneNumber"  pattern="[0-9]{10}" title="Please enter a 10-digit phone number" ><br>
             <label for="Address">Enter Customer Address</label> <br>
            <input required type="text" Name="Address" id="Address" pattern="[A-Za-z]+"><br>
            <input type="submit" value="Add"><br>
        </form>-->
        
       
<div class="container ">
        <h3 class="mt-5 ">Welcome, Enter The Customer Details</h3>
        <form:form method="POST"  action="Addcustomer1" cssClass="m-3 needs-validation" novalidate="true" modelAttribute="customer">
             <table>
                <tr>
                    <td><form:label for="name" path="name" >Enter Name</form:label></td>
                    <td><form:input path="name"  id = "name" cssClass="form-control m-1" type="text"  pattern="[A-Za-z]+" required ="true" /></td>
                </tr>
                <tr>
                    <td><form:label path="phoneNumber">Enter Phone Number</form:label></td>
                    <div>
                        <td><form:input path="phoneNumber" cssClass="form-control m-1" required="true"  type="tel"  pattern="[0-9]{10}" title="Please enter a 10-digit phone number"/></td>
                   <div class="invalid-feedback">
                    Phone Number Should have 10 numbers
                     </div></div>
                </tr>
                <tr>
                    <td><form:label path="address">
                      Enter Address</form:label></td>
                    <td><form:input path="address" required="true" cssClass="form-control m-1" type="text"  pattern="[A-Za-z]+"/></td>
                </tr>
                </table>
               
            <input type="submit" value="Add" class="btn btn-primary" style="margin-left: 370px; margin-top:10px"/>
                
            
                <div>
        </form:form>
        
                             <script>

                                    (() => {
                                    'use strict'

                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    const forms = document.querySelectorAll('.needs-validation')

                                    // Loop over them and prevent submission
                                    Array.from(forms).forEach(form => {
                                    form.addEventListener('submit', event => {
                                    if (!form.checkValidity()) {
                                    event.preventDefault()
                                    event.stopPropagation()
                                    }

                                    form.classList.add('was-validated')
                                    }, false)
                                    })
                                    })()
       </script>    
   
    </body>
</html>
