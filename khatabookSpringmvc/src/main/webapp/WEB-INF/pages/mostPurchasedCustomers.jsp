<%-- 
    Document   : mostPurchasedCustomers
    Created on : 13-Jun-2023, 9:57:16 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td,th {
                border:1px solid black;
                border-style: dashed;
                text-align: center;
            }
            table{
                border-style: groove;
            }
        </style>
    </head>
    <body>

        <table>
            <caption><h3>Most Purchased Customers</h3></caption>
            <tr>
                <th>Customer ID</th>
                <th>Total Price</th>
            </tr>
            <c:forEach items="${customers}" var="customer">
                <tr>
                    <td> ${customer.customerId} </td>
                    <td>${customer.totalPrice}</td>
                </tr>
            </c:forEach>

        </table>
    </body>
</html>
