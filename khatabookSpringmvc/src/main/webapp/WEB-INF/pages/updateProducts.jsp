<%-- 
    Document   : updateProducts
    Created on : 09-Jun-2023, 2:38:27 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

    </head>
    <body>

        <script>

            function changeInputType() {
                let selectedValue = document.getElementById("choose").value;
                let inputField = document.getElementById("value");

                if (selectedValue === "Name") {
                    inputField.type = "text";
                    inputField.pattern = "[A-Za-z]+";
                } else if (selectedValue === "Quantity" || selectedValue === "OneProductPrice" || selectedValue === "TotalProductPrice") {
                    inputField.type = "number";

                }
            }
        </script>

        <!--        <form action="updateProducts" method="post">
                    <label>Enter ProductID<label><br>
                            <input type="number" name="productID"><br>
                            <label>Choose the Field</label><br>
                            <select id="choose" name="choose" onchange="changeInputType()">
                                <option value="Name">Name</option>
                                <option value="Quantity">Quantity</option>
                                <option value="OneProductPrice">One Product Price</option>
                                <option value="TotalProductPrice">Total Product Price</option>
                            </select><br>
                            <label for="id">Enter Value</label><br>    
                            <input id="value" name="value" ><br>
                            <input type="submit" name="submit" value="Update">
                            </form><br>-->
        <div class="container">
        <h4 class="mt-5">Welcome! Update Products</h4>
        <form action="updateProducts"  method="post" class="m-3 needs-validation" novalidate="true" >

            <table   >

                <tr>  <td> <label>Enter ProductID<label></td> </tr>


                                <tr><td><input type="number"  class="form-control has-validation"  name="productID"  aria-describedby="inputGroupPrepend" required ></td> </tr>


                                <tr><td><label>Choose the Field</label></td> 

                                </tr>

                                <tr>
                                    <td>
                                        <select id="choose" name="choose" class="form-select" onchange="changeInputType()">
                                            <option value="Name">Name</option>
                                            <option value="Quantity">Quantity</option>
                                            <option value="OneProductPrice">One Product Price</option>
                                            <option value="TotalProductPrice">Total Product Price</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr> <td><label for="id">Enter Value</label> </td></tr>

                                <tr> <td><input class="form-control" id="value" name="value" required ></td> </tr>

                                </table>

                                <input type="submit" name="submit" value="Update" class="btn btn-primary" style="margin-left: 180px ; margin-top:10px">

                                </form>
        
        </div>

                                <script>

                                    (() => {
                                    'use strict'

                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    const forms = document.querySelectorAll('.needs-validation')

                                    // Loop over them and prevent submission
                                    Array.from(forms).forEach(form => {
                                    form.addEventListener('submit', event => {
                                    if (!form.checkValidity()) {
                                    event.preventDefault()
                                    event.stopPropagation()
                                    }

                                    form.classList.add('was-validated')
                                    }, false)
                                    })
                                    })()
       </script>    

                        </body>
               </html>
