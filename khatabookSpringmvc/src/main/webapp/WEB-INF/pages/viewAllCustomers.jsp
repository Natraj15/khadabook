<%-- 
    Document   : viewAllCustomers
    Created on : 07-Jun-2023, 9:56:53 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        <style>
            table, th, td {
                border:1px solid black;
            }
            caption{
                text-decoration-line: underline;
                text-decoration-style: wavy;
                text-decoration-color: royalblue;

            }
        </style>
    </head>
    <body>

        <br>
         
            <table class="container table table-bordered table-md text-center caption-top ">
                <caption  class="h3 text-center">Customer Details</caption>
                <tr >
                    <th>Customer ID</th>
                    <th>Customer Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                    <th>Balance Amount</th>
                </tr>

                <c:forEach items="${customers}" var="customer" varStatus="loop">
                    <tr>
                        <td>${customer.customerID}</td>
                        <td>${customer.name}</td>
                        <td>${customer.phoneNumber}</td>
                        <td>${customer.address}</td>
                        <td>${customer.balanceAmount}</td>
                    </tr>
                </c:forEach>

            </table>
        
    </body>
</html>
