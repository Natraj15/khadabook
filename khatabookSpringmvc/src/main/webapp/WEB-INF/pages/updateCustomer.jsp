<%-- 
    Document   : updateCustomer
    Created on : 08-Jun-2023, 12:27:41 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

        <script>

            function changeInputType() {
                let selectedValue = document.getElementById("choose").value;
                let inputField = document.getElementById("value");

                if (selectedValue === "Name" || selectedValue === "Address") {
                    inputField.type = "text";
                    inputField.pattern = "[A-Za-z]+";
                } else if (selectedValue === "PhoneNumber") {
                    inputField.type = "tel";
                    inputField.pattern = "[0-9]{10}";
                    inputField.title = "Please enter a 10-digit phone number";

                }

            }
        </script>
    </head>
    <body>
        <div class="container mt-5">
        <h4 class="m-3">Welcome! Update Customer</h4>
        <form action="updateCustomer"  method="post" class="m-3 needs-validation" novalidate="true">

            <table   >

                <tr>  <td><label  for="cusid">Enter Customer ID<label></td> </tr>


                                <tr><td><input type="number"  class="form-control"  name="customerID" id="cusid" required ></td> </tr>


                                <tr><td><label>Choose the Field</label></td> 
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                </tr>

                                <tr>
                                    <td>
                                        <select  class="form-select"id="choose" name="choose" onchange="changeInputType()" required>
                                            <option value="Name">Name</option>
                                            <option value="PhoneNumber">PhoneNumber</option>
                                            <option value="Address">Address</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr> <td><label for="id">Enter Value</label> </td></tr>

                                <tr> <td><input class="form-control" id="value" name="value" required ></td> </tr>

                                </table>

            <input type="submit" name="submit" value="Update" class="btn btn-primary"style="margin-left: 180px ; margin-top:10px">

                                </form>
        </div>
        
                                  <script>

                                    (() => {
                                    'use strict'

                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    const forms = document.querySelectorAll('.needs-validation')

                                    // Loop over them and prevent submission
                                    Array.from(forms).forEach(form => {
                                    form.addEventListener('submit', event => {
                                    if (!form.checkValidity()) {
                                    event.preventDefault()
                                    event.stopPropagation()
                                    }

                                    form.classList.add('was-validated')
                                    }, false)
                                    })
                                    })()
       </script>    

                </body>
     </html>
