<%-- 
    Document   : viewOneProduct
    Created on : 09-Jun-2023, 12:39:37 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        <style>
            table{
                border:1px solid black;
                
            }
             caption{
                text-decoration-line: underline;
                text-decoration-style: dashed;
                text-decoration-color: royalblue;

            }
  
           
        </style>
    </head>
    <body>

        <br>
        <!--  <table class="table container  caption-top text-center" >
              <caption class="text-center h4">Proudct Details</caption>
              <thead class="table-info">
              <tr>      
                  <th>ProductID </th>
                  <th>ProductName</th>
                  <th>ProductQuantity</th>
                  <th>OneProductPrice</th>
                  <th>TotalProductPrice</th>
                  <th>SoldProducts</th>
              </tr>  
           </thead>
            <tbody>
              <tr>

                  <td>${product.productID}</td>   
                  <td>${product.productName}</td>         
                  <td>${product.productQuantity}</td>   
                  <td>${product.oneProductprice}</td> 
                  <td>${product.totalProductprice}</td>     
                  <td>${product.soldProductsQuantity}</td>

              </tr> 
             </tbody>

          </table>-->
        <table class="table  container text-center caption-top " >


            <caption class="text-center h4">Product Details</caption>

            <tr><th class="table-info "> Product ID </th><td>${product.productID}</td></tr>
            <tr><th class="table-info"> Product Name</th><td>${product.productName}</td></tr>
            <tr><th  class="table-info"> Product Quantity</th><td>${product.productQuantity}</td></tr>
            <tr><th  class="table-info"> One Product Price</th><td>${product.oneProductprice}</td></tr>
            <tr><th  class="table-info"> Total Product Price</th><td>${product.totalProductprice}</td></tr>
            <tr><th   class="table-info"> Sold Products</th><td>${product.soldProductsQuantity}</td></tr>   



        </table>

    </body>
</html>
