<%-- 
    Document   : viewOneCustomer
    Created on : 08-Jun-2023, 11:43:00 am
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        <style>
            td,th{
                font-size:20px;
                border-style: groove;
            }
            table{
                border: 1px solid black;
            }
            
             caption{
                text-decoration-line: underline;
                text-decoration-style: dotted;
                text-decoration-color: royalblue;

            }

        </style>
    </head>
    <body>
            <br>
        <table class="table table-striped-columns container text-center caption-top">


            <caption class="text-center  h4">Customer Profile</caption>

             <tr>   <th> CustomerID </th><td>${customer.customerID}</td></tr>
            <tr><th> CustomerName</th><td>${customer.name}</td></tr>
            <tr><th> PhoneNumber</th><td>${customer.phoneNumber}</td></tr>
            <tr><th> Address</th><td>${customer.address}</td></tr>
            <tr> <th> BalanceAmount</th><td>${customer.balanceAmount}</td></tr>   

             

        </table>

    </body>
</html>
