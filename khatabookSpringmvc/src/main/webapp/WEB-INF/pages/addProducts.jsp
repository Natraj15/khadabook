<%-- 
    Document   : addProducts
    Created on : 08-Jun-2023, 3:28:16 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </head>
    <body>

        <!--        <form action="addProducts" method="post">
        
                    <label for="productname">Enter Product Name</label> <br>
                    <input required type="text" Name="productname" id="productname" pattern="[A-Za-z]+"><br>
                    <label for="productQuantity">Enter product Quantity</label><br>
                    <input required type="number" Name="productQuantity" id="productQuantity"><br>
                    <label for="oneproductprice">Enter one product price</label> <br>
                    <input required type="number" Name="oneproductprice" id="oneproductprice"><br>
                    <label for="totalproductprice">Enter total product price</label> <br>
                    <input required type="number" Name="totalproductprice" id="totalproductprice"><br>
                    <input type="submit" value="AddProduct"><br>
        
                </form>-->

        <div class="container">   
            <h3  class="mt-5">Welcome, Enter The Product Details</h3>
            <form:form method="POST"  action="addProducts" modelAttribute="product">
                <table>
                    <tr>
                        <td><form:label for="name" path="productName" >Enter Name</form:label></td>
                        <td><form:input path="productName" cssClass="form-control m-1" id = "name" type="text"  pattern="[A-Za-z/s]+" required ="true" /></td>
                    </tr>
                    <tr>
                        <td><form:label path="productQuantity">Enter Phone Number</form:label></td>
                        <td><form:input path="productQuantity" required="true" cssClass="form-control m-1"  type="number"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="oneProductprice">
                                Enter Address</form:label></td>
                        <td><form:input path="oneProductprice" required="true" cssClass="form-control m-1" type="number" /></td>
                    </tr>
                    <tr>
                        <td><form:label path="totalProductprice">
                                Enter Address</form:label></td>
                        <td><form:input path="oneProductprice" required="true" cssClass="form-control m-1" type="number" /></td>
                    </tr>
                    </table>
                    
                    <input type="submit" value="Add" style="margin-left: 370px; margin-top:10px" class="btn btn-primary" />
                    
                
            </form:form>
        </div>

    </body>
</html>
