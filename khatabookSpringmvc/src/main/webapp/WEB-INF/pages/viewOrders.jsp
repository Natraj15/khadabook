<%-- 
    Document   : viewConfirmedOrders
    Created on : 13-Jun-2023, 10:51:55 am
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="jakarta.tags.core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>


            th,td{
                border-style: dotted;
            }
            .no-border{
                border:none;
            }
            .right{

                text-align: right;
                padding-right:  20px;
            }
            .center{
                text-align: center;
                border-style: dashed;

            }
            .uderline{

                text-decoration-line: underline;
                text-decoration-style: wavy;
                text-decoration-color: royalblue;


            }

        </style>
    </head>
    <body>

        
        
        <c:forEach items="${orders}" var="order" >
        <%
               
                    int count = 1;
        %>  
        

        <table border="1px" style="margin-left:20px;">
            <tr>

                <th colspan="3" class="uderline">Welcome</th>
                <td  colspan="2">
                    Order ID:${order.orderId}<br>
                    Customer ID:${order.customerId}<br>
                    Date:${order.date}<br>
                </td>
            </tr>

            <tr >
                <th style="border-style: groove"> S.No.</th>
                <th style="border-style: groove"> Product ID</td>
                <th style="border-style: groove"> Quantity</th>
                <th style="border-style: groove"> Rate</th>
                <th style="border-style: groove"> Amount</th>
            </tr>
            <c:forEach items="${order.lineItems}" var="lineitems">

            <tr>
                <td style="border-style: groove; text-align: center;"><%=count++%></td>
                <td style="border-style: groove; text-align: center;">${lineitems.productId}</td>
                <td style="border-style: groove; text-align: center;">${lineitems.quantity}</td>
                <td style="border-style: groove;text-align: center;">${lineitems.oneProductPrice}</td>
                <td style="border-style: groove; text-align: center;">${lineitems.price}</td>

            </tr>
             </c:forEach>
           
            <tr><td class="right"  colspan="5" style="border-style: hidden"> Total Price:${order.totalPrice}</td></tr>
            <tr>
                <th colspan="5"  class="center" >#Thank You#</th>

            </tr>

        </table><br>
            </c:forEach>
               
    </body>
</html>
