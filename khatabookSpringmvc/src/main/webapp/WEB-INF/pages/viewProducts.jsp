<%-- 
    Document   : viewProducts
    Created on : 09-Jun-2023, 12:37:46 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </head>
    <style>
        .body{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        
        .vl {
                              border-left: 3px solid rgb(45, 51, 234);
                              transform: rotate(45deg);
                              height: 400px;
                             
                    }
        
    </style>
    <body>
       
        
             <h3>1.View All Product</h3>
        <form action="viewAllProducts" method="POST">
               
                <input type="submit" value="view" class="btn btn-primary">
            </form>
           
        <h3>2.View One Product</h3>
            <form action="viewOneProduct" method="POST">
                <label id="id">Enter product ID</label><br>
                <input required type="text" name="productID" class="form-control" style="width: 200px" ><br>
                <input type="submit" value="view" class="btn btn-sm btn-primary">
            </form>

<!--    <div class="vl  border-primary"></div>
          <div class="viewAll position-absolute " style="margin-bottom: 100px !important;margin-right: 500px;">
          <h3>1.View All Product</h3>
          <form action="viewAllProducts" method="POST">
                 
                  <input type="submit" value="view" class="btn btn-primary" style="margin-left:80px">
              </form>
              </div>
              <div style="margin-top: 150px ;margin-left: 50px; margin-bottom: 10px">
                  <h3  >2.View One Product</h3>
              <form action="viewOneProduct" method="POST">
                  <label id="id" style="margin-left: 90px;">Enter product ID</label><br>
                  <input required type="text" name="productID" class="form-control" style="width: 200px; margin-left:50px" ><br>
                  <input type="submit" value="view" class="btn  btn-primary"style="margin-left:120px;margin-bottom: 10px">
              </form>
              </div>-->
    </body>
</html>
