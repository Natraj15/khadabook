<%-- 
    Document   : orders
    Created on : 09-Jun-2023, 4:05:22 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="jakarta.tags.core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </head>
    <body>



        <h3>Welcome, Place Your Orders</h3>


        <form:form method="POST" action="placeOrder" modelAttribute="orders">
            <table>
                <tr>
                    <td><label>Customer ID:</label></td>
                    <td><form:input path="customerId"  id="customerId" type="number" placeholder="" required="true" /></td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        <div id="lineItemsContainer">
                            <div class="lineItem">
                                <label>Product ID:</label>
                                <form:input type="number" path="lineItems[0].productId" placeholder="" required="true" />
                                <label>Quantity:</label>
                                <form:input type="number" path="lineItems[0].quantity" placeholder="" required="true" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <button type="button" class="btn btn-primary" onclick="addLineItem()">Add Line Item</button>
            <button type="submit"class="btn btn-primary">Submit</button>
        </form:form><br>


        <table class="table table-sm table-hover " style="width: 500px" >

            <tr class="table-info">      
                <th>ProductID </th>
                <th>ProductName</th>
                <th>ProductQuantity</th>
                <th>OneProductPrice</th>
                <th>TotalProductPrice</th>
                <th>SoldProducts</th>
            </tr>  

            <c:forEach items="${products}" var="product" varStatus="loop">
                <tr>

                    <td>${product.productID}</td>   
                    <td>${product.productName}</td>         
                    <td>${product.productQuantity}</td>   
                    <td>${product.oneProductprice}</td>     
                    <td>${product.totalProductprice}</td>     
                    <td>${product.soldProductsQuantity}</td>

                </tr> 
            </c:forEach> 

        </table>



        <script>
            var lineItemCount = 1;

            function addLineItem() {
                let lineItemDiv = document.createElement("div");
                lineItemDiv.className = "lineItem";

                let productIdLabel = document.createElement("label");
                productIdLabel.textContent = "Product ID: ";
                let productIdInput = document.createElement("input");
                productIdInput.type = "number";
                productIdInput.name = "lineItems[" + lineItemCount + "].productId";
                productIdInput.required = true;

                let quantityLabel = document.createElement("label");
                quantityLabel.textContent = " Quantity: ";
                let quantityInput = document.createElement("input");
                quantityInput.type = "number";
                quantityInput.name = "lineItems[" + lineItemCount + "].quantity";
                quantityInput.required = true;

                lineItemDiv.appendChild(productIdLabel);
                lineItemDiv.appendChild(productIdInput);
                lineItemDiv.appendChild(quantityLabel);
                lineItemDiv.appendChild(quantityInput);

                var lineItemsContainer = document.getElementById("lineItemsContainer");
                lineItemsContainer.appendChild(lineItemDiv);

                lineItemCount++;
            }
        </script>



    </body>
</html>
