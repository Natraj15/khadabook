<%-- 
    Document   : ordersTable
    Created on : 14-Jun-2023, 5:44:43 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="jakarta.tags.core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/d4c91e826a.js" crossorigin="anonymous"></script>


        <style>
            table{
                border:1px solid black;
            }
            
            th{
                
                padding:10px !important;
               /*border:1px solid;*/
            }
            td{
               /* border:1px solid black;*/
            }
        </style>
        <script>


        </script>

    </head>


    <h5 class="d-flex justify-content-center">Welcome, Place Your Orders</h5>


    <form:form method="POST"  action="placeOrder" modelAttribute="orders" >

        <div class=" container d-flex  p-2 justify-content-center">
            <label >Customer ID:</label>
            <form:input path="customerId"  cssStyle="width:150px" id="customerId" type="number" placeholder="" required="true"/>
            <input  class="btn btn-primary btn-sm"  onclick="changedisable()"   id="check"  type="submit" name="submit" value="checkCustomer"/>

            <form:button  class="btn btn-primary btn-sm " id="view" type="button" disabled="${check}"  data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">viewProducts</form:button>

            </div>



            <div class="collapse collapse-horizontal" id="collapseWidthExample">

                <input  class="btn btn-primary d-flex justify-content-center btn-sm " type="submit"  name="submit" value="confirmOrder" style="margin-left:120px"/>
                <table class="table table-sm  container text-center " >
                    <thead   class="table-info text-center border border-2">
                        <tr>      
                            <th>ProductID</th>
                            <th>ProductName</th>
                            <th>ProductQuantity</th>
                            <th>OneProductPrice</th>
                            <th>EnterQuantity</th>
                        </tr>  
                    </thead>
                    <tbody >

                    <c:forEach items="${products}" var="product" varStatus="status">
                        <tr>

                            <td>${product.productID}<form:input type="hidden" path="lineItems[${status.index}].productId" value="${product.productID}"/></td>   
                            <td>${product.productName}</td>         
                            <td>${product.productQuantity}</td>   
                            <td>${product.oneProductprice}</td>    

                            <!--                    <td><button type="button"   id="idbutton"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart4" viewBox="0 0 16 16">
                                                        <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
                                                    </svg><form:input type="hidden" path="lineItems[${status.index}].productId" value="${product.productID}"/></td> </button>    -->

                            <td><form:input name = "quantity" id = "quantity" class="quantity" required="true" cssClass="form-control form-control-sm"  cssStyle="width:150px" type="number"  path="lineItems[${status.index}].quantity" /></td>

                        </tr> 
                    </c:forEach> 
                </tbody>
            </table>

        </form:form>

        <div>
     
           
       </body>
  </html>
