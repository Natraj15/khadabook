<%-- 
    Document   : HomePage
    Created on : 08-Jun-2023, 1:21:25 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <h2>customers</h2>

        <ul>
            <li> <a href="addCustomerForm">Add Customers</a></li>
            <li> <a href="viewCustomer">View Customers</a></li>
            <li> <a href="updateCustomerForm">Update Customers</a></li>


        </ul>

        <h2>inventory</h2>
        <ul>
            <li> <a href="addProductsForm">Add Products</a></li>
            <li> <a href="viewProducts">View Products</a></li>
            <li> <a href="updateProductFrom">Update Products</a></li>
        </ul>

        <h2>Orders</h2>
        <ul>
            <li> <a href="OrdrsForm">place Orders</a></li>
            <li> <a href="orderViewForm">View Orders</a></li>
            <li> <a href="PaymentForm">Make Payment</a></li>
        </ul>
        
         <h2>Statistics</h2>
        <ul>
            <li> <a href="statisticsHomePage">Most Purchased Customer</a></li>
            <li> <a href="paidCustomers">View Paid Customers</a></li>
            <li> <a href="unpaidCustomers">View UnPaid Customers</a></li>
            <li> <a href="mostSoldProducts">Most Sold Products First</a></li>
            <li> <a href="lessSoldProducts">Less Sold Products First</a></li>
             <li> <a href="statisticsHomePage">View Orders By Date</a></li>
        </ul>

    </body>
</html>
