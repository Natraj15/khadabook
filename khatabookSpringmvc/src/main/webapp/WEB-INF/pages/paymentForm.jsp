<%-- 
    Document   : PaymentForm
    Created on : 12-Jun-2023, 2:12:59 pm
    Author     : bas200174
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

           <script>

                function disableButton() {
//                    setTimeout(function(){
//                        document.getElementByIddisableButton(("view").readonly="true";
//                    },1000);
//                   let  button=  document.getElementByIddisableButton(("getbalance");
//                     button.disabled=true;
                  let  button1=  document.getElementByIddisableButton(("getbalance");
              button1.disabled=true;
                
    }
            
        </script>
    </head>
    <body>

        <h3>Welcome,Make Payments</h3>
        <table>
            <form:form action="addPayment" modelAttribute="payment" method="POST">
                <tr>
                    <td><label>Enter CustomerID:</label></td>
                    <td><form:input  path="customerID" type="number" required ="true" class="form-control" /></td>
                </tr>
                <tr>
                    <td> <p>
                            <input  class="btn"   type="submit" name="submit" value="getBalance" id="getbalance"  />
                            <button class="btn btn-primary" id="view" type="button" onclick=" disableButton()"  data-bs-toggle="collapse" data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                                view
                            </button>   

                        </p></td>
                    <td>   <div class="collapse collapse-horizontal" id="collapseWidthExample">

                            <input type="number"  class="form-control" readonly="true"  value="${balanceAmount}" />


                        </div>
                    </td>

                </tr>

                <tr>
                    <td>  <label>Enter payment Amount</label></td>
                    <td> <form:input path="paymentAmount" type="number"  class="form-control" /></td>
                </tr>

                <tr>
                    <td><input type="submit" value="Pay" name="submit"/></td>
                </tr>
            </form:form>
        </table>
       
     
      
  </body>
</html>
