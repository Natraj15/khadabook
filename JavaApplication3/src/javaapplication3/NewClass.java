
package javaapplication3;

public class NewClass {
    int a;
    String name;
    NewClass(){
        System.out.println("0 argument constructor");
    }
    NewClass(String name, int a){
        this.a=a;
        this.name=name;
    }
    void display(){
        System.out.println("Your Name is "+name+" your id is "+a);
    }
    
}
class new1{
    public static void main(String a[]) {
        NewClass n=new NewClass();
        NewClass k=new NewClass("raj",1);
        NewClass s=new NewClass("nj",2);
        
          System.out.println(k.a);
          System.out.println(k.name);
        
         k.display();
         s.display();
        
    }
}