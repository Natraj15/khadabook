
public class DiscountRate {
    private static double serviceDiscountPremium=0.2;
    private static double serviceDiscountGold=0.15;
    private static double serviceDiscountSilver=0.1;
    private static double productDiscountPremium=0.1;
    private static double productDiscountGold=0.1;
    private static double productDiscountSilver=0.1;



   public static double getServiceDiscountPremium() {
        return serviceDiscountPremium;
    }



   public static void setServiceDiscountPremium(double serviceDiscountPremium) {
        DiscountRate.serviceDiscountPremium = serviceDiscountPremium;
    }



   public static double getServiceDiscountGold(){
        return serviceDiscountGold;
    }



   public static void setServiceDiscountGold(double serviceDiscountGold) {
        DiscountRate.serviceDiscountGold = serviceDiscountGold;
    }



   public static double getServiceDiscountSilver() {
        return serviceDiscountSilver;
    }



   public static void setServiceDiscountSilver(double serviceDiscountSilver) {
        DiscountRate.serviceDiscountSilver = serviceDiscountSilver;
    }



   public static double getProductDiscountPremium() {
        return productDiscountPremium;
    }



   public static void setProductDiscountPremium(double productDiscountPremium) {
        DiscountRate.productDiscountPremium = productDiscountPremium;
    }



   public static double getProductDiscountGold() {
        return productDiscountGold;
    }



   public static void setProductDiscountGold(double productDiscountGold) {
        DiscountRate.productDiscountGold = productDiscountGold;
    }



   public static double getProductDiscountSilver() {
        return productDiscountSilver;
    }



   public static void setProductDiscountSilver(double productDiscountSilver) {
        DiscountRate.productDiscountSilver = productDiscountSilver;
    }
    
}