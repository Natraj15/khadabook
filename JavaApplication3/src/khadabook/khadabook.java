package khadabook;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.InputMismatchException;
public class khadabook {

    public static customer[] cusarr = new customer[500];
    public static int arrcount = 0;
    public static inventory[] inv = new inventory[500];
    public static int pcount = 0;
    static placeorder[] order = new placeorder[500];
    static  int ocount = 0;
    static  customer cus=new customer();
    static inventory inn = new inventory();
    static  placeorder plc = new placeorder();
    public static void main(String[] args) throws IOException {
        payment[] pay = new payment[500];
        int paycount = 0;
        boolean b = true;
         cus.readCustomer();
         inn.readinventory();
         plc.readorders();
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Menu");
            System.out.println("1.customer");
            System.out.println("2.inventory");
            System.out.println("3.statistics");
            System.out.println("4.exit");
            System.out.println(" ");
            int customer = 0;
            BufferedReader ins = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                try {
                    customer = Integer.parseInt(ins.readLine());
                    break;
                } catch (InputMismatchException | NumberFormatException ioe) {
                    System.err.println("Select The Correct option");

                }
            }
            switch (customer) {
                case 1: {
                    boolean custom = true;
                    do {
                        System.out.println(" ");
                        System.out.println("1.add customer");
                        System.out.println("2.place order");
                        System.out.println("3.Accept Paymet");
                        System.out.println("4.view customer");
                        System.out.println("5.update customer");
                        System.out.println("6. placed Orders");
                        System.out.println("7.exit");
                        System.out.println(" ");
                        int co = 0;
                        while (true) {
                            try {
                                co = Integer.parseInt(ins.readLine());
                                break;
                            } catch (InputMismatchException | NumberFormatException ioe) {
                                System.err.println("Select The correct option");

                            }
                        }
                        customer cust1 = new customer();
                        placeorder pl = new placeorder();
                        switch (co) {
                            case 1: {
                                cust1.addcustomer();
                                cusarr[arrcount++] = cust1;
                                cust1.display();
                                break;
                            }
                            case 2: {
                                order[ocount++] = pl;
                                pl.order(cusarr, arrcount, inv, pcount);

                                break;
                            }
                            case 3: {
                                payment py = new payment();
                                py.payment(cusarr, arrcount, order, ocount);
                                pay[paycount++] = py;
                                break;

                            }

                            case 4: {
                                cust1.viewCustomer(cusarr, arrcount);

                                break;
                            }
                            case 5: {
                                cust1.updateAddress(cusarr, arrcount);
                                break;
                            }
                            case 6: {
                                pl.placedorders(order, ocount);
                                break;
                            }
                            case 7: {
                                custom = false;
                                break;
                            }

                        }
                    } while (custom);
                    break;
                }

                case 2: {
                    boolean produc = true;
                    do {
                        System.out.println(" ");
                        System.out.println("1.add products");
                        System.out.println("2.view products");
                        System.out.println("3.update products");
                        System.out.println("4.Exit");
                        System.out.println(" ");
                        int po = 0;
                        while (true) {
                            try {
                                po = Integer.parseInt(ins.readLine());
                                break;
                            } catch (InputMismatchException | NumberFormatException ioe) {
                                System.err.println("Select The correct option");

                            }
                        }

                        inventory in = new inventory();
                        switch (po) {
                            case 1: {
                                in.addnewproducts();
                                inv[pcount++] = in;
                                in.disp();
                                break;
                            }
                            case 2: {
                                in.viewproducts(inv, pcount);

                                break;
                            }
                            case 3: {
                                in.updateProducts(inv, pcount);

                                break;
                            }
                            case 4: {
                                produc = false;
                                break;
                            }

                        }
                    } while (produc);
                    break;
                }
                case 3: {
                    boolean sts = true;
                    do {
                        System.out.println("1.Maximun selling amount order detail (start date -end date)");
                        System.out.println("2.Minimum selling amount order detail (week or month)");
                        System.out.println("3.Based on date customer orders");
                        System.out.println("4.Most sold product");
                        System.out.println("5.Less sold product");
                        System.out.println("6.Exit");
                        int st = 0;
                        while (true) {
                            try {
                                st = Integer.parseInt(ins.readLine());
                                break;
                            } catch (InputMismatchException | NumberFormatException ioe) {
                                System.err.println("Select The correct option");

                            }
                        }
                        Statistics sta = new Statistics();
                        switch (st) {
                            case 1: {
                                sta.Maxsell(order, ocount);

                                break;
                            }
                            case 2: {
                                sta.minsell(order, ocount);

                                break;
                            }
                            case 3: {
                                sta.totalsell(order, ocount);
                                break;
                            }
                            case 4: {
                                sta.mostsold(inv, pcount);
                                break;
                            }
                            case 5: {
                                sta.lesssold(inv, pcount);
                                break;
                            }
                            case 6:{
                                sts = false;
                                break;
                            }
                        }

                    } while (sts);
                    break;
                }
                case 4: {
                    b = false;
                    break;
                }

            }

        } while (b);
        cus.writecustomer();
        inn.writeiventory();
        plc.writeorders(order, ocount);
    }

   
}
