package khadabook;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import static khadabook.khadabook.pcount;
import static khadabook.khadabook.inv;
public class inventory {
    public long productId;
    public  String productName;
    public  int proQuantity;
    public  double price;
    public double oprice;
    public int bquantity;
    public int soldpro;
    public double soldpercentage;
    BufferedReader red=new BufferedReader(new InputStreamReader (System.in));

    public inventory() {
    }

    public inventory(long productId, String productName, int Quantity, double price, double oprice,int bquantity,int soldpro,double soldpercentage ) {
        this.productId = productId;
        this.productName = productName;
        this.proQuantity = Quantity;
        this.price = price;
        this.oprice = oprice;
        this.bquantity = bquantity;
        this.soldpro = soldpro;
        this.soldpercentage= soldpercentage;
    }
    
   void addnewproducts() throws IOException{
       Scanner scn=new Scanner(System.in);
       System.out.println("Enter the new products");
        productName=scn.next();
           while(true){
         try{
        System.out.println("Enter the ProdectID");
         productId=Long.parseLong(red.readLine());
      
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
          while(true){
         try{
        System.out.println("Enter the Quantity of products");
         proQuantity=Integer.parseInt(red.readLine());
         bquantity=proQuantity;
      
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
           while(true){
         try{
        System.out.println("Enter The Total product price");
         price=Double.parseDouble(red.readLine());
      
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
     while(true){
         try{
        System.out.println("Enter the One Price of the product");
         oprice=Integer.parseInt(red.readLine());
      
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }

}     
     void disp(){
         System.out.println("product is "+productName);
            System.out.println("productID is "+productId);
            System.out.println("Total prodect Quantity "+proQuantity);
            System.out.println("Total prodect price "+price);
            System.out.println("One prodect price "+oprice);
            System.out.println("Balance Products Quantity "+bquantity);
            System.out.println("Sold Products Quantity "+soldpro);
            System.out.println("sold percentage "+ soldpercentage+"%");
            System.out.println(" ");
         
     }
         
       void viewproducts(inventory[] inv,int pcount) throws IOException{
                  System.out.println("1:One product Details");
                 System.out.println("2:All product Details");
                 while(true){
         try{
            
           int  y=Integer.parseInt(red.readLine());
             switch(y){
                 case 1:
                      System.out.println("Enter The product ID");
                     int cust1=Integer.parseInt(red.readLine());
                            for(int i=0;i<pcount;i++){ 
                                if(cust1==inv[i].productId){
                               inv[i].disp();
                            }
                         }
                     break;
                 case 2:
                    
                for(int i=0;i<pcount;i++){
             
                    inv[i].disp();
                    }
                  
             
             break;   
         
             }
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Enter 1 or 2");
         }
        }
                   
                     
         
      
     }
   

     
        void updateProducts(inventory[] inv,int pcount) throws IOException {
          
          long productId;
          int Quantity;
          double price;
          double onprice;
            while(true){
         try{
        System.out.println("Enter the ProdectID");
         productId=Long.parseLong(red.readLine()); 
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }

               while(true){
         try{
        System.out.println("Change the Quantity");
         Quantity=Integer.parseInt(red.readLine()); 
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
                                      
               while(true){
         try{
        System.out.println("change the Total product price");
         price=Double.parseDouble(red.readLine()); 
         break;
         }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
         while(true){
            try{
              System.out.println("change the One product price");
              onprice=Double.parseDouble(red.readLine()); 
             break;
            }catch(InputMismatchException  |NumberFormatException ioe){
             System.err.println("Number formate only accepted");
         }
        }
             
          for(int i=0;i<pcount;i++){
                if (inv[i].productId==(productId))
            {
                
                inv[i].proQuantity=Quantity;
                inv[i].price=price;
                inv[i].oprice=onprice;
               
            }   
          }          

   }
        public  void writeiventory() throws IOException{
           try(FileOutputStream str=new FileOutputStream("/home/bas200174/khadabook/Inventory.txt"); DataOutputStream dst = new DataOutputStream(str);){ 
             for(int i=0;i<pcount;i++){
               dst.writeLong(inv[i].productId);
               dst.writeUTF(inv[i].productName);
               dst.writeInt(inv[i].proQuantity);
               dst.writeDouble(inv[i].price);
               dst.writeDouble(inv[i].oprice);
               dst.writeInt(inv[i].bquantity);
               dst.writeInt(inv[i].soldpro);
               dst.writeDouble(inv[i].soldpercentage);
             }
             
               System.out.println("Product write sucessfully");
           }catch(FileNotFoundException fnf){
               System.out.println("File Not Read");
           }
        }
         
        public  void readinventory() throws IOException {
        try ( FileInputStream fis = new FileInputStream("/home/bas200174/khadabook/Inventory.txt");  DataInputStream dis = new DataInputStream(fis);){
            while (dis.available() > 0) {
                long productid = dis.readLong();
                String proName = dis.readUTF();
                int prodQuantity = dis.readInt();
                double tprice = dis.readDouble();
                double oneprice = dis.readDouble();
                int baquantity = dis.readInt();
                int soldprod = dis.readInt();
                double soldpercentag=dis.readDouble();
              inventory in1 = new inventory(productid, proName,prodQuantity,tprice,oneprice,baquantity,soldprod,soldpercentag);     
                     inv[pcount++]=in1;
            }
            System.out.println(pcount);
            System.out.println("Products Read succussfully");
         } catch (FileNotFoundException | EOFException fnot) {
             System.out.println("File Not found");
        }
        
    }
}
