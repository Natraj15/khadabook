package khadabook;


public class lineitems {
   private int lineitemid;
   private long productid;
   private int Quantity;
   private double price;

    public lineitems() {
    }
        
       
    public lineitems( int lineitemid,long productid, int Quantity, double price) {
         this.lineitemid=lineitemid;
         this.productid = productid;
         this.Quantity = Quantity;
         this.price = price;
    }

    public int getLineitemid() {
        return lineitemid;
    }

    public void setLineitemid(int lineitemid) {
        this.lineitemid = lineitemid;
    }
      
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getProductid() {
        return productid;
    }

    public void setProductid(int productid) {
        this.productid = productid;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    @Override
    public String toString() {
        return "lineitemid: "+lineitemid+"\n"+"productid: "+productid+"\n"+"Quantity: "+Quantity+"\n"+"price: "+price;
    }
 

}