package lambdaexpression;

interface Multiple{
    int plus(int a,int b);
}
interface StringFunction{
    String run(String value);
}


public class MultipleParameters {
    
    public static void main(String[] args) {
        
        Multiple m=(a,b)->{//without datatypes
            //System.out.println(a+b);
            return a+b;
        };
        
        
        
        Multiple m1=(int a,int b)->{//withdatatypes
            
           return a+b;
        };
        
        Multiple m2=(a,b)->(a+b);//without return keyword
        
        System.out.println(m.plus(3, 4));
        System.out.println(m1.plus(10, 10));
        System.out.println(m2.plus(5, 5));
        
        System.out.println("");
        
        StringFunction exclaim=(s)->(s+"!");
        StringFunction ask=(a)->(a+"?");
        
        System.out.println(exclaim.run("Hi")); 
        System.out.println(ask.run("Hi")); 
        
        printFormatted("Hello",exclaim);
        printFormatted("Hello",ask);
        
    }
    
    public static void printFormatted(String str,StringFunction formate){
        
        String result=formate.run(str);
        System.out.println(result);
        
    }
}
