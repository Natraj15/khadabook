package lambdaexpression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

interface p1{
    void print();
}
interface p2{
    String getValue(String value);
}
interface p3{
    int getMultyple(int a, int b);
}

public class Practice {
    
    public static void main(String[] args) {
        p1 p=()->System.out.println("hi lambt");
        
        p2 ps=value->("Your value is :"+value);
        
        p3 pd=(a,b)->(a*b);
        
        System.out.println(ps.getValue("ten"));
        
        p.print();
       
        System.out.println( pd.getMultyple(4, 2));
         System.out.println("");
        List list=new ArrayList();
        list.add(10);
        list.add(20);
        list.add(30);
        
        list.forEach(n->System.out.println(n));
        System.out.println("");
        Consumer consumer=n->System.out.println(n);
        list.forEach(consumer);
        
        List<String>  list2=new ArrayList();
        
        list2.add("natraj");
        list2.add("kanna");
        list2.add("dina");
        list2.add("jobi");
        list2.add("arun");
        
        
        Collections.sort(list2,(s1,s2)->s1.compareTo(s2));
        
        list2.forEach(l->System.out.println(l));
        
        List<Integer> num=new ArrayList<>();
        num.add(5);
        num.add(2);
        num.add(1);
        System.out.println("");
        
        List<Products> product=new ArrayList<>();
        product.add(new Products(2,"a",15));
        product.add(new Products(1,"r",30));
        product.add(new Products(4,"f",20));
        product.add(new Products(6,"e",5));
        product.add(new Products(3,"e",50));
        
        
        
        Stream<Products> products=product.stream().filter(q->(q.price>10));
        Collections.sort(product,(p1,p2)->Integer.compare(p1.id, p2.id));
     
        products.forEach(e->System.out.println(e));
        
        System.out.println("");
        Set<Integer> set=new HashSet<>();
        set.add(10);
        set.add(10);
        set.add(20);
        set.add(30);
        set.add(60);
        set.add(70);
        set.add(7);
        set.add(2);
        
        Stream<Integer> filter_set=set.stream().filter(a->a.intValue()>10);
        
        filter_set.forEach(a->System.out.println(a));
        

    }
    
}
