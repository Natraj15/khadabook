package lambdaexpression;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;


class Products{
    
    int id;
    String name;
    double price;

    public Products() {
    }

    public Products(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "products{" + "id=" + id + ", name=" + name + ", price=" + price + '}';
    }
    
    
}

public class ComparatorDemo {
    
    
    public static void main(String[] args) {
         List<Products> product=new ArrayList<>();
         
         product.add(new Products(1,"c",5.0));
         product.add(new Products(2,"a",5.0));
         product.add(new Products(3,"b",50.0));
         
    
         
         Collections.sort(
         product,(p1,p2)->(p1.name.compareTo(p2.name))//compare string
         );
         
         product.forEach(
       (n)->System.out.println(n)
       );
          System.out.println("");
         List<Products> products=new ArrayList<>();
         
         products.add(new Products(4,"c",5.0));
         products.add(new Products(3,"a",5.0));
         products.add(new Products(2,"b",50.0));
         products.add(new Products(6,"d",50.0));
         
       Collections.sort(products,(l1,l2)->Integer.compare(l1.id, l2.id));//compare Integer
         
               
        products.forEach(a->System.out.println(a));
       
      
      
    }
   
}
