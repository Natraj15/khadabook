package lambdaexpression;

class Normal implements DemoInterface{//normal

    @Override
    public void print() {
        System.out.println("normal");
    }
    public static void main(String[] args) {
        Normal d=new Normal();
        d.print();
    }
    
}



public class LambdaDemo{
    
    public static void main(String[] args) {
        
        DemoInterface di=new DemoInterface(){//normal
            @Override
            public void print() {

                System.out.println("normalimplimenting");
            }
            
        };
        
        DemoInterface de=()->{
           
            System.out.println("lamdaexpression");
        };
        
        de.print();
    }
    
}
