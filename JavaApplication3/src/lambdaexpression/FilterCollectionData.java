package lambdaexpression;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class FilterCollectionData {
    
    public static void main(String[] args) {
        
        List<Products> product=new ArrayList<>();
        product.add(new Products(1,"a",50.0));
        product.add(new Products(2,"b",5.0));
        product.add(new Products(3,"c",100.0));
        product.add(new Products(4,"d",70.0));
        product.add(new Products(5,"e",40.0));
        product.add(new Products(6,"f",4.0));
        
      //  product.forEach(l->System.out.println(l));
        
     //   System.out.println(" ");
        
        Stream<Products> filter_products=product.stream().filter(p->p.price>50);
        
        filter_products.forEach(a->System.out.println(a));
        System.out.println("");
         
        Stream<Products> filter=product.parallelStream().filter(s->s.id>1);
        // Stream<Products> filter2=product.parallelStream().filter(s->s.name.contains("s"));String filter
         filter.forEach(a->System.out.println(a));
        System.out.println("");
       
        List<String> list=new ArrayList<>();
        
        list.add("kannan");
        list.add("natraj");
        list.add("natraj");
        list.add("jobin");
        list.add("dina");
        list.add("pratheepan");
         
        Stream<String> filtr_list=list.stream().filter(a->a.equals("natraj"));//string filter
        filtr_list.forEach(q->System.out.println(q));
        System.out.println("");
        Set<Integer> set=new HashSet<>();
        
        set.add(2);
        set.add(20);
        set.add(24);
        set.add(40);
        set.add(60);
        set.add(75);
        set.add(110);
        
        Stream<Integer> filter_set=set.stream().filter(a->a.intValue()>50);
        filter_set.forEach(c->System.out.println(c));
        System.out.println("");
        set.stream().filter(pr->pr.intValue()==2).forEach(System.out::println);
        
    }
    
}
