package lambdaexpression;

public class Sample  extends Demo1{
    
    public static void main(String[] args) {
        
        Second s=new Second(){//interface
            @Override
            public void getAmount() {
                System.out.println("1000");
            }

            @Override
            public void getBalance() {
                System.out.println("10");
            }
            
        };
        
        Demo1 d=new Demo1(){//class
            @Override
           void play() {
                
                  System.out.println("kabadi");
                 
            }

            @Override
            void sing() {
                System.out.println("paravaiye engu erukirai?");
            }
            
            
        };
      
        
      
        s.getAmount();
        s.getBalance();
        
        d.play();
        d.sing();
    }
    
}
