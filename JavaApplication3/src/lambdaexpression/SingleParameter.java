package lambdaexpression;

interface Single{
    
    void food(String name);
}


public class SingleParameter {
    
    public static void main(String[] args) {
      //  String name="birniyani";
        Single s=(name)->{
            System.out.println("food :"+name);
        };
        
        Single s2=food->{//we can give without ()
            System.out.println("food :"+food);
        };
        
        s.food("biriyani");
        s2.food("porotta");
    }
   
}
