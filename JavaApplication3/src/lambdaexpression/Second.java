package lambdaexpression;

public interface Second {
    
    void getAmount();
    void getBalance();
}
