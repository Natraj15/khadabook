package lambdaexpression;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class ForeachLoop {
    
    public static void main(String[] args) {
        
        List list=new ArrayList();
        list.add("natraj");
        list.add("kannan");
        list.add("pradeepan");
        list.add("jobin");
        
        for(Object l:list){//normal
            
            System.out.println(l);
        }
        System.out.println("");
        
        list.forEach(
                
                (n)->System.out.println(n)// (n)->{System.out.println(n);}
        );
        
        
        
        System.out.println("");
        Consumer cunsumer=n->System.out.println(n);
        list.forEach(cunsumer);
    }
    
}
