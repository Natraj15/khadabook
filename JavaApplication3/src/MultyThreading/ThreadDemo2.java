package MultyThreading;

public class ThreadDemo2 implements Runnable{
     static  int  i=0;
    public static void main(String[] args) throws InterruptedException {
       
        System.out.println(i);
        System.out.println("outside the thread");
        
        ThreadDemo2 demo=new ThreadDemo2();
        Thread thread=new Thread(demo);
        thread.start();
        thread.join();
        i++;
        System.out.println(i);
        
    }
    
    
    
    @Override
    public void run() {
     
        i++;
        System.out.println("inside the thread");
    }
    
    
}

class Demo3 extends Thread{
    
    public static void main(String[] args) {
        
        System.out.println("outside thread");
        
        Demo3 thread=new Demo3();
        thread.start();
        
    }

    @Override
    public void run() {
        
        System.out.println("inside thread");
    }
    
}


class TimeThread extends Thread
{

    public TimeThread() {
        
        Thread thread=new Thread(this,"timethread");
      //  TimeThread thread1=new TimeThread();
      
        thread.start();
        System.out.println(thread.getName());
    }

    @Override
    public void run() {
       
        System.out.println("Time : "+java.time.LocalDate.now());
    }
    
    public static void main(String[] args) {
        new TimeThread();
       
       
        
     /*   Runnable thread=()->{
            System.out.println("lambda expression");
        };
      
         Thread thre=new Thread(thread);
         thre.start();
         
         Thread thread2=new Thread(
         ()->{
             System.out.println("annonymous");
         },"thread2"
         );
         
         thread2.start();*/
    }
    
}