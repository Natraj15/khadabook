package MultyThreading;

import java.util.logging.Level;
import java.util.logging.Logger;

class Q{
    
    int num;
    boolean cheack=false;
    
    public synchronized void setQ(int num){
      
        while(cheack){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Q.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("put :"+num);
        this.num=num;
        cheack=true;
        notify();
    }
    public synchronized void getQ(){
         while(!cheack){
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Q.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("get :"+num);
        cheack=false;
        notify();
    }
}

class Producer implements Runnable{
    
    Q q;

    public Producer(Q q) {
        this.q = q;
      //  Thread t1=new Thread(this,"producer");
       // t1.start();
    }

    @Override
    public void run() {
         int i=1;
        while(true){
             try {
                 q.setQ(i++);
                 Thread.sleep(500);
             } catch (InterruptedException ex) {
                 Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
    
}

class Cunsumer implements Runnable{
    
     Q q;

    public Cunsumer(Q q) {
        this.q = q;
        //Thread t1=new Thread(this,"consumer");//when object creat for cunsumer thread will start
        //t1.start();
    }

    @Override
    public void run() {
         
        while(true){
             try {
                 q.getQ();
                 Thread.sleep(1000);
             } catch (InterruptedException ex) {
                 Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
             }
        }
    }
    
}


public class ThreadInterCommunication {
    
    public static void main(String[] args) {
        
         Q q=new Q();
     //  new Producer(q);
     //  new Cunsumer(q);
      
     Producer producer=new Producer(q);
     Cunsumer cunsumer=new Cunsumer(q);
     
     Thread th1=new Thread(producer,"producer");
     Thread th2=new Thread(cunsumer,"cunsumer");
     
     th1.start();
     th2.start();
     
    }
   
    
}
