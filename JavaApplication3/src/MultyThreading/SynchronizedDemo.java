package MultyThreading;

import java.util.logging.Level;
import java.util.logging.Logger;


class Table1{
   
   synchronized void printTable(int num){
        for (int i = 1; i <= 5; i++) {
            System.out.println(i*num);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Table1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}


public class SynchronizedDemo {
    
    public static void main(String[] args) {
        
        Table1 t=new Table1();
      //  t.printTable(5);
        
        Thread t1=new Thread(){
           public void run(){
               t.printTable(5);
               
           } 
           
        };
        
        
        
        Thread t2=new Thread(()->{
                t.printTable(10);
            },"secondthread");
        
        t2.setPriority(Thread.MAX_PRIORITY);
        
        t1.start();
        t2.start();
        System.out.println("priority1 "+t1.getPriority());//default priority 5
        System.out.println("priority2 "+t2.getPriority());
        System.out.println(t2.getName());
    }
    
}
