package MultyThreading;

import java.util.logging.Level;
import java.util.logging.Logger;

class Demo extends Thread{
    
   @Override
   public  void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("updatedp");
           // System.out.println(arr);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DemoThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
   void num(){
       System.out.println("1234");
   }
}


public class DemoThread implements Runnable{
    
   
   public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
              try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DemoThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
      
    }
    
    public static void main(String[] args) throws InterruptedException {
       
        
        Runnable thread1=()->// Runnbale is a function interface so using Lambta expression 
        {
              for (int i = 0; i < 5; i++) {
            System.out.println("ananymous");
           // System.out.println(arr);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DemoThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        };
        
        
        
        Thread thread3=new Thread(){
            public void run(){
                
                for (int i = 0; i < 5; i++) {
                    System.out.println("Natraj");
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(DemoThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        
         Demo demo=new Demo();//extends thread class
        
        
         DemoThread demothread=new DemoThread();//implimens runnable interface 
         Thread thread=new Thread(demothread);
        
         Thread th=new Thread(thread1);
        
        demo.start();
         
        thread3.start();
        
        th.start();
        
        thread.start();
        
        demo.join();
        thread.join();
        
        demo.setName("demothread");//set thread name
        
        System.out.println(demo.getName());
        //if(demo.isAlive()==false){
        System.out.println("bye...");
    //}
       
    }
    
}
