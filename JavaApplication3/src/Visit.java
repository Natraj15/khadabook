
 import java.util.Date;


public class Visit extends Custom{
    private Custom customer;
    private Date date;
   private double serviceExpense;
   private double productExpense;



   public Visit(String name, Date date) {
        super(name);
        this.date=date;
    }



   public double getServiceExpense() {
        return serviceExpense;
    }



   public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }



   public double getProductExpense() {
        return productExpense;
    }



   public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }
    public double getTotalExpenses()
    {
        return serviceExpense+productExpense;
    }
    
}
    

